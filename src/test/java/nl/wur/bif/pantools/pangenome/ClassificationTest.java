package nl.wur.bif.pantools.pangenome;

import nl.wur.bif.pantools.pantools.Pantools;
import org.apache.logging.log4j.LogManager;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Unit tests for methods in the Classification class.
 *
 * @author Dirk-Jan van Workum
 */
public class ClassificationTest {
    private static Classification classification;

    @BeforeAll
    static void disableLogging() {
        System.setProperty("log4j2.rootLevel", "off");
        Pantools.logger = LogManager.getLogger(Pantools.class);
    }

    @BeforeEach
    public void setUp() {
        // Create a new classification
        classification = new Classification();
    }

    @AfterEach
    public void tearDown() {
        // Get all files starting with "log_file" and delete them
        try {
            Files.list(Paths.get("."))
                    .filter(Files::isRegularFile)
                    .filter(path -> path.getFileName().toString().startsWith("log_file"))
                    .forEach(path -> {
                        try {
                            Files.delete(path);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testCalculateCoreAccessoryUniqueNewGenes1() {
        ArrayList<String> allGenomeList = new ArrayList<>();
        allGenomeList.add("1");
        allGenomeList.add("2");
        allGenomeList.add("3");
        ArrayList<String> genomeList = new ArrayList<>();
        genomeList.add("1");
        genomeList.add("2");
        genomeList.add("3");
        int[][] allHmGroupArray = new int[3][genomeList.size()];
        allHmGroupArray[0][0] = 1;
        allHmGroupArray[0][1] = 1;
        allHmGroupArray[0][2] = 1;
        allHmGroupArray[1][0] = 1;
        allHmGroupArray[1][1] = 1;
        allHmGroupArray[1][2] = 1;
        allHmGroupArray[2][0] = 1;
        allHmGroupArray[2][1] = 1;
        allHmGroupArray[2][2] = 1;
        ArrayList<Integer> seenHmGroups = new ArrayList<>();

        // first homology group is core, second is core, third is core and no hm group is seen yet
        int[] expectedResult = new int[]{3, 0, 0, 3};

        int[] result = classification.calculateCoreAccessoryUniqueNewGroups(genomeList, allGenomeList, allHmGroupArray, seenHmGroups);

        Assertions.assertArrayEquals(expectedResult, result);
    }

    @Test
    void testCalculateCoreAccessoryUniqueNewGenes2() {
        ArrayList<String> allGenomeList = new ArrayList<>();
        allGenomeList.add("1");
        allGenomeList.add("2");
        allGenomeList.add("3");
        ArrayList<String> genomeList = new ArrayList<>();
        genomeList.add("1");
        genomeList.add("2");
        genomeList.add("3");
        int[][] allHmGroupArray = new int[3][genomeList.size()];
        allHmGroupArray[0][0] = 1;
        allHmGroupArray[0][1] = 1;
        allHmGroupArray[0][2] = 1;
        allHmGroupArray[1][0] = 1;
        allHmGroupArray[1][1] = 0;
        allHmGroupArray[1][2] = 1;
        allHmGroupArray[2][0] = 1;
        allHmGroupArray[2][1] = 1;
        allHmGroupArray[2][2] = 1;
        ArrayList<Integer> seenHmGroups = new ArrayList<>();

        // first homology group is core, second is accessory, third is core and no hm group is seen yet
        int[] expectedResult = new int[]{2, 1, 0, 3};

        int[] result = classification.calculateCoreAccessoryUniqueNewGroups(genomeList, allGenomeList, allHmGroupArray, seenHmGroups);

        Assertions.assertArrayEquals(expectedResult, result);
    }

    @Test
    void testCalculateCoreAccessoryUniqueNewGenes3() {
        ArrayList<String> allGenomeList = new ArrayList<>();
        allGenomeList.add("1");
        allGenomeList.add("2");
        allGenomeList.add("3");
        ArrayList<String> genomeList = new ArrayList<>();
        genomeList.add("1");
        genomeList.add("2");
        genomeList.add("3");
        int[][] allHmGroupArray = new int[3][genomeList.size()];
        allHmGroupArray[0][0] = 1;
        allHmGroupArray[0][1] = 1;
        allHmGroupArray[0][2] = 1;
        allHmGroupArray[1][0] = 1;
        allHmGroupArray[1][1] = 0;
        allHmGroupArray[1][2] = 1;
        allHmGroupArray[2][0] = 0;
        allHmGroupArray[2][1] = 1;
        allHmGroupArray[2][2] = 0;
        ArrayList<Integer> seenHmGroups = new ArrayList<>();

        // first homology group is core, second is accessory, third is unique and no hm group is seen yet
        int[] expectedResult = new int[]{1, 1, 1, 3};

        int[] result = classification.calculateCoreAccessoryUniqueNewGroups(genomeList, allGenomeList, allHmGroupArray, seenHmGroups);

        Assertions.assertArrayEquals(expectedResult, result);
    }

    @Test
    void testCalculateCoreAccessoryUniqueNewGenes4() {
        ArrayList<String> allGenomeList = new ArrayList<>();
        allGenomeList.add("1");
        allGenomeList.add("2");
        allGenomeList.add("3");
        ArrayList<String> genomeList = new ArrayList<>();
        genomeList.add("1");
        genomeList.add("2");
        genomeList.add("3");
        int[][] allHmGroupArray = new int[3][genomeList.size()];
        allHmGroupArray[0][0] = 1;
        allHmGroupArray[0][1] = 1;
        allHmGroupArray[0][2] = 1;
        allHmGroupArray[1][0] = 1;
        allHmGroupArray[1][1] = 0;
        allHmGroupArray[1][2] = 1;
        allHmGroupArray[2][0] = 0;
        allHmGroupArray[2][1] = 1;
        allHmGroupArray[2][2] = 0;
        ArrayList<Integer> seenHmGroups = new ArrayList<>();
        seenHmGroups.add(1);

        // first homology group is core, second is accessory, third is unique and hm group 1 is seen before
        int[] expectedResult = new int[]{1, 1, 1, 2};

        int[] result = classification.calculateCoreAccessoryUniqueNewGroups(genomeList, allGenomeList, allHmGroupArray, seenHmGroups);

        Assertions.assertArrayEquals(expectedResult, result);
    }

    @Test
    void testCalculateCoreAccessoryUniqueNewGenes5() {
        ArrayList<String> allGenomeList = new ArrayList<>();
        allGenomeList.add("1");
        allGenomeList.add("2");
        allGenomeList.add("3");
        ArrayList<String> genomeList = new ArrayList<>();
        genomeList.add("1");
        genomeList.add("2");
        genomeList.add("3");
        int[][] allHmGroupArray = new int[3][genomeList.size()];
        allHmGroupArray[0][0] = 1;
        allHmGroupArray[0][1] = 2;
        allHmGroupArray[0][2] = 1;
        allHmGroupArray[1][0] = 1;
        allHmGroupArray[1][1] = 0;
        allHmGroupArray[1][2] = 1;
        allHmGroupArray[2][0] = 0;
        allHmGroupArray[2][1] = 3;
        allHmGroupArray[2][2] = 0;
        ArrayList<Integer> seenHmGroups = new ArrayList<>();
        seenHmGroups.add(1);

        // first homology group is core, second is accessory, third is unique and hm group 1 is seen before
        // also, increasing copy numbers here for the first and third homology group
        int[] expectedResult = new int[]{1, 1, 1, 2};

        int[] result = classification.calculateCoreAccessoryUniqueNewGroups(genomeList, allGenomeList, allHmGroupArray, seenHmGroups);

        Assertions.assertArrayEquals(expectedResult, result);
    }

    @Test
    void testCalculateCoreAccessoryUniqueNewGenes6() {
        ArrayList<String> allGenomeList = new ArrayList<>();
        allGenomeList.add("1");
        allGenomeList.add("2");
        allGenomeList.add("3");
        ArrayList<String> genomeList = new ArrayList<>();
        genomeList.add("1");
        genomeList.add("2");
        int[][] allHmGroupArray = new int[3][3]; // 3 homology groups, 3 genomes
        allHmGroupArray[0][0] = 1;
        allHmGroupArray[0][1] = 2;
        allHmGroupArray[0][2] = 1;
        allHmGroupArray[1][0] = 1;
        allHmGroupArray[1][1] = 0;
        allHmGroupArray[1][2] = 1;
        allHmGroupArray[2][0] = 0;
        allHmGroupArray[2][1] = 3;
        allHmGroupArray[2][2] = 0;
        ArrayList<Integer> seenHmGroups = new ArrayList<>();
        seenHmGroups.add(1);

        // first homology group is core, second is unique, third is unique and hm group 1 is seen before
        int[] expectedResult = new int[]{1, 0, 2, 2};

        int[] result = classification.calculateCoreAccessoryUniqueNewGroups(genomeList, allGenomeList, allHmGroupArray, seenHmGroups);

        Assertions.assertArrayEquals(expectedResult, result);
    }

    @Test
    void testCalculateCoreAccessoryUniqueNewGenes7() {
        ArrayList<String> allGenomeList = new ArrayList<>();
        allGenomeList.add("1");
        allGenomeList.add("2");
        allGenomeList.add("3");
        ArrayList<String> genomeList = new ArrayList<>();
        genomeList.add("1");
        genomeList.add("3");
        int[][] allHmGroupArray = new int[3][3]; // 3 homology groups, 3 genomes
        allHmGroupArray[0][0] = 1;
        allHmGroupArray[0][1] = 2;
        allHmGroupArray[0][2] = 1;
        allHmGroupArray[1][0] = 1;
        allHmGroupArray[1][1] = 1;
        allHmGroupArray[1][2] = 0;
        allHmGroupArray[2][0] = 0;
        allHmGroupArray[2][1] = 3;
        allHmGroupArray[2][2] = 0;
        ArrayList<Integer> seenHmGroups = new ArrayList<>();
        seenHmGroups.add(1);

        // first homology group is core, second is unique and hm group 1 is seen before
        // also, the third homology group is completely absent
        int[] expectedResult = new int[]{1, 0, 1, 1};

        int[] result = classification.calculateCoreAccessoryUniqueNewGroups(genomeList, allGenomeList, allHmGroupArray, seenHmGroups);

        Assertions.assertArrayEquals(expectedResult, result);
    }
}

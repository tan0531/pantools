package nl.wur.bif.pantools.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Sample tests for the FileUtils.loadScoringMatrix() functions for each of the available matrices.
 */
class LoadScoringMatrixTest {
    @Test
    void loadBlosum45Test() {
        final int[][] blosum = FileUtils.loadScoringMatrix("BLOSUM45");
        assertAll(
                () -> assertEquals(blosum['F']['G'], -3),
                () -> assertEquals(blosum['W']['W'], 15),
                () -> assertEquals(blosum['V']['Q'], -3)
        );
    }
    @Test
    void loadBlosum62Test() {
        final int[][] blosum = FileUtils.loadScoringMatrix("BLOSUM62");
        assertAll(
                () -> assertEquals(blosum['F']['G'], -3),
                () -> assertEquals(blosum['W']['W'], 11),
                () -> assertEquals(blosum['V']['Q'], -2)
        );
    }

    @Test
    void LoadNucTest() {
        final int[][] blosum = FileUtils.loadScoringMatrix("NUC.4.4");
        assertAll(
                () -> assertEquals(blosum['G']['W'], -4),
                () -> assertEquals(blosum['G']['W'], -4),
                () -> assertEquals(blosum['G']['W'], -4)
        );
    }
}
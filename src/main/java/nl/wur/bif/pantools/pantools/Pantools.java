/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.wur.bif.pantools.pantools;


import nl.wur.bif.pantools.cli.Map;
import nl.wur.bif.pantools.cli.*;
import nl.wur.bif.pantools.utils.GraphUtils;
import nl.wur.bif.pantools.utils.ConsoleUtils;
import nl.wur.bif.pantools.variation.add_pav.AddPavCLI;
import nl.wur.bif.pantools.variation.add_variants.AddVariantsCLI;
import nl.wur.bif.pantools.variation.remove_pav.RemovePavCLI;
import nl.wur.bif.pantools.variation.remove_variants.RemoveVariantsCLI;
import nl.wur.bif.pantools.variation.variation_overview.VariationOverviewCLI;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;
import picocli.CommandLine.*;
import picocli.CommandLine.Model.CommandSpec;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.*;

import static java.util.ResourceBundle.getBundle;
import static nl.wur.bif.pantools.utils.Globals.setGlobals;
import static nl.wur.bif.pantools.utils.Utils.disconnectPangenome;
import static nl.wur.bif.pantools.utils.Utils.readPropertiesFile;
import static picocli.CommandLine.Model.UsageMessageSpec.SECTION_KEY_COMMAND_LIST;
import static picocli.CommandLine.ScopeType.INHERIT;


/**
 * Implements the main function and declares shared variables. Initializes command line logic.
 * Contains the --help and --version commands of the main function.
 *
 * @author Robin van Esch, Bioinformatics Group, Wageningen University, the Netherlands.
 */

@Command(name = "pantools",
        versionProvider = Pantools.GitVersionProvider.class,
        subcommands = {
        BuildPangenome.class,
        AddGenomes.class,
        BuildPanproteome.class,
        AddAnnotations.class,
        AddAntiSMASH.class,
        AddFunctions.class,
        AddPhenotypes.class,
        AddPavCLI.class,
        RemovePavCLI.class,
        AddVariantsCLI.class,
        RemoveVariantsCLI.class,
        BuscoProtein.class,
        RemoveAnnotations.class,
        RemoveFunctions.class,
        RemoveNodes.class,
        RemovePhenotypes.class,
        RemoveGrouping.class,
        MoveGrouping.class,
        Group.class,
        OptimalGrouping.class,
        ChangeGrouping.class,
        Metrics.class,
        GeneClassification.class,
        CoreUniqueThresholds.class,
        GroupingOverview.class,
        PangenomeStructure.class,
        KmerClassification.class,
        FunctionalClassification.class,
        FunctionOverview.class,
        GOEnrichment.class,
        CorePhylogeny.class,
        ConsensusTree.class,
        ANI.class,
        MLSAFindGenes.class,
        MLSAConcatenate.class,
        MLSA.class,
        RenamePhylogeny.class,
        RootPhylogeny.class,
        CreateTreeTemplate.class,
        LocateGenes.class,
        FindGenesByAnnotation.class,
        FindGenesByName.class,
        FindGenesInRegion.class,
        ShowGO.class,
        CompareGO.class,
        GroupInfo.class,
        OrderMatrix.class,
        RenameMatrix.class,
        RetrieveFeatures.class,
        RetrieveRegions.class,
        MSA.class,
        Map.class,
        ExportPangenome.class,
        VariationOverviewCLI.class,
})
public class Pantools {
    @Parameters(descriptionKey = "database-path", index = "0", scope = INHERIT)
    private Path databaseDirectory;

    @Option(names = {"-f", "--force"}, hidden = true, scope = INHERIT)
    boolean force;

    @Option(names = "--no-input", hidden = true, negatable = true, scope = INHERIT)
    boolean input;

    @Option(names = {"--trace"}, hidden = true, scope = INHERIT)
    boolean trace;

    @Option(names = {"-d", "--debug"}, hidden = true, scope = INHERIT)
    boolean debug;

    @Option(names = {"-q", "--quiet"}, hidden = true, scope = INHERIT)
    boolean quiet;

    @Option(names = {"--silent"}, hidden = true, scope = INHERIT)
    boolean silent;

    @Option(names = {"-V", "--version"}, versionHelp = true, hidden = true)
    boolean versionInfoRequested;

    @Option(names = {"-h", "--help"}, usageHelp = true, hidden = true, scope = INHERIT)
    boolean usageHelpRequested;

    @Option(names = {"-M", "--manual"}, help = true, hidden = true, scope = INHERIT)
    boolean manualRequested;

    public static Logger logger;
    private GraphUtils pangenomeGraph;

    /**
     * The main function of PanTools.
     *
     * @param args The command line arguments
     */
    public static void main(String[] args) throws IOException, URISyntaxException {
        final Instant startTime = Instant.now();

        // Instantiate main class and command line
        final Pantools pantools = new Pantools();
        final CommandLine cmd = new CommandLine(pantools);

        // Set custom default values file, usage help message file and help subcommand section
        cmd.setDefaultValueProvider(pantools.getDefaultValueProvider());
        cmd.setResourceBundle(getBundle("MessageBundle"));
        cmd.getHelpSectionMap().put(SECTION_KEY_COMMAND_LIST, new CommandGroupRenderer());

        // Parse arguments and execute subcommands
        int exitCode = cmd.setExecutionStrategy(pantools::executionStrategy).execute(args);

        // Common code for all subcommands before system exit
        pantools.exitStrategy(startTime);
        System.exit(exitCode);
    }

    /**
     * Get the defaults file from project.
     *
     * @return Defaults.properties file
     */
    private PropertiesDefaultProvider getDefaultValueProvider() {
        final Properties properties = new Properties();
        final URL defaultsFile = getClass().getClassLoader().getResource("Defaults.properties");
        if (defaultsFile != null) {
            try (InputStream inputStream = defaultsFile.openStream()) {
                properties.load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new PropertiesDefaultProvider(properties);
    }

    /**
     * Initialization logic. Sets global parameters, configures logging.
     * If access to the manual is requested, open a browser.
     *
     * @return exit code after running the subcommand
     */
    private int executionStrategy(ParseResult parseResult) {
        if (manualRequested) {
            return browseManual(parseResult);
        }
        if (parseResult.hasSubcommand() && !usageHelpRequested) {
            setGlobals(this);
        }
        return new CommandLine.RunLast().execute(parseResult);
    }

    /**
     * Setup function for log4j2 logger and system properties used in log4j2.properties.
     * The log file has the name of the called subcommand with a timestamp
     * The log levels is set according to the user input
     * The log folder is located in the database directory
     * @param spec CommandsPec containing subclass specific information
     * @exception ParameterException thrown when the given database directory does not exist or is not a directory
     */
    public void createLogger(CommandSpec spec) throws ParameterException {
        // validate database directory before creating a log directory
        if (!Files.exists(databaseDirectory) || !Files.isDirectory(databaseDirectory)) {
            throw new ParameterException(spec.commandLine(), "Pangenome database directory does not exist.");
        }

        // set system properties for the log4j2 configuration file
        final String logLevel =  silent ? "error" : quiet ? "warn" : debug ? "debug" : trace ? "trace" : "info";
        System.setProperty("log4j2.level", logLevel);
        System.setProperty("log4j2.saveDirectory", String.valueOf(databaseDirectory.resolve("logs")));
        System.setProperty("log4j2.logFile", spec.name());

        // create logger
        logger = LogManager.getLogger(this);
        logger.info("Usage: pantools {}", String.join(" ", spec.commandLine().getParseResult().originalArgs()));
    }

    /**
     * Get the read the docs webpage for a given command and browse it.
     * @param parseResult argument parsing result
     */
    private int browseManual(ParseResult parseResult) {
        final String gitBranch = getCurrentGitBranch();
        String extension = "";
        if (parseResult.hasSubcommand()) {
            String name = parseResult.subcommands().get(0).commandSpec().name();

            // function / header naming inconsistencies
            if (name.startsWith("mlsa")) name = "mlsa";

            try {
                extension = String.format("%s.html#%s", getManualSection(name), name.replaceAll("_", "-"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        final String url = String.format("https://pantools.readthedocs.io/en/%s/%s", gitBranch, extension);
        browse(url);
        return 0;
    }

    /**
     * Get the manual section for a given command
     * @param command command name
     * @return manual section for the command
     * @throws IOException when the properties file cannot be read
     */
    private String getManualSection(String command) throws IOException {
        final Properties manualSections = new Properties();
        final URL manualSectionsFile = getClass().getClassLoader().getResource("ManualSections.properties");
        if (manualSectionsFile != null) {
            try (InputStream inputStream = manualSectionsFile.openStream()) {
                manualSections.load(inputStream);
            }
        }
        return manualSections.getProperty(command);
    }

    /**
     * Reads the branch name for the running project.
     * @return the git branch name
     */
    private String getCurrentGitBranch() {
        String branch = "master";
        try {
            Process process = Runtime.getRuntime().exec("git rev-parse --abbrev-ref HEAD");
            process.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            branch = reader.readLine();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return branch;
    }

    /**
     * Browse the default web browser to open the URL page
     * @param urlString page to be opened
     */
    private void browse(String urlString) {
        try {
            if (Desktop.isDesktopSupported()) {
                Desktop desktop = Desktop.getDesktop();
                if (desktop.isSupported(Desktop.Action.BROWSE)) {
                    desktop.browse(URI.create(urlString));
                }
            }
        } catch (IOException | InternalError e) {
            e.printStackTrace();
        }
    }

    /**
     * Common commands for all subcommands before system exit. Clears Neo4j database transactions and temporary files.
     * Adds time of execution, command line arguments, run time and exit code to the usage log.
     */
    private void exitStrategy(Instant startTime) {
        if (!(usageHelpRequested || versionInfoRequested || manualRequested || logger == null)) {
            disconnectPangenome();
            logger.info(String.format("Run time: %s\n", getRunTime(startTime)));
        }
    }

    /**
     * Calculate the total run time based on the start time and current time.
     * Formats the output to a readable format.
     *
     * @param startTime time instant recorded at execution
     * @return formatter run time
     */
    private String getRunTime(Instant startTime) {
        final Instant stopTime = Instant.now();
        final Duration duration = Duration.between(startTime, stopTime);
        return duration.toString().substring(2).replaceAll("(\\d[HMS])(?!$)", "$1 ").toLowerCase();
    }

    /**
     * Custom IHelpSectionRenderer class to use in 'pantools --help'.
     * Orders the pantools commands by category.
     *
     * @author  Robin van Esch
     */
    static class CommandGroupRenderer implements IHelpSectionRenderer {

        /**
         * Creates a custom field for picocli's SECTION_KEY_COMMAND_LIST.
         * Consist of commands ordered in categories initialized by createSectionsMap().
         *
         * @param help CommandLine Help class initialized by '--help' option containing help message options
         * @return String field for SECTION_KEY_COMMAND_LIST
         */
        public String render(Help help) {
            // create a HashMap separating subcommands by section
            final LinkedHashMap<String, List<CommandLine>> sections = new LinkedHashMap<>();

            // get the message bundle containing subcommand section names
            final ResourceBundle messageBundle = help.commandSpec().resourceBundle();

            // get the PanTools subcommands as set to avoid duplicates via aliases
            final Set<CommandLine> subcommands = new HashSet<>(help.commandSpec().subcommands().values());

            // add each subcommand to their section
            for (CommandLine command : subcommands) {
                final String header = messageBundle.getString("pantools." + command.getCommandName() + ".section");
                sections.computeIfAbsent(header, k -> new ArrayList<>()).add(command);
            }

            // return the rendered section headers and usage messages as a single string
            final StringBuilder commandList = new StringBuilder();
            sections.forEach((key, value) -> commandList.append(renderSection(key, value, help)));
            return commandList.toString();
        }

        /**
         * Collects use messages for all commands for a certain category.
         * @param sectionHeading String for category description
         * @param subcommands List of picocli @Command names for pantools subcommands
         * @param help CommandLine Help class initialized by '--help' option containing help message options
         * @return String field for all commands in the given category and their usage message
         */
        private String renderSection(String sectionHeading, List<CommandLine> subcommands, Help help) {
            final Help.TextTable textTable = createTextTable(help);
            sectionHeading = help.createHeading(String.format("%n@|bold,underline %s|@%n", sectionHeading));

            for (CommandLine command : subcommands) {
                final CommandSpec commandSpec = command.getCommandSpec();

                // create comma-separated list of command name and aliases
                String names = commandSpec.names().toString();
                names = names.substring(1, names.length() - 1); // remove leading '[' and trailing ']'

                // description may contain line separators; use Text::splitLines to handle this
                String description = commandSpec.usageMessage().description()[0];
                Help.Ansi.Text[] lines = help.colorScheme().text(description).splitLines();

                for (int i = 0; i < lines.length; i++) {
                    Help.Ansi.Text cmdNamesText = help.colorScheme().commandText(i == 0 ? names : "");
                    textTable.addRowValues(cmdNamesText, lines[i]);
                }
            }

            return sectionHeading + textTable;
        }

        /**
         * Create a custom layout for the pantools subcommands and their description
         * @param help CommandLine Help class containing help message options
         * @return TextTable class containing help message layout for the commands section
         */
        private Help.TextTable createTextTable(Help help) {
            CommandSpec spec = help.commandSpec();
            // prepare layout: two columns
            // the left column overflows, the right column wraps if text is too long
            int commandLength = maxLength(spec.subcommands());
            Help.TextTable textTable = Help.TextTable.forColumns(help.colorScheme(),
                    new Help.Column(commandLength + 2, 2, Help.Column.Overflow.SPAN),
                    new Help.Column(spec.usageMessage().width() - (commandLength + 2), 2, Help.Column.Overflow.WRAP));
            textTable.setAdjustLineBreaksForWideCJKCharacters(
                    spec.usageMessage().adjustLineBreaksForWideCJKCharacters());
            return textTable;
        }

        /**
         * Calculates the maximum column width of subcommand names for spacing of the text table.
         * @param subcommands Map of all pantools subcommands
         * @return int for maximum column width
         */
        private int maxLength(java.util.Map<String, CommandLine> subcommands) {
            int result = subcommands.values().stream().map(
                    cmd -> cmd.getCommandSpec().names().toString().length() - 2).max(Integer::compareTo).get();
            return Math.min(37, result);
        }
    }

    /**
     * Custom IVersionProvider class to use in 'pantools --version'.
     * Gets the most up to date version information from git.
     *
     * @author Robin van Esch TODO: add authors of getVersionInformation() function (originally in utils/Utils.java)
     */
    protected static class GitVersionProvider implements IVersionProvider {

        /**
         * Return build version of PanTools and abbreviated commit ID, also indicating if the current state of the code base
         * is dirty (containing changed files not staged). Build version is the latest tag.
         *
         * Will throw an {{@link IllegalArgumentException}} in case the git.properties file is found, but does not contain
         * a needed item.
         * @return String[] containing the PanTools version and abbreviated commit ID, with a comma in in-between.
         */
        public String[] getVersion() {
            try {
                // Try to read from git.properties, which will be included in a Maven-generated jar
                final Path gitPropertiesFile = Paths.get("/git.properties");
                final Properties properties = readPropertiesFile(gitPropertiesFile);
                final String buildVersion = properties.getProperty("git.build.version");
                final String abbreviatedCommitId = properties.getProperty("git.commit.id.abbrev");
                return new String[] {String.format("version %s, abbrev. commit ID %s", buildVersion, abbreviatedCommitId)};
            } catch (IOException e) {
                // TODO: if exception is thrown because of missing file or something else
                // Caused (most likely) by missing git.properties file, return unknown version
                // TODO: use JGit to extract information from local repository
            } catch (NullPointerException ignored) {
                throw new IllegalArgumentException("missing build version or abbreviated commit ID in /git.properties");
            }
            return new String[] {"development version"};
        }
    }

    /**
     * Validate the given database directory path and creates a database if empty.
     * @throws IOException When the database is not valid
     */
    public void createDatabaseDirectory() throws IOException {
        validateDirectoryPath();
        Files.createDirectories(databaseDirectory);
    }

    /**
     * Validates the database directory path for a new directory.
     * Cleans the directory if it contains files, considering user input.
     * @throws IOException When the database contains files or is not a directory
     */
    private void validateDirectoryPath() throws IOException {
        // check if the database already exists and is a directory
        if (!Files.exists(databaseDirectory)) return;
        if (!Files.isDirectory(databaseDirectory)) {
            throw new IllegalArgumentException("Given database directory is not a directory.");
        }

        // if the directory is empty or --force is given, overwrite
        if (force || Objects.requireNonNull(databaseDirectory.toFile().list()).length == 0) {
            FileUtils.deleteDirectory(databaseDirectory.toFile());
            return;
        }

        // Ask for user input to decide whether to overwrite the directory or not
        if (input) {
            // Ask for user input to decide whether to overwrite the directory or not
            final String query = String.format("<%s> already exists and contains files, do you want to overwrite it?",
                    databaseDirectory);
            if (ConsoleUtils.askYesOrNo(query)) {
                FileUtils.deleteDirectory(databaseDirectory.toFile());
                return;
            }
        }
        throw new FileAlreadyExistsException("Given database directory already exists and contains files");
    }

    public void setPangenomeGraph() throws IOException {
        GraphUtils.createGraphDatabaseService(databaseDirectory);
        GraphUtils.setDatabaseParameters();
    }

    public void setPangenomeGraph(String type) throws IOException {
        setPangenomeGraph();
        GraphUtils.validateGraphType(type);
    }

    public Logger getLogger() {
        return logger;
    }
    public Path getDatabaseDirectory() {
        return databaseDirectory;
    }
    public boolean isForce() {
        return force;
    }
    public boolean isInput() {
        return input;
    }
}



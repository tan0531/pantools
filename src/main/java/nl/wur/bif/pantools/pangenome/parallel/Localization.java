package nl.wur.bif.pantools.pangenome.parallel;

import java.io.Serializable;
import java.util.Objects;

/***
 * This class stores updates to the localization information stored in the pangenome graph. It is also used to calculate
 * the frequency with which each nucleotide occurs in each genome. Updates are written out during the localization
 * process, sorted and aggregated, and subsequently written out to the pangenome graph.
 * <br/>
 * For sequence 'sequenceIndex' of genome 'genomeIndex', at offset 'offset', the sequence moves from the nucleotide node
 * at the source of a relationship (of type FF, FR, RF or RR) with ID 'relationshipId' to the nucleotide node with ID
 * 'endNodeId'.
 */
class Localization implements Serializable {
    private final long relationshipId;
    private final int genomeIndex;
    private final int sequenceIndex;
    private final int offset;
    private final long endNodeId;

    public Localization(long relationshipId, int genomeIndex, int sequenceIndex, int offset, long endNodeId) {
        this.relationshipId = relationshipId;
        this.genomeIndex = genomeIndex;
        this.sequenceIndex = sequenceIndex;
        this.offset = offset;
        this.endNodeId = endNodeId;
    }

    // No-args constructor needed for Kryo
    @SuppressWarnings("unused")
    public Localization() {
        relationshipId = -1;
        genomeIndex = -1;
        sequenceIndex = -1;
        offset = -1;
        endNodeId = -1;
    }

    public long getRelationshipId() {
        return relationshipId;
    }

    public int getGenomeIndex() {
        return genomeIndex;
    }

    public int getSequenceIndex() {
        return sequenceIndex;
    }

    public int getOffset() {
        return offset;
    }

    public long getEndNodeId() {
        return endNodeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Localization that = (Localization) o;
        return relationshipId == that.relationshipId && genomeIndex == that.genomeIndex && sequenceIndex == that.sequenceIndex && offset == that.offset && endNodeId == that.endNodeId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(relationshipId, genomeIndex, sequenceIndex, offset, endNodeId);
    }

    @Override
    public String toString() {
        return "LocalizationUpdate{" +
            "relationshipId=" + relationshipId +
            ", genomeIndex=" + genomeIndex +
            ", sequenceIndex=" + sequenceIndex +
            ", offset=" + offset +
            ", endNodeId=" + endNodeId +
            '}';
    }
}

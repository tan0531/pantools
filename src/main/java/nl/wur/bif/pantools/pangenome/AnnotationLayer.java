/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.wur.bif.pantools.pangenome;

import htsjdk.samtools.util.Log;
import htsjdk.tribble.AbstractFeatureReader;
import htsjdk.tribble.TribbleException;
import htsjdk.tribble.gff.Gff3Codec;
import htsjdk.tribble.gff.Gff3Feature;
import htsjdk.tribble.readers.LineIterator;
import nl.wur.bif.pantools.index.IndexPointer;
import nl.wur.bif.pantools.index.IndexScanner;
import nl.wur.bif.pantools.pantools.Pantools;
import nl.wur.bif.pantools.sequence.SequenceScanner;
import org.apache.commons.io.FilenameUtils;
import org.neo4j.graphdb.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import static nl.wur.bif.pantools.pangenome.GenomeLayer.locate;
import static nl.wur.bif.pantools.pangenome.create_skip_arrays.create_skip_arrays;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

/**
 * Implements all the functionalities related to the annotation layer of the pangenome
 * 
 * @author Siavash Sheikhizadeh, Eef Jonkheer, Bioinformatics group, Wageningen University, the Netherlands.
 */
public class AnnotationLayer {
    private int num_proteins;
    private static char[] aminoacid_table;
    private static int[] binary;
    private static StringBuilder protein_builder;
    private HashMap<String, Integer>[] sequenceNrs;
    private boolean connectAnnotations;
    private boolean assumeOneMrnaPerCds;

    /**
     * Initializes some variables
     */
    public AnnotationLayer() {
        aminoacid_table = new char[]
          {'K','N','K','N',
           'T','T','T','T',
           'R','S','R','S',
           'I','I','M','I',
           'Q','H','Q','H',
           'P','P','P','P',
           'R','R','R','R',
           'L','L','L','L',
           'E','D','E','D',
           'A','A','A','A',
           'G','G','G','G',
           'V','V','V','V',
           '*','Y','*','Y',
           'S','S','S','S',
           '*','C','W','C',
           'L','F','L','F'
          };
        binary=new int[256];
        binary['A'] = 0; 
        binary['C'] = 1; 
        binary['G'] = 2; 
        binary['T'] = 3; 
    // All degenerate bases will be replaces by 'A' in the translation // TODO: fix this
        protein_builder = new StringBuilder();

        // Suppress all logging from the htsjdk library for Gff3Codec
        Log.setGlobalLogLevel(Log.LogLevel.ERROR);

        // Initialize connectAnnotations and assumeOneMrnaPerCds with default value (false)
        connectAnnotations = false;
        assumeOneMrnaPerCds = false;
    }
   
    /**
     * Implements a comparator for feature nodes
     */
    public class FeatureNodeComparator implements Comparator<Node> {
        @Override
        public int compare(Node x, Node y) {
            int[] x_address = (int[]) x.getProperty("address");
            int[] y_address = (int[]) y.getProperty("address");
            int x_start = x_address[2];
            int y_start = y_address[2];
            int x_end = x_address[3];
            int y_end = y_address[3];

            if (x_start < y_start)
                return -1;
            else if (x_start > y_start)
                return 1;
            else if (x_end < y_end)
                return -1;
            else if (x_end > y_end)
                return 1;
            else
                return 0;
        }
    }

    /**
     * Implements a class for genomic features
     */
    public class feature{
        Node node;
        String ID;
        public feature(Node n, String id) {
            node = n;
            ID = id;
        }
    }
    
    /**
     * Adds genomic features to genomes and generates their proteomes.
     * @param databaseDirectory directory to the pangenome database
     * @param annotationsFile path to the annotations file
     * @param ignoreInvalidFeatures ignore invalid annotation features
     * @param connectAnnotations connect annotations to the nucleotide layer
     * @param assumeOneMrnaPerCds whether to assume that each CDS has only one mRNA in case of absence of mRNA in the annotation file (gff3)
     * @throws IOException if a problem occurs reading the annotations files listed there
     * @throws TribbleException if an annotation file cannot be parsed
     */
    public void addAnnotations(Path databaseDirectory, Path annotationsFile, boolean ignoreInvalidFeatures,
                               boolean connectAnnotations,  boolean assumeOneMrnaPerCds)
            throws IOException, TribbleException {
        Pantools.logger.info("Construct or expand the annotation layer.");

        this.connectAnnotations = connectAnnotations;
        if (!connectAnnotations) {
            Pantools.logger.info("--connect was not included. Gene nodes will not be connected the nucleotide layer.");
        }

        this.assumeOneMrnaPerCds = assumeOneMrnaPerCds;
        if (assumeOneMrnaPerCds) {
            Pantools.logger.info("--assume-one-mrna-per-cds was included. If mRNA is not present in the annotation file between CDS and gene features, each CDS will be connected to a single mRNA.");
        }

        // TODO: validate all annotation files before starting the process (this will require a complete rewrite of this class)

        // start pangenome
        connect_pangenome();
        INDEX_SC = new IndexScanner(INDEX_DB);
        K_SIZE = INDEX_SC.get_K();
        GENOME_SC = new SequenceScanner(GENOME_DB, 1, 1, K_SIZE, INDEX_DB.get_pre_len());
        num_proteins = 0;

        // Create an array in which a hashmap for sequence IDs and their number are stored per genome
        sequenceNrs = new HashMap[GENOME_DB.num_genomes + 1];
        for (int i = 1; i <= GENOME_DB.num_genomes; i++) {
            sequenceNrs[i] = getSequenceNumbers(i);
        }

        // get the pangenome graph variables
        try (Transaction tx = GRAPH_DB.beginTx()) {
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome(pangenome_node, "add_annotations"); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes 
            tx.success();
        }

        final HashMap<Integer, LinkedHashSet<Gff3Feature>> annotationFeatures =
                getAnnotationFeatures(annotationsFile, ignoreInvalidFeatures);

        for (Map.Entry<Integer, LinkedHashSet<Gff3Feature>> entry : annotationFeatures.entrySet()) {
            final int genomeNumber = entry.getKey();
            final LinkedHashSet<Gff3Feature> annotation = entry.getValue();

            Pantools.logger.info("Adding annotation features to the pangenome graph for genome {}.", genomeNumber);
            addAnnotationToPangenome(genomeNumber, annotation, databaseDirectory);
        }

        Pantools.logger.info("Annotated proteins available in {}", databaseDirectory.resolve("proteins"));

        // update the pangenome graph variable and print the annotation overview
        try (Transaction tx = GRAPH_DB.beginTx()) {
            Node pangenomeNode = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            pangenomeNode.removeProperty("num_proteins");
            pangenomeNode.setProperty("num_proteins", num_proteins);
            create_annotation_overview();
            tx.success();
        }

        // close the graph database
        disconnectPangenome();
    }

    /**
     * Get the number of each sequence for a given genome based on its ID.
     * @param genome the genome number to get the sequence numbers for
     * @return a hashmap with the sequence id as key and the number as value
     */
    private HashMap<String, Integer> getSequenceNumbers(int genome) {
        HashMap<String, Integer> sequenceNumbers = new HashMap<>();

        int numSequences = GENOME_DB.num_sequences[genome];
        for (int i = 1; i <= numSequences; i++) {
            try {
                String sequence = GENOME_DB.sequence_titles[genome][i];
                String[] fields = sequence.split("\\s+");
                String id = fields[0];
                sequenceNumbers.put(id, i);
            } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
                Pantools.logger.error("Could not find sequence for genome {} and sequence number {}.", genome, i);
                throw new RuntimeException("Invalid sequence number");
            }
        }

        // print the number of each sequence with Pantools.logger
        Pantools.logger.debug("Sequence numbers for genome {}: ", genome);
        for (String id : sequenceNumbers.keySet()) {
            Pantools.logger.debug("\t{}: {}", id, sequenceNumbers.get(id));
        }

        return sequenceNumbers;
    }

    /**
     * Function to collect all annotation features from the annotation files provided.
     * @param annotationsFile text file with the paths to the annotation files per genome number
     * @param ignore whether to ignore invalid annotation features or not
     * @return Map linking genome numbers to all gff3 features for that genome
     * @throws IOException if one of the provided annotation files does not have a gff or gff3 extension
     */
    HashMap<Integer, LinkedHashSet<Gff3Feature>> getAnnotationFeatures(Path annotationsFile, boolean ignore)
            throws IOException, TribbleException {
        final HashMap<Integer, LinkedHashSet<Gff3Feature>> annotationFeatures = new HashMap<>();
        final HashMap<Integer, Path> annotationPaths = readAnnotationsFile(annotationsFile);
        for (Map.Entry<Integer, Path> entry : annotationPaths.entrySet()) {
            final int genomeNumber = entry.getKey();
            final Path annotationFile = entry.getValue();
            final String extension = FilenameUtils.getExtension(String.valueOf(annotationFile));
            if (!extension.matches("gff|gff3")) {
                Pantools.logger.error("PanTools only accepts .gff and .gff3 files as input annotation files.");
                throw new IOException("Invalid extension for annotation file: " + extension);
            }
            final LinkedHashSet<Gff3Feature> annotation = parseGff(annotationFile);
            validateFeatures(annotation, genomeNumber, ignore);
            annotationFeatures.put(genomeNumber, annotation);
        }
        return annotationFeatures;
    }

    /**
     * Read the annotations file and collect its genome number annotation file pairs.
     * @param annotationsFile text file with genome numbers and their annotation file paths
     * @return Map of genome number / annotation path pairs
     * @throws IOException if the format for the annotations file is invalid
     */
    private HashMap<Integer, Path> readAnnotationsFile(Path annotationsFile) throws IOException {
        Pantools.logger.info("Reading annotation locations file.");
        final HashMap<Integer, Path> annotationPaths = new HashMap<>();
        try(BufferedReader br = Files.newBufferedReader(annotationsFile)) {
            String line;
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if (line.equals("") || line.startsWith("#")) continue;
                final String[] fields = line.split("\\s+"); // split on space or multiple spaces
                try {
                    assert fields.length == 2;
                    final Integer genomeNumber = Integer.valueOf(fields[0]);
                    final Path annotationFile = Paths.get(fields[1]);
                    annotationPaths.put(genomeNumber, annotationFile);
                } catch(Exception e) {
                    Pantools.logger.error("Each line should contain a genome number with its annotation file path, " +
                            "separated by a space.");
                    throw new IOException("Invalid format for the annotation locations file");
                }
            }
        }
        return annotationPaths;
    }

    /**
     * New gff parser based on htsjdk
     * @param annotationFile path to gff3 file
     * @return parsed annotation
     * @throws IOException if file is not found
     * @throws TribbleException if file is not gff3
     * @throws StackOverflowError if occurs, likely due to a feature being its own parent
     */
    private LinkedHashSet<Gff3Feature> parseGff(Path annotationFile) throws IOException, TribbleException,
            StackOverflowError {
        Pantools.logger.info("Parsing annotation file {}", annotationFile.getFileName());
        final LinkedHashSet<Gff3Feature> annotation = new LinkedHashSet<>();
        final Gff3Codec codec = new Gff3Codec();
        final AbstractFeatureReader<Gff3Feature, LineIterator> reader =
                AbstractFeatureReader.getFeatureReader(annotationFile.toString(), null, codec, false);
        for (final Gff3Feature feature : reader.iterator()) {
            annotation.add(feature);
        }
        return annotation;
    }

    /**
     * Finds all invalid features in an annotation, given a genome number.
     * @param annotation the genome annotation to check
     * @param genome the genome number to which the annotation should belong
     * @param ignore whether to ignore invalid features or not
     *
     * This method checks if the locations of each feature in the annotation are
     * within the boundaries of its sequence. If not, the feature is not valid.
     */
    private void validateFeatures(LinkedHashSet<Gff3Feature> annotation, int genome, boolean ignore) {
        Pantools.logger.info("Validating features for genome {}", genome);

        // create hashmap to store the length of each sequence for the given genome
        final HashMap<String, Long> sequenceLengths = getSequenceLengths(genome);

        // create hashset to store invalid features
        final Set<Gff3Feature> invalidFeatures = new HashSet<>();

        // check for each feature if it falls within the boundaries of its sequence
        for (Gff3Feature feature : annotation) {
            final String seqId = feature.getContig();

            // check if the sequence id exists for the given genome
            if (!sequenceLengths.containsKey(seqId)) {
                Pantools.logger.warn("Feature {} is invalid; sequence ID {} does not exist for genome {}.",
                        feature.getID(), seqId, genome);
            }

            // check if the feature is within the boundaries of its sequence
            else if (feature.getStart() < 1 || feature.getEnd() > sequenceLengths.get(seqId)) {
                Pantools.logger.warn("Feature {} does not fit within the boundary of sequence {} for genome {}.",
                        feature.getID(), seqId, genome);
            }

            // continue if feature is valid
            else continue;

            // add invalid features (and descendants) to list
            invalidFeatures.add(feature);
            if (ignore) invalidFeatures.addAll(feature.getDescendents());
        }

        // handle invalid features
        if (!invalidFeatures.isEmpty()) {
            if (ignore) { // remove invalid features and their descendants if ignored
                Pantools.logger.info("Removing {} invalid features and descendants for genome {}",
                        invalidFeatures.size(), genome);
                annotation.removeAll(invalidFeatures);
            } else { // throw exception if one or more features are invalid
                Pantools.logger.error("Invalid features were found in the annotation file for genome {}.", genome);
                throw new RuntimeException("Invalid annotation file");
            }

        }
    }

    /**
     * Get the length of each sequence for a given genome.
     * @param genome the genome number to get the lengths for
     * @return a hashmap with the sequence id as key and the length as value
     */
    private HashMap<String, Long> getSequenceLengths(int genome) {
        HashMap<String, Long> sequenceLengths = new HashMap<>();

        int numSequences = GENOME_DB.num_sequences[genome];
        for (int i = 1; i <= numSequences; i++) {
            try {
                String sequence = GENOME_DB.sequence_titles[genome][i];
                Long length = GENOME_DB.sequence_length[genome][i];
                String[] fields = sequence.split("\\s+");
                String id = fields[0];
                sequenceLengths.put(id, length);
            } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
                Pantools.logger.error("Could not find sequence for genome {} and sequence number {}.", genome, i);
                System.exit(1);
            }
        }

        // print the length of each sequence with Pantools.logger
        Pantools.logger.debug("Sequence lengths for genome {}: ", genome);
        for (String id : sequenceLengths.keySet()) {
            Pantools.logger.debug("\t{}: {}", id, sequenceLengths.get(id));
        }

        return sequenceLengths;
    }

    /**
     * Creates annotation_overview.txt, an overview of the .gff and .gbk files incorporated in the pangenome
     */
    public static void create_annotation_overview() {
        total_genomes = (int) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("num_genomes");
        StringBuilder output_builder = new StringBuilder();
        int annotation_count = 0;
        for (int i = 1; i <= total_genomes; i++) {
            StringBuilder genome_builder = new StringBuilder();
            ResourceIterator<Node> annotationNodes = GRAPH_DB.findNodes(ANNOTATION_LABEL, "genome", i);
            HashSet<String> id_set = new HashSet<>();
            while (annotationNodes.hasNext()) {
                Node annotationNode = annotationNodes.next();
                long genome_node_id = annotationNode.getId();
                String identifier = (String) annotationNode.getProperty("identifier");
                id_set.add(identifier);
                String date = (String) annotationNode.getProperty("date");
                genome_builder.append("\nAnnotation id: ").append(identifier)
                    .append("\nStored in node: ").append(genome_node_id)
                    .append("\nCreation date ").append(date).append("\n");
                annotation_count ++;
            }
            String ids = id_set.toString().replace("[","").replace("]","").replace(", ",",");
            String total = "No annotations";
            if (id_set.size() == 1) {
                total = "1 annotation: ";
            } else if (id_set.size() > 1) {
                total = id_set.size() + " annotations: ";
            }
            output_builder.append("\n#Genome ").append(i).append("\n")
                    .append(total).append(ids).append("\n")
                    .append(genome_builder);
        }

        String anno_str = "Total annotations included in the pangenome: " + annotation_count + "\n";
        write_string_to_file_in_DB(anno_str + output_builder, "annotation_overview.txt");
    }

    /**
     * Adds regular annotation to pangenome
     * @param genomeNr genome number
     * @param annotation annotation parsed by htsjdk
     * @param databaseDirectory path to pangenome database directory
     * @throws IOException if the proteins directory is inaccessible
     */
    private void addAnnotationToPangenome(int genomeNr, LinkedHashSet<Gff3Feature> annotation,
                                          Path databaseDirectory) throws IOException {
        final CoFeatures coFeatures = new CoFeatures(genomeNr, GRAPH_DB);
        final Node annotationNode = createAnnotationNode(genomeNr);

        final Path proteinsDirectory = databaseDirectory.resolve("proteins");
        if (!Files.exists(proteinsDirectory)) {
            Files.createDirectory(proteinsDirectory);
        }

        try {
            Transaction tx = GRAPH_DB.beginTx(); // start database transaction
            int trsc = 0;
            final String annotationId = (String) annotationNode.getProperty("identifier");
            for (Gff3Feature gff3Feature : annotation) {

                if (gff3Feature.isTopLevelFeature()) {

                    // top-level features are guaranteed to have an ID
                    final String featureId = gff3Feature.getID();

                    // get the type of the feature
                    final String featureType = gff3Feature.getType();

                    // do for all top level features, regardless of type
                    final Node featureNode = createFeatureNode(gff3Feature, featureId, featureType, genomeNr, annotationId);

                    //since we have a top level feature, handle child relationships accordingly
                    switch (featureType) {
                        case "gene":
                            featureNode.addLabel(GENE_LABEL);
                            handleTopLevelGene(gff3Feature, featureNode, genomeNr, annotationId);

                            if (gff3Feature.hasCoFeatures()) {
                                coFeatures.add(featureNode);
                            }

                            break;
                        case "tRNA":
                            featureNode.addLabel(TRNA_LABEL);
                            handleTopLevelTrna(gff3Feature, featureNode, genomeNr, annotationId, false);
                            break;
                        case "rRNA":
                            featureNode.addLabel(RRNA_LABEL);
                            handleTopLevelRrna(gff3Feature, featureNode, genomeNr, annotationId, false);
                            break;
                        case "mRNA":
                        case "transcript":
                            featureNode.addLabel(MRNA_LABEL);
                            handleTopLevelMrna(gff3Feature, featureNode, genomeNr, annotationId, false);
                            break;
                        case "exon":
                            featureNode.addLabel(EXON_LABEL);
                            handleTopLevelExon(gff3Feature, featureNode, genomeNr, annotationId, false);
                            break;
                        case "CDS":
                            featureNode.addLabel(CDS_LABEL);
                            handleTopLevelCds(gff3Feature, featureNode, genomeNr, annotationId, false);
                            break;
                        //TODO remove intron from everywhere
                        default:
                            handleMiscFeature(gff3Feature, featureNode, genomeNr, annotationId, false, 1);
                            Pantools.logger.debug("Feature '{}' has unhandled type '{}'.", featureId, featureType);
                            break;
                    }
                }

                trsc++;
                if (trsc >= MAX_TRANSACTION_SIZE) {
                    trsc = 0;
                    tx.success();
                    tx.close();
                    tx = GRAPH_DB.beginTx();
                }
            }

            tx.success();
            tx.close();
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Could not find node in database.");
            throw new RuntimeException("Error in database transaction");
        }

        // add coFeatures to pangenome
        Pantools.logger.debug("Adding {} co-features ({}) to pangenome.", coFeatures.size(), coFeatures.getCoFeatures());
        coFeatures.addCoFeaturesToPangenome();

        try { // start database transaction
            Transaction tx = GRAPH_DB.beginTx();
            final String annotationId = (String) annotationNode.getProperty("identifier");

            //create sets of hashmaps to map feature nodes to gff3features
            ArrayList<Node> geneNodes = getNodes(GENE_LABEL, annotationId);
            ArrayList<Node> mrnaNodes = getNodes(MRNA_LABEL, annotationId);
            ArrayList<Node> trnaNodes = getNodes(TRNA_LABEL, annotationId);
            ArrayList<Node> rrnaNodes = getNodes(RRNA_LABEL, annotationId);

            //create proteins
            int numProteins = setProteinSequences(geneNodes, genomeNr, proteinsDirectory);

            //add numbers of features to annotation node
            int numGenes = geneNodes.size();
            int numTrnas = trnaNodes.size();
            int numRrnas = rrnaNodes.size();
            int numMrnas = mrnaNodes.size();
            annotationNode.setProperty("total_genes", numGenes);
            annotationNode.setProperty("total_tRNAs", numTrnas);
            annotationNode.setProperty("total_rRNAs", numRrnas);
            annotationNode.setProperty("total_mRNAs", numMrnas);
            annotationNode.setProperty("num_proteins", numProteins);

            //close transaction
            tx.success();
            tx.close();

            Pantools.logger.info("Genome {}: {} genes, {} mRNAs, {} tRNAs, {} rRNAs",
                    genomeNr, numGenes, numMrnas, numTrnas, numRrnas);

            //exit if no genes were found
            if (numGenes == 0) {
                Pantools.logger.error("No gene nodes could be created.");
                undo_failed_annotation(annotationNode);
                Pantools.logger.error("None of the final GFF's annotations were included to the pangenome.");
                Pantools.logger.error("Please verify the correctness of the annotation file for {}.", genomeNr);
                throw new RuntimeException("No gene nodes could be created");
            }
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the pangenome database.");
            throw new RuntimeException("Error in database transaction");
        }
    }

    /**
     * Create a new annotation node for a given genome
     * @param genomeNumber number of the genome to create an annotation for
     * @return annotation identifier
     */
    private Node createAnnotationNode(int genomeNumber) {
        Node annotationNode;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            annotationNode = GRAPH_DB.createNode(ANNOTATION_LABEL);
            final Node genomeNode = GRAPH_DB.findNode(GENOME_LABEL, "number", genomeNumber);

            // check for genome node
            if (genomeNode == null) {
                Pantools.logger.error("Genome {} does not exist in the pangenome graph!", genomeNumber);
                throw new RuntimeException("Genome node does not exist");
            }

            // add node properties
            annotationNode.createRelationshipTo(genomeNode, RelTypes.annotates);
            final int degree = genomeNode.getDegree(RelTypes.annotates, Direction.INCOMING);
            final String identifier = String.format("%s_%s", genomeNumber, degree);
            annotationNode.setProperty("date", new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
            annotationNode.setProperty("genome", genomeNumber);
            annotationNode.setProperty("number", degree);
            annotationNode.setProperty("identifier", identifier);

            tx.success();
            Pantools.logger.debug("Added annotation node {} for genome {}", identifier, genomeNumber);
        }
        return annotationNode;
    }

    /**
     * Handles a gene feature that is top level
     * @param geneFeature top level gene gff3 feature to add
     * @param geneNode top level gene node in pangenome to add
     * @param genomeNr genome number to add feature to
     * @param annotationId id for annotation
     */
    private void handleTopLevelGene(Gff3Feature geneFeature, Node geneNode, int genomeNr, String annotationId) {
        //a gene node has mRNA, rRNA or tRNA children
        for (Gff3Feature feature : geneFeature.getChildren()) {

            //get the feature type
            String featureType = feature.getType();

            //since not top-level features are not guaranteed to have an ID, test this
            String featureId = feature.getID();
            if (featureId == null) {
                featureId = geneFeature.getID() + "-" + featureType;
            }

            label:
            switch (featureType) {
                case "mRNA": case "transcript":
                    Node mrnaNode = createFeatureNode(feature, featureId, featureType, genomeNr, annotationId);
                    geneNode.createRelationshipTo(mrnaNode, RelTypes.is_parent_of);
                    geneNode.createRelationshipTo(mrnaNode, RelTypes.codes_for);

                    mrnaNode.addLabel(MRNA_LABEL);
                    handleTopLevelMrna(feature, mrnaNode, genomeNr, annotationId, true);
                    break;
                case "rRNA":
                    Node rrnaNode = createFeatureNode(feature, featureId, featureType, genomeNr, annotationId);
                    geneNode.createRelationshipTo(rrnaNode, RelTypes.is_parent_of);

                    rrnaNode.addLabel(RRNA_LABEL);
                    handleTopLevelRrna(feature, rrnaNode, genomeNr, annotationId, true);
                    break;
                case "tRNA":
                    Node trnaNode = createFeatureNode(feature, featureId, featureType, genomeNr, annotationId);
                    geneNode.createRelationshipTo(trnaNode, RelTypes.is_parent_of);

                    trnaNode.addLabel(TRNA_LABEL);
                    handleTopLevelTrna(feature, trnaNode, genomeNr, annotationId, true);
                    break;
                case "CDS":
                    //unusual case, CDS is direct child of gene without mRNA
                    //we can 1) assume every CDS has its own mRNA, or 2) assume there is only one mRNA per gene

                    if (!assumeOneMrnaPerCds) { //default: assumes all CDS features belong to one mRNA

                        //break if mRNA already exists for this genome and annotation
                        for (Relationship rel : geneNode.getRelationships(RelTypes.is_parent_of)) {
                            Node child = rel.getEndNode();
                            if (child.hasLabel(MRNA_LABEL) && child.getProperty("genome").equals(genomeNr) &&
                                    child.getProperty("annotation_id").equals(annotationId)) {
                                Pantools.logger.debug("mRNA node ({}) already exists for gene node ({}) in genome {} and annotation {}, skipping.",
                                        child.getProperty("id"), geneNode.getProperty("id"), genomeNr, annotationId);
                                break label;
                            }
                        }

                        //TODO: in theory there can be another exon node that is a child of gene, but this is not handled (it currently becomes a misc feature)

                        //make mRNA node with original featureId
                        Node newMrnaNode = createFeatureNode(geneFeature, featureId, "mRNA", genomeNr, annotationId);

                        geneNode.createRelationshipTo(newMrnaNode, RelTypes.is_parent_of);
                        geneNode.createRelationshipTo(newMrnaNode, RelTypes.codes_for);

                        newMrnaNode.addLabel(MRNA_LABEL);

                        handleTopLevelMrna(geneFeature, newMrnaNode, genomeNr, annotationId, true);
                    } else { // assumes one mRNA node per CDS feature
                        Node newMrnaNode = createFeatureNode(feature, featureId, "mRNA", genomeNr, annotationId);

                        geneNode.createRelationshipTo(newMrnaNode, RelTypes.is_parent_of);
                        geneNode.createRelationshipTo(newMrnaNode, RelTypes.codes_for);

                        newMrnaNode.addLabel(MRNA_LABEL);

                        //create exon node as well //TODO: in theory there can be another exon node that is a child of gene, but this is not handled
                        String exonId = featureId + "-exon";
                        Node exonNode = createFeatureNode(feature, exonId, "exon", genomeNr, annotationId);
                        newMrnaNode.createRelationshipTo(exonNode, RelTypes.is_parent_of);

                        exonNode.addLabel(EXON_LABEL);

                        //now continue with CDS
                        String cdsId = featureId + "-cds";
                        Node cdsNode = createFeatureNode(feature, cdsId, featureType, genomeNr, annotationId);
                        newMrnaNode.createRelationshipTo(cdsNode, RelTypes.is_parent_of);
                        cdsNode.createRelationshipTo(newMrnaNode, RelTypes.contributes_to);

                        cdsNode.addLabel(CDS_LABEL);

                        handleTopLevelCds(feature, cdsNode, genomeNr, annotationId, true);
                    }
                    break;
                default:
                    Node rnaNode = createFeatureNode(feature, featureId, featureType, genomeNr, annotationId);
                    geneNode.createRelationshipTo(rnaNode, RelTypes.is_parent_of);

                    handleMiscFeature(feature, rnaNode, genomeNr, annotationId, true, 2);
                    break;
            }
        }
    }

    /**
     * Handles an mRNA feature that is top level
     * @param mrnaFeature top level mRNA gff3 feature to add
     * @param mrnaNode top level mRNA node in pangenome to add
     * @param genomeNr genome number to add feature to
     * @param annotationId id for annotation
     * @param hasParent whether this mRNA feature has a parent feature
     */
    private void handleTopLevelMrna(Gff3Feature mrnaFeature, Node mrnaNode, int genomeNr, String annotationId, boolean hasParent) {
        //if mRNA has no parent, artificially create gene as parent
        if (!hasParent) {

            //this is a top-level feature, so we are guaranteed to have an ID
            String geneId = mrnaFeature.getID() + "-gene";

            Node geneNode = createFeatureNode(mrnaFeature, geneId, "gene", genomeNr, annotationId);
            geneNode.addLabel(GENE_LABEL);
            geneNode.createRelationshipTo(mrnaNode, RelTypes.is_parent_of);
            geneNode.createRelationshipTo(mrnaNode, RelTypes.codes_for);
        }

        //an mRNA node has both exon and CDS children
        for (Gff3Feature feature : mrnaFeature.getChildren()) {

            //since not top-level features are not guaranteed to have an ID, test this
            String featureId = feature.getID();
            if (featureId == null) {
                featureId = mrnaFeature.getID() + "-" + feature.getType();
            }

            //get feature type
            String featureType = feature.getType();

            if (featureType.equals("exon")) {
                Node exonNode = createFeatureNode(feature, featureId, featureType, genomeNr, annotationId);
                mrnaNode.createRelationshipTo(exonNode, RelTypes.is_parent_of);

                exonNode.addLabel(EXON_LABEL);
                handleTopLevelExon(feature, exonNode, genomeNr, annotationId, true);
            } else if (featureType.equals("CDS")) {
                Node cdsNode = createFeatureNode(feature, featureId, featureType, genomeNr, annotationId);
                mrnaNode.createRelationshipTo(cdsNode, RelTypes.is_parent_of);
                cdsNode.createRelationshipTo(mrnaNode, RelTypes.contributes_to);

                cdsNode.addLabel(CDS_LABEL);
                handleTopLevelCds(feature, cdsNode, genomeNr, annotationId, true);
            } else {
                Node miscNode = createFeatureNode(feature, featureId, featureType, genomeNr, annotationId);
                mrnaNode.createRelationshipTo(miscNode, RelTypes.is_parent_of);

                handleMiscFeature(feature, miscNode, genomeNr, annotationId, true, 3);
            }
        }
    }

    /**
     * Handles an exon feature that is top level
     * @param exonFeature top level exon gff3 feature to add
     * @param exonNode top level exon node in pangenome to add
     * @param genomeNr genome number to add feature to
     * @param annotationId id for annotation
     * @param hasParent whether this exon feature has a parent feature
     */
    private void handleTopLevelExon(Gff3Feature exonFeature, Node exonNode, int genomeNr, String annotationId, boolean hasParent) {
        //if exon has no parent, artificially create mRNA as parent and gene as parent of the new mRNA
        if (!hasParent) {
            //keep mrnaId same as top-level feature Id for linking data to proteins or genes
            String mrnaId = (String) exonNode.getProperty("id");

            // update rest of the feature Ids by adding a suffix to mrnaId
            String geneId = mrnaId + "-gene";

            // add suffix to the exon feature Id in the Neo4j db
            exonNode.setProperty("id", mrnaId + "-exon");

            Node mrnaNode = createFeatureNode(exonFeature, mrnaId, "mRNA", genomeNr, annotationId);
            mrnaNode.addLabel(MRNA_LABEL);
            mrnaNode.createRelationshipTo(exonNode, RelTypes.is_parent_of);

            Node geneNode = createFeatureNode(exonFeature, geneId, "gene", genomeNr, annotationId);
            geneNode.addLabel(GENE_LABEL);
            geneNode.createRelationshipTo(mrnaNode, RelTypes.is_parent_of);
            geneNode.createRelationshipTo(mrnaNode, RelTypes.codes_for);
        }

        //an exon node has no children
        for (Gff3Feature feature : exonFeature.getChildren()) {
            Pantools.logger.warn("Exon feature {} has a child: {}.", exonFeature.getID(), feature.getID());
        }
    }

    /**
     * Handles an CDS feature that is top level
     * @param cdsFeature top level CDS gff3 feature to add
     * @param cdsNode top level CDS node in pangenome to add
     * @param genomeNr genome number to add feature to
     * @param annotationId id for annotation
     * @param hasParent whether this CDS feature has a parent feature
     */
    private void handleTopLevelCds(Gff3Feature cdsFeature, Node cdsNode, int genomeNr, String annotationId, boolean hasParent) {
        //if CDS has no parent, artificially create mRNA as parent and gene as parent of the new mRNA
        if (!hasParent) {
            //keep mrnaId same as top-level feature Id for linking data to proteins or genes
            String mrnaId = (String) cdsNode.getProperty("id");

            // update rest of the feature Ids by adding a suffix to mrnaId
            String exonId = mrnaId + "-exon";
            String geneId = mrnaId + "-gene";

            // add suffix to the CDS feature Id in the Neo4j db
            cdsNode.setProperty("id", mrnaId + "-cds");

            Node mrnaNode = createFeatureNode(cdsFeature, mrnaId, "mRNA", genomeNr, annotationId);
            mrnaNode.addLabel(MRNA_LABEL);
            mrnaNode.createRelationshipTo(cdsNode, RelTypes.is_parent_of);
            cdsNode.createRelationshipTo(mrnaNode, RelTypes.contributes_to);

            Node exonNode = createFeatureNode(cdsFeature, exonId, "exon", genomeNr, annotationId);
            exonNode.addLabel(EXON_LABEL);
            mrnaNode.createRelationshipTo(exonNode, RelTypes.is_parent_of);

            Node geneNode = createFeatureNode(cdsFeature, geneId, "gene", genomeNr, annotationId);
            geneNode.addLabel(GENE_LABEL);
            geneNode.createRelationshipTo(mrnaNode, RelTypes.is_parent_of);
            geneNode.createRelationshipTo(mrnaNode, RelTypes.codes_for);
        }

        //a CDS node has no children
        for (Gff3Feature feature : cdsFeature.getChildren()) {
            Pantools.logger.warn("CDS feature {} (exon) has a child: {}.", cdsFeature.getID(), feature.getID());
        }
    }

    /**
     * Handles an rRNA feature that is top level
     * @param rrnaFeature top level rRNA gff3 feature to add
     * @param rrnaNode top level rRNA node in pangenome to add
     * @param genomeNr genome number to add feature to
     * @param annotationId id for annotation
     * @param hasParent whether this rRNA feature has a parent feature
     */
    private void handleTopLevelRrna(Gff3Feature rrnaFeature, Node rrnaNode, int genomeNr, String annotationId, boolean hasParent) {
        //if rRNA has no parent, artificially create gene as parent
        if (!hasParent) {

            //this is a top-level feature, so we are guaranteed to have an ID
            String geneId = rrnaFeature.getID() + "-gene";

            Node geneNode = createFeatureNode(rrnaFeature, geneId, "gene", genomeNr, annotationId);
            geneNode.addLabel(GENE_LABEL);
            geneNode.createRelationshipTo(rrnaNode, RelTypes.is_parent_of);
        }

        //a rRNA node has no children
        for (Gff3Feature feature : rrnaFeature.getChildren()) {
            //since not top-level features are not guaranteed to have an ID, test this
            String featureId = feature.getID();
            if (featureId == null) {
                featureId = rrnaFeature.getID() + "-" + feature.getType();
            }

            //get the feature type
            String featureType = feature.getType();

            if (featureType.equals("exon")) {
                Node exonNode = createFeatureNode(feature, featureId, featureType, genomeNr, annotationId);
                rrnaNode.createRelationshipTo(exonNode, RelTypes.is_parent_of);

                exonNode.addLabel(EXON_LABEL);
                handleTopLevelExon(feature, exonNode, genomeNr, annotationId, true);
            } else {
                Node miscNode = createFeatureNode(feature, featureId, featureType, genomeNr, annotationId);
                rrnaNode.createRelationshipTo(miscNode, RelTypes.is_parent_of);

                handleMiscFeature(feature, miscNode, genomeNr, annotationId, true, 3);
            }
        }
    }

    /**
     * Handles an tRNA feature that is top level
     * @param trnaFeature top level tRNA gff3 feature to add
     * @param trnaNode top level tRNA node in pangenome to add
     * @param genomeNr genome number to add feature to
     * @param annotationId id for annotation
     * @param hasParent whether this tRNA feature has a parent feature
     */
    private void handleTopLevelTrna(Gff3Feature trnaFeature, Node trnaNode, int genomeNr, String annotationId, boolean hasParent) {
        //if tRNA has no parent, artificially create gene as parent
        if (!hasParent) {

            //this is a top-level feature, so we are guaranteed to have an ID
            String geneId = trnaFeature.getID() + "-gene";

            Node geneNode = createFeatureNode(trnaFeature, geneId, "gene", genomeNr, annotationId);
            geneNode.addLabel(GENE_LABEL);
            geneNode.createRelationshipTo(trnaNode, RelTypes.is_parent_of);
        }

        //a tRNA node has no children
        for (Gff3Feature feature : trnaFeature.getChildren()) {
            //since not top-level features are not guaranteed to have an ID, test this
            String featureId = feature.getID();
            if (featureId == null) {
                featureId = trnaFeature.getID() + "-" + feature.getType();
            }

            //get the feature type
            String featureType = feature.getType();

            if (featureType.equals("exon")) {
                Node exonNode = createFeatureNode(feature, featureId, featureType, genomeNr, annotationId);
                trnaNode.createRelationshipTo(exonNode, RelTypes.is_parent_of);

                exonNode.addLabel(EXON_LABEL);
                handleTopLevelExon(feature, exonNode, genomeNr, annotationId, true);
            } else {
                Node miscNode = createFeatureNode(feature, featureId, featureType, genomeNr, annotationId);
                trnaNode.createRelationshipTo(miscNode, RelTypes.is_parent_of);

                handleMiscFeature(feature, miscNode, genomeNr, annotationId, true, 3);
            }
        }
    }

    /**
     * Handles an misc feature that is on level X
     * @param miscFeature misc gff3 feature to add
     * @param miscNode misc node in pangenome to add
     * @param genomeNr genome number to add feature to
     * @param annotationId id for annotation
     * @param hasParent whether this misc feature has a parent feature
     * @param level level of the misc feature
     *              1 = gene-like, 2 = mRNA-like, 3 = exon-, CDS-like
     */
    private void handleMiscFeature(Gff3Feature miscFeature, Node miscNode, int genomeNr, String annotationId, boolean hasParent, int level) {
        //handle misc feature according to level
        switch (level) {
            case 1:
                handleTopLevelGene(miscFeature, miscNode, genomeNr, annotationId);
                break;
            case 2:
                handleTopLevelMrna(miscFeature, miscNode, genomeNr, annotationId, hasParent);
                break;
            case 3:
                handleTopLevelExon(miscFeature, miscNode, genomeNr, annotationId, hasParent);
                break;
            default:
                Pantools.logger.error("Unexpectedly, {} has level {}.", miscFeature.getID(), level);
                throw new RuntimeException("Unexpected level for miscellaneous feature");
        }
    }

    /**
     * Lists all nodes with a specific label
     * @param label label to look for
     * @param annotationId genome number the node should belong to
     * @return all nodes with specific label
     */
    private ArrayList<Node> getNodes(Label label, String annotationId) {
        ArrayList<Node> nodes = new ArrayList<>();
        ResourceIterator<Node> nodeIterator = GRAPH_DB.findNodes(label);
        while (nodeIterator.hasNext()) {
            Node node = nodeIterator.next();
            String nodeAnnotationId = (String) node.getProperty("annotation_id");
            if (nodeAnnotationId.equals(annotationId)) {
                nodes.add(node);
            }
        }
        return nodes;
    }

    /**
     * Creates feature nodes for a given feature
     * @param gff3Feature the feature to add to pangenome
     * @param featureId the ID of the feature (will OVERRIDE the ID of gff3Feature)
     * @param featureType the type of the feature (will OVERRIDE the type of gff3Feature)
     * @param genomeNr genome number to add feature to
     * @param annotationId id for annotation
     * @return created feature node in pangenome
     */
    private Node createFeatureNode(Gff3Feature gff3Feature, String featureId, String featureType, int genomeNr, String annotationId) {
        Pantools.logger.debug("Creating node for {}", featureId);

        int[] address = new int[4];
        address[0] = genomeNr;
        String[] name;
        String[] locusTag;

        //get parameters for feature
        String seq = gff3Feature.getContig();
        String source = gff3Feature.getSource();
        int start = gff3Feature.getStart();
        int end = gff3Feature.getEnd();
        double score = gff3Feature.getScore();
        String strand = gff3Feature.getStrand().toString();
        int phase = gff3Feature.getPhase();
        Map<String, List<String>> attributes = gff3Feature.getAttributes();

        //set address
        address[1] = sequenceNrs[address[0]].get(seq);
        address[2] = start;
        address[3] = end; // TODO: check if location physically possible for sequence

        //create node and add parameters
        Node featureNode = GRAPH_DB.createNode(FEATURE_LABEL);
        featureNode.setProperty("annotation_id", annotationId);
        featureNode.setProperty("source", source);
        featureNode.setProperty("genome", address[0]);
        featureNode.setProperty("sequence", address[1]);
        featureNode.setProperty("begin", address[2]);
        featureNode.setProperty("end", address[3]);
        featureNode.setProperty("score", score);
        featureNode.setProperty("address", address);
        featureNode.setProperty("type", featureType);
        featureNode.setProperty("phase", phase);
        featureNode.setProperty("strand", strand);
        featureNode.setProperty("length", (end - start + 1));
        featureNode.setProperty("attribute", attributes.toString());
        if (featureId != null) { //should only rarely happen
            featureNode.setProperty("id", featureId);
        } else {
            featureNode.setProperty("id", featureType + "_missing_id");
        }
        if (attributes.containsKey("locus_tag")) {
            locusTag = Arrays.stream(attributes.get("locus_tag").toArray()).map(Object::toString).toArray(String[]::new);
            featureNode.setProperty("locus_tag", locusTag);
        } else {
            locusTag = new String[]{""};
            featureNode.setProperty("locus_tag", locusTag);
        }
        if (attributes.containsKey("Name")) {
            name = Arrays.stream(attributes.get("Name").toArray()).map(Object::toString).toArray(String[]::new);
            featureNode.setProperty("name", name);
        } else {
            name = new String[]{""};
            featureNode.setProperty("name", name);
        }

        //if connectAnnotations is true, connect feature to nucleotide nodes
        if (connectAnnotations) {
            connectAnnotations(featureNode);
        }

        return featureNode;
    }

    /**
     * Connect annotation for a feature node
     * @param featureNode feature node in pangenome with address and length
     */
    private void connectAnnotations(Node featureNode) {
        int[] address = (int[]) featureNode.getProperty("address");
        int length = (int) featureNode.getProperty("length");

        IndexPointer startPtr = locate(GRAPH_DB, GENOME_SC, INDEX_SC, address[0], address[1], address[2] - 1);
        int offset = startPtr.offset;
        Node node = GRAPH_DB.getNodeById(startPtr.node_id);
        Relationship rel = featureNode.createRelationshipTo(node, RelTypes.starts);
        rel.setProperty("offset", offset);
        rel.setProperty("genomic_position", address[2]);
        rel.setProperty("forward", startPtr.canonical);
        IndexPointer stopPtr = locate(GRAPH_DB, GENOME_SC, INDEX_SC, address[0], address[1], address[3] - 1);
        rel = featureNode.createRelationshipTo(GRAPH_DB.getNodeById(stopPtr.node_id), RelTypes.stops);
        rel.setProperty("offset", stopPtr.offset);
        rel.setProperty("genomic_position", address[2] + length - 1);
        rel.setProperty("forward", stopPtr.canonical);
    }

    /**
     * Use in case something went wrong during the annotation and you want to rollback the latest annotation
     * @param annotation_node an 'annotation' node
     */
    private void undo_failed_annotation(Node annotation_node) {
        int trsc = 0;
        long node_counter = 0;
        Transaction tx = GRAPH_DB.beginTx(); // start database transaction
        try {
            String id = (String) annotation_node.getProperty("identifier");
            Iterable<Relationship> anno_relations = annotation_node.getRelationships();
            for (Relationship rel : anno_relations) {
                rel.delete();
            }
            annotation_node.delete();
            ResourceIterator<Node> annotated_nodes = GRAPH_DB.findNodes(FEATURE_LABEL, "annotation_id", id);
            while (annotated_nodes.hasNext()) {
                node_counter ++;
                Node new_node = annotated_nodes.next();
                Iterable<Relationship> new_relations = new_node.getRelationships();
                for (Relationship rel : new_relations) {
                    rel.delete();
                    trsc ++;
                    if (trsc >= 100000) { // 100k actions
                        tx.success();
                        tx.close();
                        trsc = 0;
                        tx = GRAPH_DB.beginTx(); // start a new database transaction
                    }
                }
                trsc ++;
                if (trsc >= 100000) { // 100k actions
                    tx.success();
                    tx.close();
                    trsc = 0;
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                }
                if (node_counter % 5000 == 0){
                    System.out.print("\rRolling back annotated features: " + node_counter);
                }
            }
        } finally {
            tx.close();
        }
    }


    /**
     * Parses a GenBank file and annotates the genomes at the same time.
     * 
     * @param genome The genome number to be annotated 
     * @param annotation_id The ID of annotation 
     * @param log_file A log file for annotation process
     * @param annotation_file Path to the GenBank file
     * @param proteins_directory The directory to write protein FASTA files in 
     */
    private void parse_gbk(int genome, String annotation_id, BufferedWriter log_file, String annotation_file, String proteins_directory, Node annotation_node) {
        int i, trsc, num_genes, num_mRNAs, num_tRNAs, num_rRNAs, feature_len, offset;
        int[] address = new int[4];
        String sequence_id;
        Node seq_node = null, gene_node = null, feature_node = null;
        String[] fields, coordinates;
        String strand, line, protein_id = "";
        StringBuilder protein = new StringBuilder();
        IndexPointer start_ptr, stop_ptr;
        boolean complement;
        address[0] = genome;
        num_genes = num_mRNAs = num_tRNAs = num_rRNAs = 0;
        try (BufferedReader in = new BufferedReader(new FileReader(annotation_file))) {
             BufferedWriter out = new BufferedWriter(new FileWriter(proteins_directory + "/proteins_" + address[0] + ".fasta"));
            // for each record of gff file
            while (in.ready()) {
                try (Transaction tx2 = GRAPH_DB.beginTx()) {
                    for (trsc = 0; trsc < MAX_TRANSACTION_SIZE && (line = in.readLine()) != null; ++trsc) {
                        line = line.trim();
                        if (line.equals("") || line.charAt(0) == '#') // if line is empty or a comment skip it
                            continue;
                        if (line.startsWith("LOCUS")) {
                            fields = line.split("\\s+");
                            sequence_id = fields[1];
                            address[1] = sequenceNrs[address[0]].get(sequence_id);
                            switch (address[1]) {
                                case -1:
                                    Pantools.logger.error("More than one sequence contain {} in their ID in genome {}.", sequence_id, address[0]); // usually organal genes
                                    throw new RuntimeException("Too many sequences with the same ID");
                                case 0:
                                    Pantools.logger.error("Failed to find Sequence ID = {} in genome {}.", sequence_id, address[0]); // usually organal genes
                                    throw new RuntimeException("Sequence ID not found");
                                default:
                                    seq_node = GRAPH_DB.findNode(SEQUENCE_LABEL, "number", address[1]);
                            }
                        } else {
                            if(!line.startsWith("                     ")) {
                                if(line.startsWith("     gene") || line.startsWith("     mRNA") || line.startsWith("     tRNA") || line.startsWith("     rRNA")) {
                                    complement = line.contains("complement");
                                    fields = line.replaceAll("[^0-9]+", " ").trim().split("\\s");
                                    address[2] = Integer.parseInt(fields[0]);
                                    address[3] = Integer.parseInt(fields[fields.length - 1]);
                                    strand = complement ?  "-" : "+";
                                    feature_len = address[3] - address[2] + 1;
                                    feature_node = GRAPH_DB.createNode(FEATURE_LABEL);
                                    feature_node.setProperty("address", address);
                                    feature_node.setProperty("strand", strand);
                                    feature_node.setProperty("length", feature_len);
                                    feature_node.setProperty("annotation_id", annotation_id);
                                    feature_node.setProperty("genome",address[0]);
                                    if(line.startsWith("     gene")) {
                                        ++num_genes;
                                        gene_node = feature_node;
                                        gene_node.addLabel(GENE_LABEL);
                                        feature_node.setProperty("type", "gene");
                                    } else if(line.startsWith("     mRNA")) {
                                        ++num_mRNAs;
                                        feature_node.addLabel(MRNA_LABEL);
                                        gene_node.createRelationshipTo(feature_node, RelTypes.codes_for);
                                        feature_node.setProperty("type", "mRNA");
                                    } else if(line.startsWith("     tRNA")) {
                                        ++num_tRNAs;
                                        feature_node.addLabel(TRNA_LABEL);
                                        feature_node.setProperty("type", "tRNA");
                                    } else if(line.startsWith("     rRNA")) {
                                        ++num_rRNAs;
                                        feature_node.addLabel(RRNA_LABEL);
                                        feature_node.setProperty("type", "rRNA");
                                    } 
                                } 
                            } else {
                                    if(line.startsWith("                     /gene"))
                                        feature_node.setProperty("id", line.substring(27));
                                    else if(line.startsWith("                     /locus_tag"))
                                        feature_node.setProperty("name", line.substring(32));
                                    else if(line.startsWith("                     /protein_id"))
                                        feature_node.setProperty("protein_ID", protein_id = line.substring(33));
                                    else if(line.startsWith("                     /product"))
                                        feature_node.setProperty("product", line.substring(30));
                                    else if(line.startsWith("                     /note"))
                                        feature_node.setProperty("note", line.substring(27));
                                    else if(line.startsWith("                     /translation")) {
                                            protein.setLength(0);
                                            protein.append(line.split("/translation=")[1].replaceAll("\"", ""));
                                            while ((line = in.readLine()) != null && !line.endsWith("\"")) {
                                                line = line.trim();
                                                if (line.equals(""))
                                                    continue;
                                                if (line.endsWith("\"")) {
                                                    protein.append(line.replaceAll("\"", ""));
                                                    break;
                                                } else
                                                    protein.append(line);
                                            }
                                            //Pantools.logger.info(protein);
                                            String protein_str = protein.toString();
                                            if (protein_str.endsWith("*")) {
                                                protein_str = protein_str.replaceFirst(".$",""); // removes last character
                                            }
                                            feature_node.setProperty("protein", protein_str);
                                            feature_node.setProperty("protein_length", protein_str);
                                            out.write(">" + protein_id + "\n" + protein_str + "\n");
                                    }
                            }
                        }
                        if (trsc % 987 == 1)
                            System.out.print("\r" + address[0] + "\t" + num_genes + "\t" + num_mRNAs + "\t" + num_tRNAs + "\t" + num_rRNAs + "\t");
                    }// for trsc
                    tx2.success();
                } // tx2
            } // while lines
            in.close();
            out.close();
            System.out.println("\r" + address[0] + "\t" + num_genes + "\t" + num_mRNAs + "\t" + num_tRNAs + "\t" + num_rRNAs + "\t");
            log_file.write("Genome "+address[0] + " : " + num_genes + " genes\t" + num_mRNAs + " mRNAs\t" + num_tRNAs + " tRNAs\t" + num_rRNAs + " rRNAs\n");
            annotation_node.setProperty("num_proteins", num_mRNAs);
            annotation_node.setProperty("total_mRNAs", num_mRNAs);
            annotation_node.setProperty("total_genes", num_genes);
            annotation_node.setProperty("total_tRNAs",  num_tRNAs);
            annotation_node.setProperty("total_rRNAs", num_rRNAs);            
            log_file.write("----------------------------------------------------\n");
        } catch (IOException ioe) {
            Pantools.logger.error("Could not open {}!", annotation_file);
            throw new RuntimeException("Could not open file");
        }
    }

    /**
     * Translates and adds protein sequence to the mRNA nodes and writes them on disk 
     * 
     * @param geneNodes A list of all the annotated genes
     * @param genome The number of the genome is currently being annotated
     * @param proteinDir The directory where protein FASTA files are written
     */
    private Integer setProteinSequences(ArrayList<Node> geneNodes, int genome, Path proteinDir) {
        Node mrnaNode;
        Node geneNode;
        int trsc;
        int numIsoforms;
        int geneStartPos;
        String protein;
        String mrnaId;
        int[] address;
        boolean isCoFeature;
        StringBuilder geneBuilder = new StringBuilder();
        String mrnaSeq;
        HashMap<String,Integer> idOccurrences = new HashMap<>();
        int proteinCounter = 0;
        HashSet<String> processedGenes = new HashSet<>();

        Pantools.logger.info("Adding protein sequences for genome {}.", genome);
        Pantools.logger.debug("geneNodes.size() = {}", geneNodes.size());

        try (BufferedWriter out = new BufferedWriter(new FileWriter(proteinDir + "/proteins_" + genome + ".fasta"))) {

            Iterator<Node> keys = geneNodes.iterator();
            Transaction tx = GRAPH_DB.beginTx();
            try {
                trsc = 0;
                while (keys.hasNext()) {
                    geneNode = keys.next();
                    geneBuilder.setLength(0);

                    String geneId = (String) geneNode.getProperty("id");
                    address = (int[]) geneNode.getProperty("address");
                    geneStartPos = address[2];
                    address[2] -= 1;
                    address[3] -= 1;
                    isCoFeature = geneNode.hasLabel(COFEATURE_LABEL);

                    Pantools.logger.debug("geneNode.getProperty(\"id\") = {}", geneId);

                    if (processedGenes.contains(geneId)) {
                        Pantools.logger.debug("Gene {} already processed.", geneId);
                        continue;
                    }

                    GENOME_SC.get_sub_sequence(geneBuilder, address[0], address[1], address[2], address[3] - address[2] + 1, true);

                    Pantools.logger.trace("address = {}", Arrays.toString(address));
                    Pantools.logger.trace("geneBuilder.length() = {}", geneBuilder.length());
                    StringBuilder title = new StringBuilder();
                    GENOME_SC.get_sequence_title(title, address[0], address[1]);
                    Pantools.logger.trace("title = {}", title);
                    StringBuilder seq = new StringBuilder();
                    GENOME_SC.get_sub_sequence(seq, address[0], address[1], address[2], address[3] - address[2] + 1, true);
                    Pantools.logger.trace("seq = {}", seq);
                    Pantools.logger.trace("genomeDb.sequence_length[g][s] = {}", GENOME_DB.sequence_length[address[0]][address[1]]);

                    if (geneBuilder.length() == 0) {
                        Pantools.logger.debug("No sequence for gene {} could be found.", geneNode.getProperty("id"));
                        continue;
                    }
                    numIsoforms = 0;

                    for (Relationship rel1 : geneNode.getRelationships(RelTypes.codes_for, Direction.OUTGOING)) {
                        mrnaNode = rel1.getEndNode();
                        mrnaId = (String) mrnaNode.getProperty("id");

                        Pantools.logger.debug("mrnaId = {} (address: {})", mrnaId, Arrays.toString((int[]) mrnaNode.getProperty("address")));

                        if (mrnaNode.hasRelationship(RelTypes.contributes_to, Direction.INCOMING)) {
                            ++numIsoforms;
                            geneNode.addLabel(CODING_GENE_LABEL);

                            // get protein sequence
                            mrnaSeq = getMrnaSequence(mrnaId, geneBuilder, geneStartPos, mrnaNode, isCoFeature);
                            protein = translate(new StringBuilder(mrnaSeq));

                            // log
                            Pantools.logger.trace("mrnaSeq = {}", mrnaSeq);
                            Pantools.logger.trace("protein = {}", protein);

                            if (protein.length() > 0) {
                                if (protein.endsWith("*")) {
                                    protein = protein.substring(0, protein.length() - 1); // remove stop codon
                                } else {
                                    Pantools.logger.debug("Protein {} has no stop codon.", mrnaId);
                                }
                                if (!protein.startsWith("M")) {
                                    Pantools.logger.debug("Protein {} has no start codon.", mrnaId);
                                }

                                //for historic reasons keeping both num_proteins and proteinCounter
                                ++num_proteins;
                                proteinCounter++;

                                Pantools.logger.debug("Protein {}", proteinCounter);

                                if (idOccurrences.containsKey(mrnaId)) {
                                    idOccurrences.put(mrnaId, idOccurrences.get(mrnaId) + 1);
                                } else {
                                    idOccurrences.put(mrnaId, 1);
                                }
                                int occurances = idOccurrences.get(mrnaId);
                                if (occurances > 1) {
                                    mrnaId += "_" + (occurances - 1);
                                }

                                out.write(">" + mrnaId + "\n");
                                write_fasta(out, protein, 60);

                                mrnaNode.setProperty("nucleotide_sequence", mrnaSeq);
                                mrnaNode.setProperty("protein_sequence", protein);
                                mrnaNode.setProperty("protein_ID", mrnaId);
                                mrnaNode.setProperty("protein_length", protein.length());
                            }
                        }

                    }
                    geneNode.setProperty("isoforms_num", numIsoforms);

                    // add gene to processedGenes list
                    processedGenes.add(geneId);

                    trsc++;
                    if (trsc >= MAX_TRANSACTION_SIZE) {
                        trsc = 0;
                        tx.success();
                        tx.close();
                        tx = GRAPH_DB.beginTx();
                    }
                }
            } finally {
                tx.success();
                tx.close();
            }
        } catch (IOException ioe) {
            Pantools.logger.info(ioe.getMessage());
        }   
        
        boolean first = true;
        for (String protein_id : idOccurrences.keySet()) {
            int occurrences =  idOccurrences.get(protein_id);
            Pantools.logger.trace("{} {}",  protein_id, occurrences);
            if (occurrences < 2) {
                continue;
            }
            if (first) {
                Pantools.logger.info("One or more mRNAs have the same identifier; this is caused by incorrect naming in your GFF file.");
                first = false;
            }

            Pantools.logger.debug("Protein {} was found {} times.", protein_id, occurrences);
        }
        return proteinCounter;
    }

    /**
     * Creates a mRNA sequence from an mRNA node by combining the CDS sequences, taking strand and phase into account.
     * @param mrnaId the mRNA ID
     * @param geneBuilder the gene sequence
     * @param geneStartPos the gene start position
     * @param mrnaNode the mRNA node
     * @param isCoFeature whether the gene is a co-feature (i.e. whether to use the strand information for concatenating the CDS sequences)
     * @return the mRNA sequence
     */
    private String getMrnaSequence(String mrnaId, StringBuilder geneBuilder, int geneStartPos, Node mrnaNode, boolean isCoFeature) throws IOException {

        // get all CDS nodes for this mRNA
        FeatureNodeComparator comparator = new FeatureNodeComparator();
        PriorityQueue<Node> cdsNodes = new PriorityQueue<>(comparator);
        for (Relationship rel : mrnaNode.getRelationships(RelTypes.contributes_to, Direction.INCOMING)) {
            Node cdsNode = rel.getStartNode();
            cdsNodes.add(cdsNode);
        }
        int numCds = cdsNodes.size();

        // get the CDS sequences from the address and GeneBuilder; add to cdsSequences
        StringBuilder cdsBuilder = new StringBuilder();
        int phase = 0;
        int count = 0;
        while (!cdsNodes.isEmpty()) {
            Node cdsNode = cdsNodes.poll();
            int[] address = (int[]) cdsNode.getProperty("address");
            boolean forward = cdsNode.getProperty("strand").equals("+"); //unused since CDS nodes have this information

            // get phase from CDS node (for first CDS if forward, for last CDS if reverse; if isCoFeature, keep phase on 0)
            if (!isCoFeature) {
                if ((forward && count == 0) || (!forward && count == (numCds - 1))) {
                    phase = (int) cdsNode.getProperty("phase");
                    Pantools.logger.trace("\t\tCDS {} phase = {}", count, phase);
                }
            }

            // get CDS sequence from geneBuilder
            String cdsSequence;
            try {
                cdsSequence = geneBuilder.substring(address[2] - geneStartPos, address[3] - geneStartPos + 1);
            } catch (StringIndexOutOfBoundsException e) {
                Pantools.logger.warn("mRNA {} is miss-annotated! (its CDS is out of bounds).", mrnaId);
                break;
            }

            // add to cdsBuilder and reverse complement if necessary
            if (forward) { // add to end of cdsBuilder
                cdsBuilder.append(cdsSequence);
            } else { // reverse complement
                StringBuilder cdsBuilder2 = new StringBuilder(cdsSequence);
                reverse_complement(cdsBuilder2, cdsSequence); // cdsBuilder2 is now the reverse complement of cdsSequence
                if (isCoFeature) { // add to end of cdsBuilder
                    cdsBuilder.append(cdsBuilder2);
                } else { // add to start of cdsBuilder
                    cdsBuilder.insert(0, cdsBuilder2);
                }
            }

            // log what was done
            Pantools.logger.trace("CDS node = {} (strand: {}) at: {}",
                    cdsNode.getId(), forward, Arrays.toString(address));
            Pantools.logger.trace("cdsSequence = {}", cdsSequence);

            // increment count
            count++;
        }

        // protective clause to prevent indexing an empty string on return
        if (cdsBuilder.length() == 0) return "";

        // create mRNA sequence from CDS sequence
        StringBuilder mrnaSequence = new StringBuilder(cdsBuilder.toString());

        // return the mRNA sequence from position defined by phase
        Pantools.logger.trace("phase = {}", phase);
        if (phase == -1) { // assume phase is 0 if not specified
            return mrnaSequence.toString();
        } else {
            return mrnaSequence.substring(phase);
        }
    }

    /**
     * Retrieves sequence of the features and stores them in a FASTA file. 
     */
    public void retrieve_features() {
        Pantools.logger.info("Retrieve sequences of annotated (GFF) features from the pangenome.");
        // Verify if the provided --label argument is correct
        if (SELECTED_LABEL == null) { 
            Pantools.logger.error("No GFF annotation feature/node was provided via --label.");
            throw new RuntimeException("Missing selected label");
        }
        if (!labels.containsKey(SELECTED_LABEL)) {
            Pantools.logger.warn("{} is not a known node type.", SELECTED_LABEL);
            Pantools.logger.warn("Allowed features: gene, coding_gene, mRNA, CDS, exon, intron, tRNA, rRNA");
        }
        
        // Verify if the genomes were correctly selected
        if (skip_genomes == null && target_genome == null) {
            Pantools.logger.error("Missing a genome selection via --feature-type, --skip or --reference. Using multiple arguments is not allowed.");
            throw new RuntimeException("Missing genome selection argument");
        }
        
        Label feature_label = labels.get(SELECTED_LABEL);
        connect_pangenome();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome(pangenome_node, "retrieve_features"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user        
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) { 
            Pantools.logger.error("Unable to start the database.");
            throw new RuntimeException("Database error");
        }
        create_directory_in_DB("retrieval/features/");
        GENOME_SC = new SequenceScanner(GENOME_DB, 1, 1, K_SIZE, INDEX_DB.get_pre_len());
    
        // skip_array is used to control for which genomes the sequences are extracted.
        // By using --skip or --reference, the array is already prepared. 
        // When using --label, the array is prepared below
        
        BufferedWriter[] out = new BufferedWriter[GENOME_DB.num_genomes];
       
        try { // start the file writers
            for (int i = 0; i < total_genomes; i++) { // i+1 is the genome number
                if (skip_array[i]) {
                    continue;
                }
                out[i] = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "retrieval/features/" + SELECTED_LABEL + "s_" + (i+1) + ".fasta"));
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read fasta file.");
            throw new RuntimeException("File error");
        }
        
        // retrieve nodes that match the provided --label and extract sequences
        int extract_counter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            try {
                ResourceIterator<Node> feature_nodes = GRAPH_DB.findNodes(feature_label);
                while (feature_nodes.hasNext()) {
                    Node feature = feature_nodes.next();
                    int genome = (int) feature.getProperty("genome");
                    String feature_id = (String) feature.getProperty("id");
                    StringBuilder feature_seq = new StringBuilder();
                    if (skip_array[genome-1]) { // genome was not selected by user
                        continue;
                    }
                    if (SELECTED_LABEL.equals("mRNA")) { 
                        feature_seq.append(feature.getProperty("sequence"));
                    } else {
                        int[] address = (int[]) feature.getProperty("address");
                        boolean forward = feature.getProperty("strand").equals("+"); // returns a boolean                     
                        int region_length = address[3] - address[2] + 1;
                        GENOME_SC.get_sub_sequence(feature_seq, address[0], address[1], address[2]-1, region_length, forward);
                    }
                    extract_counter ++;
                    out[genome-1].write(">" + feature_id + "\n");
                    write_fasta(out[genome-1], feature_seq.toString(), 70);
                } 
                
                System.out.println("\r" + extract_counter + " " + SELECTED_LABEL + "s found and retrieved successfully.");
                for (int i = 0; i < total_genomes; i++) { // i+1 is the genome number
                    if (skip_array[i]) {
                        continue;
                    }
                    out[i].close();
                }
            } catch (IOException ioe) {
                Pantools.logger.error("Failed to write files to {}retrieval/features/", WORKING_DIRECTORY);
                throw new RuntimeException("File error");
            }
            tx.success();
        }
        Pantools.logger.info("Output written to:");
        Pantools.logger.info("{}retrieval/features/", WORKING_DIRECTORY);
    }
    
    /**
     * Translates a transcript to a protein sequence
     * 
     * @param mRNA The sequence of the mRNA
     * @return The protein sequence
     */
    public String translate(StringBuilder mRNA) {
        protein_builder.setLength(0);
        int i;
        for (i = 0; i <= mRNA.length() - 3; i += 3)
            protein_builder.append(aminoacid_table[binary[mRNA.charAt(i)] * 16 
                                                 + binary[mRNA.charAt(i + 1)] * 4
                                                 + binary[mRNA.charAt(i + 2)]]);
        return protein_builder.toString();
    }    

    /**
     * Removes an annotation from a genome. annotation, feature, gene, CDS, tRNA, rRNA, mRNA, intron and exon
     * Requires 
     * -dp 
     * 
     * Requires either ONE of the following arguments
     * --skip/--reference 
     * --annotations-file
     * 
     */
    public void remove_annotations() {
        Pantools.logger.info("Removing annotations");
        long total_annotations = 0;
        int removed_counter = 0;
        ArrayList<String> annotations_to_remove = new ArrayList<>();
        ArrayList<Node> anno_nodes_to_remove = new ArrayList<>();
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) {
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome(pangenome_node, "remove_annotations"); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes 
            total_annotations = count_nodes(ANNOTATION_LABEL);
            tx.success();
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            throw new RuntimeException("Database error");
        }
        if (total_annotations == 0) {
            Pantools.logger.warn("No annotations are present.");
            return;
        }
        
        if (target_genome == null && skip_genomes == null && PATH_TO_THE_ANNOTATIONS_FILE == null) {
            Pantools.logger.error("Provide a selection of genomes (--skip, --reference) or annotations (--annotations-file).");
            throw new RuntimeException("No genome selection or annotations file provided");
        }
        
        create_skip_arrays(false, true); // create skip arrays for sequences and genomes if -skip/-ref is provided by user
        String genome_or_identifier = "genome";

        if ((target_genome != null || skip_genomes != null) && PATH_TO_THE_ANNOTATIONS_FILE == null) { // option 1, remove all from selected genomes
            try (Transaction tx = GRAPH_DB.beginTx()) {
                ResourceIterator<Node> annotation_nodes = GRAPH_DB.findNodes(ANNOTATION_LABEL);
                while (annotation_nodes.hasNext()) {
                    Node annotation_node = annotation_nodes.next();
                    int genome_nr = (int) annotation_node.getProperty("genome");
                    if (skip_array[genome_nr-1]) {
                        continue;
                    }
                    removed_counter ++;
                    String identifier = (String) annotation_node.getProperty("identifier");
                    annotations_to_remove.add(identifier);
                    anno_nodes_to_remove.add(annotation_node);
                }
                tx.success();
            }
        }
        
        if ((target_genome == null && skip_genomes == null) && PATH_TO_THE_ANNOTATIONS_FILE != null) { // option 2, remove annotations matching the identifiers 
            genome_or_identifier = "identifier";
            ArrayList<String> ids_to_remove = read_annotation_identifiers_to_remove();     
            try (Transaction tx = GRAPH_DB.beginTx()) {
                ResourceIterator<Node> annotation_nodes = GRAPH_DB.findNodes(ANNOTATION_LABEL);
                while (annotation_nodes.hasNext()) {
                    Node annotation_node = annotation_nodes.next();
                    String identifier = (String) annotation_node.getProperty("identifier");
                    if (ids_to_remove.contains(identifier)) {
                        removed_counter ++;
                        annotations_to_remove.add(identifier);
                        anno_nodes_to_remove.add(annotation_node);
                    }
                }
                tx.success();
            }  
        }
        ask_to_remove_annotations(removed_counter, total_annotations); 
        for (String identifier : annotations_to_remove) {
            delete_file_in_DB("proteins/longest_transcripts/proteins_" + identifier + ".fasta");
            remove_node_rels_matching_genome_or_identifier(MRNA_LABEL, identifier, genome_or_identifier);
            remove_node_rels_matching_genome_or_identifier(TRNA_LABEL, identifier, genome_or_identifier);
            remove_node_rels_matching_genome_or_identifier(RRNA_LABEL, identifier, genome_or_identifier);
            remove_node_rels_matching_genome_or_identifier(CDS_LABEL, identifier, genome_or_identifier);
            remove_node_rels_matching_genome_or_identifier(GENE_LABEL, identifier, genome_or_identifier);
            remove_node_rels_matching_genome_or_identifier(EXON_LABEL, identifier, genome_or_identifier);
            remove_node_rels_matching_genome_or_identifier(INTRON_LABEL, identifier, genome_or_identifier);
            remove_node_rels_matching_genome_or_identifier(FEATURE_LABEL, identifier, genome_or_identifier);
        }
       
        try (Transaction tx = GRAPH_DB.beginTx()) {
            for (Node annotation_node: anno_nodes_to_remove) {
                Iterable<Relationship> relationships = annotation_node.getRelationships();
                for (Relationship rel : relationships) {
                    rel.delete();
                }
                annotation_node.delete();
            }
            tx.success();
        }
    }
    
    /**
     * 
     * @param target_label a node label
     * @param identifier an annotation identifier
     * @param genome_or_identifier 
     */
    public void remove_node_rels_matching_genome_or_identifier(Label target_label, String identifier, String genome_or_identifier) { 
        MAX_TRANSACTION_SIZE = 10000;
        HashSet<Node> node_set = new HashSet<>();
        Transaction tx = GRAPH_DB.beginTx(); // start database transaction
        try {
            ResourceIterator<Node> nodes;
            if (genome_or_identifier.equals("genome")) {
                String[] id_array = identifier.split("_"); 
                int genome_nr = Integer.parseInt(id_array[0]);
                nodes = GRAPH_DB.findNodes(target_label, "genome", genome_nr);
            } else {
                nodes = GRAPH_DB.findNodes(target_label, "annotation_id", identifier);
            }
          
            HashSet<Relationship> rel_set = new HashSet<>();
            SELECTED_LABEL = target_label.toString();
            gather_nodes_and_rels_matching_identifier(nodes, node_set, rel_set, target_label, identifier);
            int trsc1 = 0;
            int rel_counter = 0;
            for (Relationship rel : rel_set) {
                rel.delete();
                trsc1 ++;
                rel_counter ++;
                if (rel_counter % 10000 == 0 || rel_counter == rel_set.size()) {
                    System.out.print("\rRemoving " + target_label + " relationships of annotation " + identifier + ": "
                            + rel_counter + "/" + rel_set.size() + "             ");
                } 
                if (trsc1 >= MAX_TRANSACTION_SIZE) {
                    tx.success();
                    tx.close();
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                    trsc1 = 0;
                }
            }
             tx.success();
        } finally {
           tx.close();
        }      
        
        tx = GRAPH_DB.beginTx(); // start database transaction
        try {  
            int trsc1 = 0;
            int node_counter = 0;
            for (Node node1 : node_set) {
                trsc1 ++;
                node1.delete();
                node_counter ++;
                if (node_counter % 10000 == 0 || node_counter == node_set.size()) {
                    System.out.print("\rRemoving " + target_label + " nodes of annotation " + identifier + ": "
                            + node_counter + "/" + node_set.size() + "               ");
                }
                if (trsc1 >= MAX_TRANSACTION_SIZE) {
                    tx.success();
                    tx.close();
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                    trsc1 = 0;
                }
            }
            tx.success();
        } finally {
           tx.close();
        }  
    }
    
    /**
     * 
     * @param nodes
     * @param node_set
     * @param rel_set
     * @param target_label
     * @param identifier 
     */
    public void gather_nodes_and_rels_matching_identifier(ResourceIterator<Node> nodes, HashSet<Node> node_set, 
            HashSet<Relationship> rel_set, Label target_label, String identifier) {
        
        long node_count = 0;
        while (nodes.hasNext()) { 
            Node node1 = nodes.next();
            Iterable<Relationship> rels = node1.getRelationships(); 
            int genome_nr = (int) node1.getProperty("genome");
            if (skip_array[genome_nr-1]) {
                continue;
            }
            for (Relationship rel : rels) {
                rel_set.add(rel);
            }
            node_count++;
            if (node_count % 1000 == 0) {
                 System.out.print("\rGathering " + target_label + " nodes of annotation " + identifier + ": " + node_count + "                  ");
            }
            node_set.add(node1); 
        }
    }
    
    /**
     * 
     * @param removed_counter
     * @param total_annotations 
     */
    public void ask_to_remove_annotations(int removed_counter, long total_annotations) {
        Scanner s = new Scanner(System.in);
        Pantools.logger.info("{} out of {} annotations in the pangenome were selected\nDo you really want to remove these from the database [y/n]?\n-> ", removed_counter, total_annotations);
        String str = s.nextLine().toLowerCase();
        if (!str.equals("y") && !str.equals("yes")) {
            Pantools.logger.error("Did not remove the annotations.");
            throw new RuntimeException("Not removing annotations");
        }
    }
    
    /**
     * Each annotation identifier should be on a seperate line. Stop when this is not the case
     * @return ArrayList (String) with annotation identifiers Example: 1_1, 1_2, 2_1
     */
    public ArrayList<String> read_annotation_identifiers_to_remove() {
        ArrayList<String> ids_to_remove = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(PATH_TO_THE_ANNOTATIONS_FILE))) {
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (line.contains(",") || line.contains(".") || !line.contains("_")) {
                    Pantools.logger.error("Not all identifiers in {} are correctly formatted: {}", PATH_TO_THE_ANNOTATIONS_FILE, line);
                    throw new RuntimeException("Malformed annotation identifier");
                }
                ids_to_remove.add(line);
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", PATH_TO_THE_ANNOTATIONS_FILE);
            throw new RuntimeException("File error");
        }
        Pantools.logger.info("{} annotation identifiers were included.", ids_to_remove.size());
        return ids_to_remove;
    }
}

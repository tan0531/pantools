package nl.wur.bif.pantools.pangenome.export.records;

import java.util.List;

/**
 * Record interface to be used for dumping stuff to CSV.
 */
public interface Record {
    List<String> asList();
}

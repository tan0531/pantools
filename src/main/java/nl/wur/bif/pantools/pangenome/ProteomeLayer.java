/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.wur.bif.pantools.pangenome;

import nl.wur.bif.pantools.alignment.LocalSequenceAlignment;
import nl.wur.bif.pantools.pantools.Pantools;
import nl.wur.bif.pantools.utils.GraphUtils;
import nl.wur.bif.pantools.utils.FileUtils;
import org.neo4j.graphdb.*;

import java.io.*;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import static nl.wur.bif.pantools.pangenome.Classification.*;
import static nl.wur.bif.pantools.pangenome.create_skip_arrays.create_skip_arrays;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

/**
 * Implements all the functionalities related to the proteome layer of the pangenome.
 * 
 * @author Siavash Sheikhizadeh, Eef Jonkheer, Bioinformatics group, Wageningen University, the Netherlands
 */
public class ProteomeLayer {
    private final int PEPTIDE_SIZE = 6;
    private int MAX_INTERSECTIONS;
    private int MAX_KMER_FREQ;
    private int MAX_KMERS_NUM;
    private AtomicInteger num_intersections;
    private AtomicInteger num_similarities;
    private AtomicInteger num_components;
    private AtomicInteger similarity_bars;
    private int num_hexamers;
    private int num_genomes;
    private int num_proteins;
    private int num_groups; 
    private long[][] kmers_proteins_list;
    private int[] kmer_frequencies;
    private BlockingQueue<Node> proteins;
    private BlockingQueue<Intersection> intersections;
    private BlockingQueue<Intersection> similarities;
    private BlockingQueue<LinkedList> components; 
    private BlockingQueue<LinkedList> homology_groups_list; 
    private Node pangenome_node;
    private boolean first_generate_proteins = true;
  
    private boolean print_info = false;
    private String progress;
    
    public ProteomeLayer() {
        MAX_INTERSECTIONS = 10000000;
        MAX_KMERS_NUM = (int)Math.round(Math.pow(21, PEPTIDE_SIZE));
    }

    public void setPangenomeNode(Node pangenomeNode) {
        pangenome_node = pangenomeNode;
    }

    /**
     * Implements the class for intersecting pair of proteins.
     */
    public class Intersection {
        public Node protein1;
        public Node protein2;
        public double similarity;
        /**
         * Initializes an intersection.
         * 
         * @param p1 The first protein.
         * @param p2 The second protein.
         * @param s The normalized similarity score of two proteins
         */
        public Intersection(Node p1, Node p2, double s) {
            protein1 = p1;
            protein2 = p2;
            similarity = s;
        }
    }

    /**
     * Iterates over the proteins and put them in the Blocking-queue "proteins".
     * Only one thread should be called for this runnable.
     */
    public class Generate_proteins implements Runnable {
        @Override
        public void run() {
            ResourceIterator<Node> proteins_iterator;
            int first_counter = 0;
            try(Transaction tx = GRAPH_DB.beginTx()) {
                proteins.clear();
                pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
                for (int i = 1; i <= total_genomes; ++i) {
                    if (skip_array[i-1]) {
                        continue;
                    }
                    if (first_generate_proteins) {
                        System.out.print("\rCreating k-mers protein sequences: Genome " + i);
                    }
                    proteins_iterator = GRAPH_DB.findNodes(MRNA_LABEL, "genome", i);
                    while (proteins_iterator.hasNext())
                        try {
                            Node mrna_node = proteins_iterator.next();
                            if (mrna_node.hasProperty("protein_ID")) {
                                if (PROTEOME) {
                                    proteins.put(mrna_node);
                                    if (first_generate_proteins) {
                                        first_counter++;
                                    }
                                } else { // when running against pangenome 
                                    String identifier = (String) mrna_node.getProperty("annotation_id");
                                    if (annotation_identifiers.contains(identifier)) {
                                        if (longest_transcripts) { // with this mode, only include the longest transcript of a gene
                                            if (mrna_node.hasProperty("longest_transcript")) {
                                                proteins.put(mrna_node);
                                                if (first_generate_proteins) {
                                                    first_counter++;
                                                }
                                            }
                                        } else {
                                            proteins.put(mrna_node);
                                            if (first_generate_proteins) {
                                                first_counter++;
                                            }
                                        }
                                    }
                                }
                            } 
                        } catch (InterruptedException ex) {
                            Logger.getLogger(ProteomeLayer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    proteins_iterator.close();
                }
                if (first_generate_proteins) {
                    num_proteins = first_counter;
                }
                first_generate_proteins = false;
                tx.success();
            }
        }
    }
    
    /**
     * Takes proteins from the Blocking-queue "proteins" and stores the count
     * of k-mers of the proteome in the aaray "kmer_frequencies". K-mers are
     * represented by numbers in the base of 20.
     * Only one thread should be called for this runnable
     */
    public class count_kmers implements Runnable {
        int num_proteins;
        public count_kmers(int num) {
            num_proteins = num;
        }

        @Override
        public void run() {
            int i = 0, c;
            Node protein_node;
            int protein_length, kmer_index;
            String protein;
            int[] code = new int[256];
            char[] aminoacids = new char[] {'A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y','*'};
            for (i = 0; i < 21; ++i)
                code[aminoacids[i]] = i;
            kmer_frequencies = new int[MAX_KMERS_NUM];
            try{
                long prot_counter = 0;
                try (Transaction tx = GRAPH_DB.beginTx()) {
                    for (c = 0; c < num_proteins; ++c) {
                        protein_node = proteins.take();
                        if (prot_counter % 1001 == 0 || prot_counter == num_proteins-1) {
                            System.out.print("\rCounting k-mers: " + prot_counter + "/" + num_proteins + "                     ");
                        }
                       
                        if (protein_node.hasProperty("protein") || protein_node.hasProperty("protein_sequence") ) {
                            if (protein_node.hasProperty("protein")) {
                                protein = (String)protein_node.getProperty("protein", "");
                            }else {
                                protein = (String)protein_node.getProperty("protein_sequence", "");
                            }
                            prot_counter ++;
                            protein_length = protein.length();
                            if (protein_length > PEPTIDE_SIZE) {
                                kmer_index = 0;
                                for (i = 0; i < PEPTIDE_SIZE; ++i)
                                    kmer_index = kmer_index * 21 + code[protein.charAt(i)];
                                for (; i < protein_length; ++i) {// for each kmer of the protein
                                    if (kmer_frequencies[kmer_index] == 0)
                                        ++num_hexamers;
                                    kmer_frequencies[kmer_index] += 1;
                                    kmer_index = kmer_index % (MAX_KMERS_NUM / 21) * 21 + code[protein.charAt(i)];
                                }
                            }
                        }
                    }
                    tx.success();
                   
                }
                System.out.print("\rCounting k-mers: " + prot_counter + "/" + num_proteins + "          ");
                kmers_proteins_list = new long[MAX_KMERS_NUM][];
                for (i = 0; i < MAX_KMERS_NUM; ++i) {
                    if (kmer_frequencies[i] > 1 && kmer_frequencies[i] < MAX_KMER_FREQ)// + num_genomes / 2
                        kmers_proteins_list[i] = new long[kmer_frequencies[i]];
                    else
                        kmers_proteins_list[i] = null;
                    kmer_frequencies[i] = 0;
                }
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Takes proteins from the Blocking-queue "proteins" and k-merizes the 
     * proteins. K-mers are represented by numbers in the base of 20.
     * For each k-mer a list of proteins containing that k-mer is stored in
     * the array "kmers_proteins_list".
     * Only one thread should be called for this runnable.
     */
    public class Kmerize_proteins implements Runnable {
        int num_proteins;
        public Kmerize_proteins(int num) {
            num_proteins = num;
        }

        @Override
        public void run() {
            int i = 0, c, chunk = num_proteins > 40 ? num_proteins / 40 : 1; // TODO: add progress bar
            Node protein_node;
            int protein_length, kmer_index;
            String protein;
            long protein_id;
            int[] code = new int[256];
            char[] aminoacids = new char[] {'A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y','*'};
            for (i = 0; i < 21; ++i)
                code[aminoacids[i]] = i;
            try{
                try (Transaction tx = GRAPH_DB.beginTx()) {
                    for (c = 0; c < num_proteins; ++c) {
                        protein_node = proteins.take();
                        if (protein_node.hasProperty("protein") || protein_node.hasProperty("protein_sequence")) {
                            if (protein_node.hasProperty("protein")) {
                                protein = (String)protein_node.getProperty("protein", "");
                            }else {
                                protein = (String)protein_node.getProperty("protein_sequence", "");
                            }
                            protein_length = protein.length();
                            protein_id = protein_node.getId();
                            if (protein_length > PEPTIDE_SIZE) {
                                kmer_index = 0;
                                for (i = 0; i < PEPTIDE_SIZE; ++i)
                                    kmer_index = kmer_index * 21 + code[protein.charAt(i)];
                                for (; i < protein_length; ++i) {// for each kmer of the protein
                                // ignore extremely rare and abundant k-mers
                                    if (kmers_proteins_list[kmer_index] != null) {
                                        kmers_proteins_list[kmer_index][kmer_frequencies[kmer_index]] = protein_id;
                                        ++kmer_frequencies[kmer_index];
                                    }
                                    kmer_index = kmer_index % (MAX_KMERS_NUM / 21) * 21 + code[protein.charAt(i)];
                                }
                            }
                            if (c % chunk == 0)
                                System.out.print("|"); //TODO: add progress bar
                        }
                    }
                    System.out.println();
                    tx.success();
                    kmer_frequencies = null;
                }
            } catch(InterruptedException e) {
                Pantools.logger.info(e.getMessage());
            }
        }
    }

    /**
     * Takes proteins from the Blocking-queue "proteins" and detects pairs of 
     * intersecting proteins and put them in a Blocking-queue "intersections".
     * Only one thread should be called for this runnable.
     */
    public class Find_intersections implements Runnable {
        int num_proteins;
        double frac = INTERSECTION_RATE;
        int max_intersection = MAX_INTERSECTIONS;
        public Find_intersections(int num) {
            num_proteins = num;
        }

        @Override
        public void run() {
            Pantools.logger.trace("max_intersection {}", max_intersection);
            int i, j, len, chunk = num_proteins > 40 ? num_proteins / 40 : 1;
            int p, counter, num_ids, kmer_index, num_ins = 0;
            long[] crossing_protein_ids = new long[max_intersection];
            int[] code = new int[256];
            char[] aminoacids = new char[]{'A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y','*'};
            for (i = 0; i < 21; ++i)
                code[aminoacids[i]] = i;
            Node protein_node, crossing_protein_node;
            long protein_id;
            int protein_length, shorter_len;
            String protein, crossing_protein;
            long crossing_protein_id, p_id;
            
            try (Transaction tx = GRAPH_DB.beginTx()) {
                try {
                    for (p = 0; p < num_proteins; ++p) {
                        protein_node = proteins.take();
                        if (protein_node.hasProperty("protein") || protein_node.hasProperty("protein_sequence")) {
                            protein = get_protein_sequence(protein_node);
                            protein_length = protein.length();
                            if (protein_length > PEPTIDE_SIZE) {
                                protein_id = protein_node.getId();
                                num_ids = 0;
                                kmer_index = 0;
                                for (i = 0; i < PEPTIDE_SIZE; ++i)
                                    kmer_index = kmer_index * 21 + code[protein.charAt(i)];
                                for (; i < protein_length && num_ids < max_intersection; ++i) {// for each kmer of the protein
                                    if (kmers_proteins_list[kmer_index] != null) {
                                        len = kmers_proteins_list[kmer_index].length;
                                        for (j = 0; j < len && num_ids < max_intersection; ++j) {
                                            crossing_protein_id = kmers_proteins_list[kmer_index][j];
                                        // Only crossing proteins with a higher ID
                                            if (crossing_protein_id > protein_id) { 
                                                crossing_protein_ids[num_ids++] = crossing_protein_id;
                                            }
                                        }
                                    }
                                    kmer_index = kmer_index % (MAX_KMERS_NUM / 21) * 21 + code[protein.charAt(i)];
                                }
                            // Sorts the crossing protein IDs to count the number of shared k-mers.
                                Arrays.sort(crossing_protein_ids, 0, num_ids);
                                for (i = 0, counter = 1, crossing_protein_id = crossing_protein_ids[0]; i < num_ids ; ++i) {
                                    p_id = crossing_protein_ids[i];
                                // New run of protein IDs
                                    if (crossing_protein_id != p_id) {
                                        if(counter > 1) {
                                            crossing_protein_node = GRAPH_DB.getNodeById(crossing_protein_id);
                                            //crossing_protein = (String)crossing_protein_node.getProperty("protein");
                                            crossing_protein = get_protein_sequence(crossing_protein_node);
                                            shorter_len = Math.min(protein_length, crossing_protein.length());
                                            if (counter >= frac * (shorter_len - PEPTIDE_SIZE + 1)) {
                                                intersections.put(new Intersection(protein_node, crossing_protein_node, 0));
                                                ++num_ins;
                                            }
                                        }
                                        crossing_protein_id = p_id;
                                        counter = 1; 
                                    } else
                                        ++counter;
                                }
                                if(counter > 1) {
                                    crossing_protein_node = GRAPH_DB.getNodeById(crossing_protein_id);
                                    //crossing_protein = (String)crossing_protein_node.getProperty("protein");
                                    crossing_protein = get_protein_sequence(crossing_protein_node);
                                    shorter_len = Math.min(protein_length, crossing_protein.length());
                                    if (counter >= frac * (shorter_len - PEPTIDE_SIZE + 1)) {
                                        intersections.put(new Intersection(protein_node, crossing_protein_node,0));
                                        ++num_ins;
                                    }
                                }
                                if (p % chunk == 0)
                                    System.out.print("|"); //TODO: add progress bar
                            }
                        }
                    }// for protein
                    System.out.println();
                    num_intersections.getAndAdd(num_ins);
                    Pantools.logger.info("Intersections = {}", num_ins);
                    Pantools.logger.info("Calculating similarities:");
                    System.out.println("0 ......................................... 100");
                // Signify the end of intersections queue.
                    for (i = 0; i < THREADS; ++i)
                        intersections.put(new Intersection(null, null,-1));// end of queue
                } catch(InterruptedException e) {
                    Pantools.logger.info(e.getMessage());
                }
                tx.success();
            }
        }
    } 
    
    /**
     * @param mrna_node
     * @return 
     */
    public String get_protein_sequence(Node mrna_node) {
        String sequence;
        if (mrna_node.hasProperty("protein")) {
            sequence = (String)mrna_node.getProperty("protein", "");
        }else {
            sequence = (String)mrna_node.getProperty("protein_sequence", "");
        }
        return sequence;
    }
    
    /**
     * Takes the intersections from the Blocking-queue "intersections" and 
     * calculates the similarity score of the protein pairs. If similarity is high enough
     * the intersection object is put in the Blocking-queue "similarities".
     * Multiple threads can call this runnable.
     */
    public class Find_similarities implements Runnable {
        int MAX_ALIGNMENT_LENGTH = 1000;
        int m, n, threshold = MIN_NORMALIZED_SIMILARITY;
        int processed = 0;
        StringBuilder query;
        StringBuilder subject;
        LocalSequenceAlignment aligner;
        public Find_similarities(String scoringMatrix) {
            aligner = new LocalSequenceAlignment(GAP_OPEN, GAP_EXT,MAX_ALIGNMENT_LENGTH, 0, scoringMatrix);
            query = new StringBuilder();
            subject = new StringBuilder();
        }

        @Override
        public void run() {
            Node protein_node1, protein_node2;
            String protein1, protein2;
            Intersection ints;
            int num_ints = 0;
            boolean all_intersections_found = false;
            int trsc = 0;
            try {
                Transaction tx = GRAPH_DB.beginTx(); // start database transaction
                try {
                    ints = intersections.take();
                    while (ints.protein1 != null) {
                        trsc ++;
                        if (trsc >= 50000) {
                            tx.success();
                            tx.close();
                            trsc = 0;
                            tx = GRAPH_DB.beginTx(); // start a new database transaction
                        }
                        ++processed;
                        protein_node1 = ints.protein1;
                        protein_node2 = ints.protein2;
                        protein1 = get_protein_sequence(protein_node1);
                        protein2 = get_protein_sequence(protein_node2);
                        m = protein1.length();
                        n = protein2.length();
                       
                        if (m < n) // protein1 is smaller 
                            ints.similarity = similarity_percentage(protein1, protein2); // gets a NORMALIZED similarity score 
                        else // protein1 is larger
                            ints.similarity = similarity_percentage(protein2, protein1); // gets a NORMALIZED similarity score 
                        
                        if (ints.similarity > threshold) {
                            similarities.put(ints);
                            num_similarities.getAndIncrement();
                        }
                        ints = intersections.take();
                        if (!all_intersections_found) {
                            num_ints = num_intersections.intValue();
                            if (num_ints > 0) {
                                all_intersections_found = true;
                                num_ints -= processed;
                            }
                            if (num_ints < 40)
                                num_ints = 40;
                        }
                        if (all_intersections_found && processed % (num_ints / 40) == 0) {
                            System.out.print("|"); //TODO: add progress bar
                            similarity_bars.getAndIncrement();
                        }
                    }
                // Signify the end of the similarities queue.
                    //similarities.put(new intersection(null, null,0));
                    Node pangenome_node = GRAPH_DB.getNodeById(0);
                    similarities.put(new Intersection(pangenome_node, null, 0));
                    tx.success();
                } finally {
                    tx.close();
                }  
            
            } catch(InterruptedException e) {
                Pantools.logger.info(e.getMessage());
            } 
        } 
    
        /**
         * Given two proteins calculates the normalized similarity score between 
         * them which is less or equal to 1.
         * Proteins longer than MAX_LENGTH will be broken in smaller parts to be 
         * compared correspondingly.
         * 
         * @param p1 The first protein
         * @param p2 The second protein, is larger than protein 1 
         * @return The normalized similarity score which is less or equal to 1
         */
        double similarity_percentage(String p1, String p2) { 
            int m, n,i, parts_num = 1, part_len1, part_len2;
            long score = 0, p_score = 0;
            String query, subject;
            m = p1.length();
            n = p2.length();
            if (n >= MAX_ALIGNMENT_LENGTH) {
                parts_num = (n / MAX_ALIGNMENT_LENGTH) + (n % MAX_ALIGNMENT_LENGTH == 0 ? 0 : 1); // max alignment length is 1000 
                if (n % MAX_ALIGNMENT_LENGTH == 0) { // sequences of exactly the maximum_alignment length (1000, 2000, etc) must be split in an extra part
                    parts_num ++;
                }
                part_len1 = m / parts_num;
                part_len2 = n / parts_num;
                Pantools.logger.trace("{} {} parts num {} {} {}.", m, n, parts_num, part_len1, part_len2);
                for (i = 0; i < parts_num; ++i) {
                    query = p1.substring(i * part_len1, Math.min(m, (i + 1) * part_len1));
                    subject = p2.substring(i * part_len2, Math.min(n, (i + 1) * part_len2));
                    aligner.align(query, subject);
                    score += aligner.get_similarity();
                    p_score += aligner.get_match_score(query, query);//5 * query.length();
                } 
            } else {
                aligner.align(p1, p2);
                score = aligner.get_similarity();
                p_score = aligner.get_match_score(p1, p1);//5 * query.length();
            }
            return score * 100.0 / p_score;
        }
    }

    /**
     * Takes the similarities from the Blocking-queue "similarities" and write 
     * it in the graph database.
     * Only one thread should call this runnable to avoid Deadlock exceptions.
    */
    public class Write_similarities implements Runnable {
        public Write_similarities() {
        }

        @Override
        public void run() {
            Intersection ints;
            int finished_thread_counter = 0;
            try{
                ints = similarities.take();
                while (ints.protein1 != null ) {
                    try(Transaction tx = GRAPH_DB.beginTx()) {
                        for (int trs = 0; trs < 10 * MAX_TRANSACTION_SIZE && ints.protein1 != null; ++trs) {
                            if (ints.protein2 == null) {
                                finished_thread_counter ++;
                                if (finished_thread_counter == THREADS-2) {
                                    similarities.put(new Intersection(null, null, 0));
                                }
                                ints = similarities.take();
                                continue;
                            }
                            Iterable<Relationship> relations = ints.protein1.getRelationships(RelTypes.is_similar_to, Direction.OUTGOING);
                            boolean already_found = false;
                            for (Relationship rel : relations) {
                                Node prot2 = rel.getEndNode();
                                if (prot2.getId() == ints.protein2.getId()) {
                                    already_found = true;
                                }
                            }
                            if (!already_found) { // relation does not exist yet 
                                ints.protein1.createRelationshipTo(ints.protein2, RelTypes.is_similar_to)
                                        .setProperty("similarity", ints.similarity);
                            }
                            ints = similarities.take();
                        }
                        tx.success();
                    } 
                }
            } catch(InterruptedException e) {
                Pantools.logger.info(e.getMessage());
            }
        }
    }
    
    /**
     * Builds components of similar proteins and put then in the Blocking-queue "components".
     * Only one thread should call this runnable.
     */
    public class build_similarity_components implements Runnable {
        int num_proteins;
        String pangenome_path;
        public build_similarity_components(String path, int num) {
            pangenome_path = path;
            num_proteins = num;
        }

        @Override
        public void run() {
            int i;
            Node protein_node;
            LinkedList<Node> component = new LinkedList();
            try{
                Pantools.logger.info("Building homology groups:");
                System.out.println("0 ......................................... 100");
                for (i = 0; i< num_proteins; ++i) {
                    protein_node = proteins.take();
                    if (protein_node == null) {
                        continue;
                    }
                   
                    breadth_first_search(component, protein_node);
                    Pantools.logger.trace("component = {}.", component.size());
                    if (component.size() > 0) { 
                        components.put(component);
                        num_components.getAndIncrement();
                        component = new LinkedList();
                    }
                } 
               
            // Signifies the end of the components queue for all the threads
                for (i = 0; i < THREADS; ++i)
                   components.put(new LinkedList());
            } catch(InterruptedException e) {
                Pantools.logger.info(e.getMessage());
            }
            if (num_components.get() == 0) {
                Pantools.logger.error("failure {} {}.", num_components, components.size());
                //System.exit(1);
            }
        }
        
        /**
         * Puts all the proteins similar to a protein in one component.
         * 
         * @param component An empty list to be filled with proteins similar to the query protein
         * @param start_protein The query protein 
         */
        private void breadth_first_search(LinkedList<Node> component, Node start_protein) {
            long start_id = start_protein.getId();
            Node crossing_protein;
            try (Transaction tx = GRAPH_DB.beginTx()) {
                if (!start_protein.hasProperty("component")) { // To avoid having one protein in different groups
                    start_protein.setProperty("component", start_id);
                    Queue<Node> homologs = new LinkedList();
                    homologs.add(start_protein);
                    // for all the candidates with some shared node with the protein
                    while (!homologs.isEmpty()) {
                        start_protein = homologs.remove();
                        component.add(start_protein);
                        for (Relationship crossing_edge: start_protein.getRelationships(RelTypes.is_similar_to)) {
                            crossing_protein = crossing_edge.getOtherNode(start_protein);
                            if(!crossing_protein.hasProperty("component")) {
                                crossing_protein.setProperty("component", start_id);
                                homologs.add(crossing_protein);
                            }
                        }
                    }// while
                }
                tx.success(); 
            }
        }
    }

    /**
     * Takes similarity components from the Blocking-queue "components" and 
     * splits them into homology groups represented by a list stored in the
     * Blocking-queue "homology_groups_list".
     * Multiple threads can call this runnable.
     */
    public class build_homology_groups implements Runnable {
        int num_proteins;
        String pangenome_path;
        double[][] phylogeny_distance;
        int[][] count;
        
        public build_homology_groups(String path, int num) {
            pangenome_path = path;
            num_proteins = num;
        }

        @Override
        public void run() {
            int i;
            LinkedList<Node> component;
            phylogeny_distance = new double[num_genomes + 1][];
            count = new int[num_genomes + 1][];
            for (i = 1; i < phylogeny_distance.length; ++i) {
                phylogeny_distance[i] = new double[num_genomes + 1];
                count[i] = new int[num_genomes + 1];
            } 
            try{
                component = components.take();
                while (!component.isEmpty()) {// Not finished
                    if (components.size() % 11 == 0) {
                        System.out.print("\r  " + progress + " (Components remaining " + components.size() + ")     ");
                    }
                    if (component.size() == 1) {
                        homology_groups_list.put(component);
                    }
                    else
                        break_component(component, pangenome_path);
                    component = components.take();
                }
            } catch(InterruptedException e) {
                Pantools.logger.info(e.getMessage());
            }
        }

        /**
         * Splits the similarity component into homology groups using MCL algorithm.
         * 
         * @param component The similarity component
         * @param pangenome_path The path to the current graph database
         */
        void break_component(LinkedList<Node> component, String pangenome_path) throws InterruptedException {
            int i, group_size, wating_time, time;
            double infl;
            LinkedList<Node> homology_group, singletons_group = new LinkedList();
            String graph_path, clusters_path, line, command;
            String[] fields;
            BufferedReader clusters_file;
            File tmp_file;
            group_size = component.size();
            graph_path = pangenome_path + "/" + component.getFirst().getId() + ".graph";
            clusters_path = pangenome_path + "/" + component.getFirst().getId() + ".clusters";
        // Prepare the input file for MCL
            write_similaity_matrix(component, graph_path);
        //  Estimate the run-time of MCL
            time = wating_time = 1 + (int) Math.round(group_size / 100000000.0 * group_size);
            for( infl = MCL_INFLATION; infl < 30; infl += 0.5) {
                command = "mcl " + graph_path + " --abc -I " + infl + " -o " + clusters_path;
                //Pantools.logger.info(command);
                if(executeCommand_for(command, wating_time))
                    break;
                if((tmp_file = new File(clusters_path)).exists())
                    tmp_file.delete();
                wating_time += time;
            }
            if (infl >= 30) {
                Pantools.logger.info("Failed to split group ID = {}.", component.getFirst().getId());
                homology_groups_list.put(component);
            } else {
                try (Transaction tx = GRAPH_DB.beginTx()) {
                    try{
                        clusters_file = new BufferedReader(new FileReader(clusters_path));
                    // For each line of the MCL output
                        while ((line = clusters_file.readLine()) != null) {
                            line = line.trim();
                            if (line.equals("")) // if line is empty
                                continue;
                            fields = line.split("\\s");
                            if (fields.length > 1) {
                                homology_group = new LinkedList();
                                for (i = 0; i < fields.length; ++i)
                                    homology_group.add(GRAPH_DB.getNodeById(Long.parseLong(fields[i])));
                                homology_groups_list.put(homology_group);
                            } else { // if is a singleton group
                                singletons_group.add(GRAPH_DB.getNodeById(Long.parseLong(fields[0])));
                            }
                        }
                    // put all singletons of the component in one homology groups
                        if (singletons_group.size() > 0) {
                                homology_groups_list.put(singletons_group);
                        }
                        clusters_file.close();
                        new File(clusters_path).delete();
                        new File(graph_path).delete();
                    }catch (IOException ex) {
                        Pantools.logger.info(ex.getMessage());
                    }
                    tx.success();
                }
            }
        }

        /**
         * Given a homology group complete all the pairwise similarities between its members and 
         * writes the weighted edges of the graph in SIMILARITY_GRAPH_FILE_NAME to be used by MCL clustering algorithm.
         * @param component The homology group
         * @param pangenome_path The path to the current graph database
         */
        private void write_similaity_matrix(LinkedList<Node> component, String pangenome_path) {
            ListIterator<Node> itr;
            Node protein1_node, protein2_node;
            int genome1, genome2;
            double similarity;
            try (PrintWriter graph = new PrintWriter(pangenome_path)) {
                try (Transaction tx = GRAPH_DB.beginTx()) {
                    calculate_phylogeny_distances(component);
                    for (itr = component.listIterator(); itr.hasNext(); ) {
                        protein1_node = itr.next();
                        genome1 = (int)protein1_node.getProperty("genome");
                        for (Relationship homology_edge: protein1_node.getRelationships(RelTypes.is_similar_to, Direction.OUTGOING)) {
                            protein2_node = homology_edge.getEndNode();
                            similarity = (double)homology_edge.getProperty("similarity");
                            similarity -= MIN_NORMALIZED_SIMILARITY;
                            genome2 = (int)protein2_node.getProperty("genome");
                            similarity += phylogeny_distance[genome1][genome2];
                            graph.write(protein1_node.getId()+" "+protein2_node.getId()+" "+ Math.pow(similarity, CONTRAST) + "\n");
                        }
                    }
                    tx.success();
                }
                graph.close();
            } catch (IOException ex) {
                // do nothing
            }
        }

        /**
         * Calculates pair-wise distances in a similarity component.
         * 
         * @param component the similarity component 
         */
        private void calculate_phylogeny_distances(LinkedList<Node> component) {
            int i, j, g1, g2;
            Node p1, p2;
            double similarity;
            ListIterator<Node> proteins_itr = component.listIterator();
            for (i = 1; i < phylogeny_distance.length; ++i)
                for (j = 1; j < phylogeny_distance.length; ++j) {
                    phylogeny_distance[i][j] = phylogeny_distance[i][j] = 0;
                    count[i][j] = count[j][i] = 0;
                }
            while (proteins_itr.hasNext()) {
                p1 = proteins_itr.next();
                g1 = (int)p1.getProperty("genome");
                for (Relationship r: p1.getRelationships(Direction.OUTGOING, RelTypes.is_similar_to)) {
                    p2 = r.getEndNode();
                    g2 = (int)p2.getProperty("genome");
                    similarity = (double)r.getProperty("similarity");
                    phylogeny_distance[g1][g2] += similarity;
                    phylogeny_distance[g2][g1] += similarity;
                    ++count[g1][g2];
                    ++count[g2][g1];
                }
            }
            for (i = 1; i < phylogeny_distance.length; ++i)
                for (j = 1; j < phylogeny_distance.length; ++j) {
                    if (count[i][j] > 0)
                        phylogeny_distance[i][j] = phylogeny_distance[i][j]/count[i][j];
                    else
                        phylogeny_distance[i][j] = MIN_NORMALIZED_SIMILARITY;
                }
            for (i = 1; i < phylogeny_distance.length; ++i) {
                for (j = 1; j < phylogeny_distance.length; ++j) {
                    if (i == j)
                        phylogeny_distance[i][j] = 0;
                    else
                        phylogeny_distance[i][j] = 100 - phylogeny_distance[i][j];
                }
            } 
        }
    }
    
    /**
     * Takes the homology groups from the Blocking-queue "homology_groups_list"
     * and created homology groups in the graph database and writes the groups
     * in "pantools_homologs.txt".
     * Only one thread can call this runnable.
     */
    public class write_homology_groups implements Runnable {
        String pangenome_path;
        int num_proteins;
        public write_homology_groups(String path, int num) {
            pangenome_path = path;
            num_proteins = num;
        }

        @Override
        public void run() {
            int i, trs, p = 0, chunk = num_proteins > 40 ? num_proteins / 40 : 1;
            int[] copy_number = new int[num_genomes + 1];
            LinkedList<Node> homology_group;
            Node homology_node, protein_node;
            BufferedWriter homology_file;
            try{
                try{
                    String longest_transcript = "";
                    if (longest_transcripts) {
                        longest_transcript = ", Only included the longest protein-coding transcript of genes";
                    }
                    homology_file = new BufferedWriter(new FileWriter(OUTPUT_PATH + "/pantools_homology_groups.txt"));
                    homology_file.write("#grouping_v" + grouping_version + ", similarity: " + MIN_NORMALIZED_SIMILARITY + ", inflation: " + MCL_INFLATION + 
                            ", intersection rate " + INTERSECTION_RATE + ", contrast: " + CONTRAST + longest_transcript + "\n");
                    while (p < num_proteins) {
                        try(Transaction tx = GRAPH_DB.beginTx()) {
                            for (trs = 0; trs < MAX_TRANSACTION_SIZE && p < num_proteins; ++trs) {
                                homology_group = homology_groups_list.take();
                                ++num_groups;
                                homology_node = GRAPH_DB.createNode(HOMOLOGY_GROUP_LABEL);
                                homology_node.setProperty("num_members", homology_group.size());
                                homology_node.setProperty("group_version", grouping_version);
                                homology_file.write(Long.toString(homology_node.getId()) + ":");
                               
                                while (!homology_group.isEmpty()) {
                                    protein_node = homology_group.remove();
                                    homology_node.createRelationshipTo(protein_node, RelTypes.has_homolog);
                                    int genome_nr = (int) protein_node.getProperty("genome");
                                    ++copy_number[genome_nr];
                                    if (protein_node.hasProperty("protein_ID") )
                                        homology_file.write(" " + ((String)protein_node.getProperty("protein_ID")).replace(' ', '_') + "#" + genome_nr );
                                    else
                                        homology_file.write(" " + protein_node.getId() + "#" + genome_nr);
                                    if (p % chunk == 0) {
                                        progress += "|";
                                    }
                                    ++p;
                                }
                                homology_file.write("\n");
                                homology_node.setProperty("copy_number", copy_number);
                                for (i=0; i < copy_number.length; ++i)
                                    copy_number[i] = 0;
                            }
                            tx.success();
                        }
                    }
                    homology_file.close();
                } catch (IOException ex) {
                    Pantools.logger.info(ex.getMessage());
                }
            } catch(InterruptedException e) {
                Pantools.logger.info(e.getMessage());
            }
            System.out.print("\r  " + progress + "                               ");
        }
    }

    /**
     * Stops the program if one of the files does not exist
     */
    public void check_if_all_proteome_files_exist() {
        try (BufferedReader in = new BufferedReader(new FileReader(PATH_TO_THE_PROTEOMES_FILE))) { // first check if all files exist
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (!check_if_file_exists(line)) {
                    Pantools.logger.error("Checking files: {} does not exist.", line);
                    System.exit(1);
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", PATH_TO_THE_PROTEOMES_FILE);
            System.exit(1);
        }
    }
    
    /**
     * Constructs the proteome layer of the pan-genome from a set of proteins.
     */
    public void initialize_panproteome(Path databaseDirectory) throws IOException {
        check_if_program_exists_stdout("kmc -h", 100, "kmc"); // check if program is set to $PATH
        if (PATH_TO_THE_PROTEOMES_FILE == null) {
            Pantools.logger.error("No protein file provided via --proteomes-file or -pf.");
            System.exit(1);
        }
        String file_path, file_type, line, protein_ID;
        StringBuilder protein = new StringBuilder();
        Node protein_node = null, panproteome;
        int trsc, num_proteins = 0, genome;
        String[] fields;
        check_if_all_proteome_files_exist(); 
        GraphUtils.createPanproteomeDatabase(databaseDirectory);
        try(BufferedReader protein_paths = new BufferedReader(new FileReader(PATH_TO_THE_PROTEOMES_FILE))) { 
            for (genome = 1; (file_path = protein_paths.readLine()) != null; ++genome) {
                String previous_ID_line = "";
                file_path = file_path.trim();
                if (file_path.equals("")) {// if line is empty
                    continue;
                }
                fields = file_path.split("\\.");
                file_type = fields[fields.length - 1].toLowerCase();
                if (file_type.equals("fasta") || file_type.equals("faa") || file_type.equals("fas") || file_type.equals("fa")) {
                    copy_file(file_path, WORKING_DIRECTORY + "proteins/proteins_" + genome + ".fasta");
                    try (Transaction tx = GRAPH_DB.beginTx()) {
                        try (BufferedReader in = new BufferedReader(new FileReader(file_path))) {
                            for (int c = 0; in.ready();) {
                                line = in.readLine().trim();
                                if (line.equals("")) {// if line is empty
                                    continue;
                                } else if (line.charAt(0) == '>') {
                                    ++num_proteins;
                                    if (previous_ID_line.equals("")) {
                                        previous_ID_line = line;
                                        continue;
                                    }

                                    String[] line_array = previous_ID_line.split(" ");
                                    protein_ID = line_array[0].substring(1);
                                    protein_node = GRAPH_DB.createNode(MRNA_LABEL);
                                    protein_node.setProperty("protein_ID", protein_ID);
                                    protein_node.setProperty("header", previous_ID_line);
                                    protein_node.setProperty("protein_sequence", protein.toString());
                                    protein_node.setProperty("protein_length", protein.length());
                                    protein_node.setProperty("genome", genome);
                                    protein.setLength(0);
                                    previous_ID_line = line;
                                } else {
                                    protein.append(line);
                                }
                                if (num_proteins % 100 == 1) {
                                    System.out.print("\r" + genome + " proteomes. " + num_proteins + " proteins ");
                                }
                            }
                        } catch (IOException ioe) {
                            Pantools.logger.error("Unable to read: {}", file_path);
                            System.exit(1);
                        }
                        tx.success();
                    }
                    
                // For the last protein
                    try (Transaction tx = GRAPH_DB.beginTx()) {
                        String[] line_array = previous_ID_line.split(" ");
                        protein_ID = line_array[0].substring(1);
                        protein_node = GRAPH_DB.createNode(MRNA_LABEL);
                        protein_node.setProperty("protein_ID", protein_ID);
                        protein_node.setProperty("header", previous_ID_line);
                        protein_node.setProperty("protein", protein.toString());
                        protein_node.setProperty("protein_length", protein.length());
                        protein_node.setProperty("genome",genome);
                        protein.setLength(0);
                        tx.success();
                    }
                } else {
                    Pantools.logger.error("{} does not have a valid extention (fasta, faa, fas or fa).", file_path);
                    System.exit(1);
                }
            }
            System.out.print("\r" + (genome-1) + " proteomes. " + num_proteins + " proteins\n\n");
            try (Transaction tx1 = GRAPH_DB.beginTx()) {
                panproteome = GRAPH_DB.createNode(PANGENOME_LABEL);
                panproteome.setProperty("date", new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
                panproteome.setProperty("num_genomes", genome - 1);
                panproteome.setProperty("num_proteins", num_proteins);
                tx1.success();
            }
            protein_paths.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        copy_file(PATH_TO_THE_PROTEOMES_FILE, WORKING_DIRECTORY + "log/panproteome_input_files.txt");
        genome_overview();
    }
    
    /*
     * Creates a list of relaxations that are skipped when a --value is given
     */
    public ArrayList<String> check_which_rn_to_use(boolean print, String input_values) {
        ArrayList<String> skip_groupings = new ArrayList<>(Arrays.asList("95","85","75","65","55","45","35","25"));
        ArrayList<String> selected_groupings = new ArrayList<>();
        String[] original_values = {"95","85","75","65","55","45","35","25"};
        if (input_values == null) {
            skip_groupings = new ArrayList<>();
            return skip_groupings;
        }
        if (input_values.contains("-") || input_values.contains(".")) {
            Pantools.logger.error("Numbers are only allowed to be seperated by a comma.");
            System.exit(1);
        }
        String[] input_array = input_values.split(",");
        for (String input_str : input_array) {
            skip_groupings.removeIf(name -> name.equals(input_str)); // the first time this function is run, input_str contains relaxation numbers, not similarities 
            int input_nr = Integer.parseInt(input_str);
            if (input_nr > 8) {
                continue;
            }
            String similarity = original_values[input_nr-1];
            selected_groupings.add(similarity);
            skip_groupings.removeIf(name -> name.equals(similarity));
        }
        if (print) {
            Pantools.logger.info("Selected groupings: {}", selected_groupings.toString().replace(" ","").replace("[","").replace("]",""));
            Pantools.logger.info("Skipping grouping: {}", skip_groupings.toString().replace(" ","").replace("[","").replace("]",""));
        }
        return skip_groupings;
    }
    
    /**
     * Run all 8 relaxation settings against the pangenome.
     * BUSCOs must be run via the 'busco_protein' function
     * 
     Settings
       D1 - similarity: 95%, mcl_inflation: 10.8, intersection: 0.08, contrast: 8
       D2 - similarity: 85%, mcl_inflation: 9.6, intersection: 0.07, contrast: 7
       D3 - similarity: 75%, mcl_inflation: 8.4, intersection: 0.06, contrast: 6
       D4 - similarity: 65%, mcl_inflation: 7.2, intersection: 0.05, contrast: 5
       D5 - similarity: 55%, mcl_inflation: 6.0, intersection: 0.04, contrast: 4
       D6 - similarity: 45%, mcl_inflation: 4.8, intersection: 0.03, contrast: 3
       D7 - similarity: 35%, mcl_inflation: 3.6, intersection: 0.02, contrast: 2
       D8 - similarity: 25%, mcl_inflation: 2.4, intersection: 0.01, contrast: 1
     */
    public void optimal_grouping(String scoringMatrix) {
        if (Mode.contains("FAST")) {
            FAST = true; // check if the F1 score dropped compared to the previous grouping, stop when this is the case
        }
        Pantools.logger.info("Finding the most optimal setting for 'group'.");
        create_directory_in_DB("group"); 
        String date = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
        ArrayList<String> skip_groupings = check_which_rn_to_use(true, NODE_VALUE);
        for(int i=0; i<=7; i++) {
            INTERSECTION_RATE = new double[] {0.08, 0.07, 0.06, 0.05, 0.04, 0.03, 0.02, 0.01}[i];
            MIN_NORMALIZED_SIMILARITY = new int[] {95, 85, 75, 65, 55, 45, 35, 25}[i];
            MCL_INFLATION = new double[]{10.8, 9.6, 8.4, 7.2, 6.0, 4.8, 3.6, 2.4}[i];
            CONTRAST = new double[] {8, 7, 6, 5, 4, 3, 2, 1 }[i];
            boolean exists = check_if_file_exists(WORKING_DIRECTORY + "group/" + MIN_NORMALIZED_SIMILARITY);
            String similarity_str = String.valueOf(MIN_NORMALIZED_SIMILARITY);
            if (skip_groupings.contains(similarity_str) || (exists && !FAST)) {
                continue;
            }
            boolean stop_grouping;
            if (exists && FAST) {
                stop_grouping = check_if_optimal_has_been_found_already(i, skip_groupings);
                if (stop_grouping) {
                    break; // stop the grouping
                } else {
                   continue; 
                }
            }
            Pantools.logger.info("Grouping {}, similarity = {}.", (i+1), MIN_NORMALIZED_SIMILARITY);
            first_generate_proteins = true;
            group(scoringMatrix);
            /*append_string_to_file_full_path(date + "\npantools optimal_grouping\nRuntime after grouping relaxation: " + (i+1) + "\n",
                    WORKING_DIRECTORY + "/log/used_commands");
            print_runtime(false, true);*/
            move_grouping(false);
            copy_file(WORKING_DIRECTORY + "pantools_homology_groups.txt", WORKING_DIRECTORY + "/group/" + MIN_NORMALIZED_SIMILARITY);
            if (i > 0 && i < 7 && FAST && !skip_groupings.contains(similarity_str)) { // 
                stop_grouping = check_if_optimal_has_been_found_already(i, skip_groupings);
                if (stop_grouping) {
                    break; // stop the grouping
                }
            }
        }   
        print_info = true; // compare_busco_to_grouping() now prints some info to user
        compare_busco_to_grouping(skip_groupings);
        String dir = WORKING_DIRECTORY + "optimal_grouping/";
        if (grouping_version == -1) {
            System.out.println("\rNo grouping is currently active. Use 'change_active_grouping'");
        } else {
             System.out.println("\rGrouping " + grouping_version + " is currently active.");
        } 
        if (INPUT_FILE != null) {
            Pantools.logger.info("Results written to:");
            Pantools.logger.info(" {}grouping_overview.csv", dir);
            Pantools.logger.info(" {}optimal_grouping.R", dir);
            Pantools.logger.info(" {}counts_per_busco.log", dir);
        }
    }
    
    /**
     * Prevents additional clustering rounds by asssuming the optimal grouping is found when the F-score drops one (--mode FASTEST) or two (--mode FAST) consecutive rounds.
     * @param iteration
     * @param original_skip_groupings the groupings that should already be skipped
     * @return 
     */
    public boolean check_if_optimal_has_been_found_already(int iteration, ArrayList<String> original_skip_groupings) {  
        if (INPUT_FILE == null) {
            Pantools.logger.error("No BUSCO set provided via --input-file or -if. Select the BUSCO output directory in the pangenome database.");
            System.exit(1);
        }
        ArrayList<String> all_groupings = new ArrayList<>(Arrays.asList("95","85","75","65","55","45","35","25"));
        String rn_to_test = "";
        for (int i=0; i <= iteration; i++) {
            if (original_skip_groupings.contains(all_groupings.get(i))) {
                continue;
            }
            rn_to_test += all_groupings.get(i) + ",";
        }
        ArrayList<String> skip_groupings = check_which_rn_to_use(false, rn_to_test);
        compare_busco_to_grouping(skip_groupings);
        boolean stop = check_optimal_grouping_output();
        return stop;
    }
    
    /**
     * 
     * @return 
     */
    public boolean check_optimal_grouping_output() {
        double highest = 0;
        boolean stop = false;
        ArrayList<Double> f_scores = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "/optimal_grouping/grouping_overview.csv"))) { 
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (line.startsWith("Relaxation")) {
                    continue;
                }
                String[] line_array = line.split(",");
                double f_score = Double.parseDouble(line_array[line_array.length-1]);
                if (f_score > highest) {
                    highest = f_score; 
                } 
                f_scores.add(f_score);
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read {}/optimal_grouping/grouping_overview.csv", WORKING_DIRECTORY);
            System.exit(1);
        }
        int countdown = 10;
        for (double f_score : f_scores) {
            if (f_score == highest) {
                countdown = 2;
            }
            countdown --;
            if (countdown == 0) { // stop
                stop = true;
                break;
            }
        }
        return stop;
    }
        
    public void read_grouping_input_for_optimal_grouping(String file_name, HashMap<String, String> mrna_hmgroup_map, HashMap<String, Integer> hmgroup_size_map,
            String[] csv_output_array) {
        
        boolean first = true;
        int sco_groups = 0, core_groups = 0, unique_groups = 0, total_groups = 0;
        try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "group/" + file_name))) {
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (line.startsWith("#grouping")) { // header
                    String[] line_array = line.split(",");
                    String[] line_array2 = line_array[0].split("grouping_v");
                    csv_output_array[0] = line_array2[1]; // the grouping version
                    continue;
                }
                String[] line_array = line.split(": ");
                String[] mrna_array = line_array[1].split(" ");
                int skip_count = 0;
                TreeSet<Integer> present_genomes = new TreeSet<>();
                for (String mrna_id : mrna_array) {
                    if (first) { // check if the first mrna/protein is in the correct format
                        if (!mrna_id.contains("#")) {
                            Pantools.logger.error("{}group/{} is not in the correct format. mRNA identifiers must have + # + genome number.", WORKING_DIRECTORY, file_name);
                            System.exit(1);
                        }
                        first = false;
                    }
                    String[] mrna_id_array = mrna_id.split("#");
                    int genome_nr = Integer.parseInt(mrna_id_array[1]);
                    if (skip_array[genome_nr-1]) {
                        skip_count ++;
                    } else {
                        present_genomes.add(genome_nr);
                        mrna_hmgroup_map.put(mrna_id, line_array[0]);
                    }
                }
                total_groups ++; 
                int group_size = mrna_array.length - skip_count;
                if (adj_total_genomes <= present_genomes.size()) {
                    core_groups ++;
                }
                if (adj_total_genomes == present_genomes.size() && group_size == present_genomes.size()) {
                    sco_groups ++;
                }
                if (present_genomes.size() == 1) {
                    unique_groups ++;
                }
                hmgroup_size_map.put(line_array[0], group_size);
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}group/{}", WORKING_DIRECTORY, file_name);
            System.exit(1);
        }
        csv_output_array[1] = total_groups + "";
        csv_output_array[2] = core_groups + "";
        csv_output_array[3] = sco_groups + "";
        csv_output_array[4] = unique_groups + ""; 
    }
   
    /**
     * @param skip_groupings 
     */
    public void compare_busco_to_grouping(ArrayList<String> skip_groupings) {
        check_database(); // starts up the graph database if needed
        create_directory_in_DB("optimal_grouping");
        if (INPUT_FILE == null) {
            Pantools.logger.info("No BUSCO set provided via --input-file/-if. Select the BUSCO output directory in the pangenome database.");
            return;
        }
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes  
            create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            update_skip_array_based_on_anno_ids();
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
        
        StringBuilder busco_log_builder = new StringBuilder("##This log file consists of two parts\n##Part 1. Selection of BUSCO genes\n");
        HashMap<String, ArrayList<String>> mrna_busco_map = prepare_busco_hashmap(busco_log_builder); // read results from 'busco_protein' analysis
        File folder = new File(WORKING_DIRECTORY + "group/");
        File[] listOfFiles = folder.listFiles();
        ArrayList correct_files = new ArrayList<>(Arrays.asList("95","85","75","65","55","45","35","25","OF")); 
        ArrayList<String> file_names = new ArrayList<>();
       
        StringBuilder r_input = new StringBuilder("Type,Mode,Score\nFP,0,1\n");
        String[][] csv_output_array = new String[9][12]; // D1-D8 & Homology groups, core, Single copy groups, unique, 
            // Correct groups, TP, FP, FN, Recall, Precision, F-score, grouping version
        String groupings_with_missing = "";
        int highest_number_in_plot = 0;
        for (File file : listOfFiles) { // go over grouping output files 
            String[] file_array = file.toString().split("/"); 
            String file_name = file_array[file_array.length-1];
            if (!correct_files.contains(file_name) || skip_groupings.contains(file_name)) { 
                continue;
            }
            if (print_info) {
                System.out.print("\rReading 'group' results: " + WORKING_DIRECTORY + "group/" + file_name);
            }
            busco_log_builder.append("#Grouping ").append(file_name).append("\n");
            file_names.add(file_name);
            HashMap<String, String> mrna_hmgroup_map = new HashMap<>(); // key is mrna node identifier, value is homology_group node identifier
            HashMap<String, Integer> hmgroup_size_map = new HashMap<>(); // key is homology_group node identifier, value is number of proteins in the group
            int[] statistics = new int[5]; // perfect_group_counter, true_pos, true_neg, false_pos, null_counter 
            int index = correct_files.indexOf(file_name); 
            read_grouping_input_for_optimal_grouping(file_name, mrna_hmgroup_map, hmgroup_size_map, csv_output_array[index]);
            for (String busco_id : mrna_busco_map.keySet()) {
                ArrayList<String> found_mrnas = mrna_busco_map.get(busco_id);
                busco_log_builder.append(busco_id).append("\n");
                HashMap<String, Integer> hm_count_map = count_different_groups_for_busco(found_mrnas, mrna_hmgroup_map, busco_log_builder);
                count_busco_true_false_pos_negatives(hm_count_map, statistics, busco_log_builder, hmgroup_size_map);
            }
            
            if (statistics[4] > 0) { // some mRNAs were not found in the grouping
                groupings_with_missing += file_name + ",";
            }
            if (statistics[2] > highest_number_in_plot) { 
                highest_number_in_plot = statistics[2];
            } else if (statistics[3] > highest_number_in_plot) { 
                highest_number_in_plot = statistics[3];
            }
            busco_log_builder.append("\n");
            add_stats_to_optimal_csv_overview(statistics, r_input, csv_output_array[index], index); 
        }
        if (print_info) {
            System.out.print("\r                                                                                       ");
        }
        
        create_optimal_grouping_csv_overview(csv_output_array, mrna_busco_map.size());
        write_SB_to_file_in_DB(r_input, "optimal_grouping/fn_fp.csv");
        print_warning_mrna_mismathes(groupings_with_missing, busco_log_builder);
        create_best_grouping_rscript(highest_number_in_plot);
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            check_current_grouping_version();
            tx.success();
        }
    }
    
    /**
     * not every mrna identifier from the grouping output files matches the current grouping in the pangenome
     * @param groupings_with_missing
     * @param busco_log_builder 
     */
    public void print_warning_mrna_mismathes(String groupings_with_missing, StringBuilder busco_log_builder) {
        if (groupings_with_missing.length() == 0) {
            write_SB_to_file_in_DB(busco_log_builder, "optimal_grouping/counts_per_busco.log");
        } else { // include a warning
            String warning = "IMPORANT! Scores are lower for certain groupings because the mRNA identifiers from the BUSCO output did not match the identifiers of the grouping";
            Pantools.logger.warn(warning);
            Pantools.logger.warn("Please see {}optimal_grouping/counts_per_busco.log", WORKING_DIRECTORY);
            write_string_to_file_in_DB(warning + ": " + groupings_with_missing.replaceFirst(".$","") +
                    "\nFind the non matching mRNAs by searching for 'not found' in this file\n\n" + busco_log_builder.toString(), "optimal_grouping/counts_per_busco.log");
        }
    }
    
    /**
     * 
     * @param statistics perfect_group_counter, true_pos, true_neg, false_pos, null_counter 
     * @param r_input
     * @param csv_output_array
     * @param index 
     */
    public void add_stats_to_optimal_csv_overview(int[] statistics, StringBuilder r_input, String[] csv_output_array, int index) {
        double recall = statistics[1];
        double precision = statistics[1];
        recall = recall / (statistics[2] + statistics[1]);
        precision = precision / (statistics[3] + statistics[1]);
        double rec_prec = recall * precision;
        double rec_prec2 = recall + precision;
        double f_score = 2.0 * rec_prec / rec_prec2;
       
        if (index <= 7) {
            r_input.append("FN,").append((index+1)).append(",").append(statistics[2])
                    .append("\nFP,").append((index+1)).append(",").append(statistics[3]).append("\n");
        } else { // in case orthofinder output is included
            int fn = statistics[2];
            if (fn == 0) {
                fn = 1; // set to 1 to make sure the text stays in the plot 
            }
            int fp = statistics[3];
            if (fp == 0) {
                fp = 1;
            }
            String add_str = "geom_point(aes(x=1, y=" + statistics[2] + "), colour=\"#0285cc\", size = 3) +\n"
                + "annotate(geom=\"text\", x=2, y=" + fn + ", label=\"Orthofinder FN\", color=\"#0285cc\", size = 7) +\n"
                + "geom_point(aes(x=1, y=" + statistics[3] + "), colour=\"#0033b3\", size = 3) +\n"
                + "annotate(geom=\"text\", x=2, y=" + fp + ", label=\"Orthofinder FP\", color=\"#0033b3\", size = 7) + \n";
            write_string_to_file_in_DB(add_str, "optimal_grouping/orthofinder_benchmark");
        }
        csv_output_array[5] = statistics[0] + "";
        csv_output_array[6] = statistics[1] + "";
        csv_output_array[7] = statistics[3] + "";
        csv_output_array[8] = statistics[2] + "";
        csv_output_array[9] = recall + "";
        csv_output_array[10] = precision + "";
        csv_output_array[11] = f_score + "";
    } 
    
    /**
     * Two loops. 
     * First check if the group is perfect, a single copy group with only BUSCO genes
     * 
     * When the group is not perfect, go through second loop
     * Second. Go over groups, use the group with most BUSCOs to calculate TP FP.
     * BUSCOs in remaining groups are counted as FN
     * 
     * @param hm_count_map
     * @param statistics perfect_group_counter, true_pos, true_neg, false_pos, null_counter 
     * @param busco_log_builder
     * @param hmgroup_size_map 
     */
    public void count_busco_true_false_pos_negatives(HashMap<String, Integer> hm_count_map, int[] statistics, StringBuilder busco_log_builder, 
            HashMap<String, Integer> hmgroup_size_map) {
        
        int highest = 0, biggest_grp = 0;
        boolean perfect = false; // perfect groups contain busco genes only and are single copy 
        for (String group : hm_count_map.keySet()) { // first loop. check if group is perfect, all BUSCO genes and SCO 
            if (group == null) {
                statistics[4] ++;
                continue;
            }
            int hm_counter = hm_count_map.get(group);
            int group_size = hmgroup_size_map.get(group);
            if (hm_counter > highest) {
                highest = hm_counter; 
                biggest_grp = group_size;
            }
            if (group_size == adj_total_genomes && hm_counter == adj_total_genomes && hm_count_map.size() == 1) {
                statistics[0] ++;
                statistics[1] += adj_total_genomes;
                perfect = true;
                busco_log_builder.append(" perfect -> ").append(group).append(" (").append(hm_counter).append(" sequences): ")
                        .append(adj_total_genomes).append(" TP\n" );
            } 
        }
               
        if (perfect) {
            return;
        }
        boolean first = true;
        for (String group : hm_count_map.keySet()) {
            if (group == null) {
                 continue;
            }
            int hm_counter = hm_count_map.get(group);
            int size = hmgroup_size_map.get(group);
            if ((hm_counter == highest) && (size == biggest_grp) && first) { // if multiple groups are the largest, only the first is selected for true and false positives
                busco_log_builder.append(" best group -> ").append(group).append(" (").append(size).append(" sequences): ")
                        .append(hm_counter).append( " TP, ").append((size-hm_counter)).append(" FP\n");
                statistics[1] += hm_counter;
                statistics[3] += (size-hm_counter);
                first = false;
            } else {
                busco_log_builder.append(" incorrect group -> ").append(group).append(" (").append(size).append(" sequences): ").append(hm_counter).append( " TN\n");
                statistics[2] += hm_counter;
            } 
        }
    }
    
    /**
      Go over the mRNAs of a BUSCO. Get the homology group that belongs to an mRNA.
     * @param found_mrnas
     * @param mrna_hmgroup_map
     * @param busco_log_builder
     * @return 
     */
    public HashMap<String, Integer> count_different_groups_for_busco(ArrayList<String> found_mrnas, HashMap<String, String> mrna_hmgroup_map, 
            StringBuilder busco_log_builder) {
        
        HashMap<String, Integer> hm_count_map = new HashMap<>();
        for (String mrna_id : found_mrnas) {
            String hmgroup = mrna_hmgroup_map.get(mrna_id); 
            if (hmgroup == null) {
                busco_log_builder.append(" ").append(mrna_id).append(" not found" + "\n");
            }
            try_incr_hashmap(hm_count_map, hmgroup, 1);
        }
        return hm_count_map;
    }
      
    /*
    Requires 
      -database-path/-dp 
    
    Optional 
        --mode fast will only show grouping versions
    */
    public void grouping_overview() {
        Pantools.logger.info("Creating an overview of all homology groups in the pangenome.");
        check_database(); // starts up the graph database if needed
        HashMap<Integer, StringBuilder> hm_info_map = new HashMap<>(); 
        TreeSet<Integer> present_versions = new TreeSet<>();
        StringBuilder output_builder = new StringBuilder();
        int total_groups, total_inactive_groups;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start a new database transaction
            pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            total_groups = (int) count_nodes(HOMOLOGY_GROUP_LABEL);
            total_inactive_groups = (int) count_nodes(INACTIVE_HOMOLOGY_GROUP_LABEL);
            if (total_groups == 0 && total_inactive_groups == 0) {
                Pantools.logger.error("No homology groups available. Run 'group' first.");
                System.exit(1);
            } 
            if (Mode.contains("FAST") || FAST) { // Mode.contains("FAST") was the old method --mode fast, do not remove yet
                show_available_grouping(total_groups, true);
            } else {
                put_hmgroups_in_map_version_in_set(hm_info_map, present_versions, HOMOLOGY_GROUP_LABEL, "active");
                output_builder.append("Active homology group version: ").append(present_versions).append("\n");
                put_hmgroups_in_map_version_in_set(hm_info_map, present_versions, INACTIVE_HOMOLOGY_GROUP_LABEL, "inactive");
                output_builder.append("All homology group versions: ").append(present_versions).append("\n");
                for (Map.Entry<Integer, StringBuilder> entry : hm_info_map.entrySet()) {
                    Integer key = entry.getKey();
                    StringBuilder value = entry.getValue(); 
                    String value_str = value.toString();
                    output_builder.append("\n#Version ").append(key).append(": ");
                    if (pangenome_node.hasProperty("grouping_v" + key)) { // this property is standard in later versions
                        String group_settings = (String) pangenome_node.getProperty("grouping_v" + key);
                        output_builder.append(group_settings);
                    }
                    output_builder.append("\n").append(value_str).append("\n");
                }
            }
            tx.success(); // transaction successful, commit changes
        }
       
        if (!Mode.contains("FAST") || FAST || total_inactive_groups > 0) {
            write_SB_to_file_in_DB(output_builder, "group/grouping_overview.txt");
            Pantools.logger.info("Output written to:");
            Pantools.logger.info(" {}group/grouping_overview.txt", WORKING_DIRECTORY);
            if (total_groups > 0) {
                Pantools.logger.info(" {}pantools_homology_groups.txt", WORKING_DIRECTORY);
            }
        }
    } 
    
    /**
     * 
     * @param total_groups
     * @param check_active_grouping 
     */
    public void show_available_grouping(long total_groups, boolean check_active_grouping) {
        int group_version = 0;
        if (total_groups != 0) {
            Node hm_node = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL).next();
            group_version = (int) hm_node.getProperty("group_version");
        } 
         
        for (int i=1; i < 100; i++) {
            if (pangenome_node.hasProperty("grouping_v" + i)) { // this property is standard in later versions
                String group_settings = (String) pangenome_node.getProperty("grouping_v" + i);
                Pantools.logger.info("Grouping {}: {}.", i, group_settings);
            }
        }
        if (group_version == 0 && check_active_grouping) {
            Pantools.logger.info("There is no grouping active.");
        } else if (check_active_grouping) {
            Pantools.logger.info("Active grouping version: {}.", group_version);
        }
    }
    
    /**
     * 
     * @param gene_nodes_map
     * @param present_versions
     * @param label1 'homology_group_label' or 'inactive_homology_group_label'
     * @param active_or_inactive 
     */
    public void put_hmgroups_in_map_version_in_set(HashMap<Integer, StringBuilder> gene_nodes_map, TreeSet<Integer> present_versions, 
            Label label1, String active_or_inactive) {
        
        ResourceIterator<Node> hm_nodes = GRAPH_DB.findNodes(label1);
        StringBuilder output_builder = new StringBuilder();
        boolean active = true;
        if (active_or_inactive.contains("inactive")) {
            active = false;
        }
        int total_groups = (int) count_nodes(label1);
        long counter = 0;
        while (hm_nodes.hasNext()) { 
            counter ++;
            if (counter % 100 == 0 || counter == total_groups ) {
                System.out.print("\rFinding " + active_or_inactive + " homology groups: " + counter + "/" + total_groups + " groups");
            }
            Node hm_node = hm_nodes.next();
            long hm_id = hm_node.getId();
            int num_members = (int) hm_node.getProperty("num_members");
            int group_version = (int) hm_node.getProperty("group_version");
            present_versions.add(group_version);
            int[] original_copy_number = (int[]) hm_node.getProperty("copy_number");
            int[] copy_number = remove_first_position_array(original_copy_number);
            try_incr_SB_hashmap(gene_nodes_map, group_version, hm_id + ", " + num_members + " members\n" + Arrays.toString(copy_number) + "\n");
            if (active) {
                StringBuilder line = new StringBuilder();
                line.append(hm_id).append(": ");
                Iterable<Relationship> hm_rels = hm_node.getRelationships(RelTypes.has_homolog);
                for (Relationship rel : hm_rels) {
                    Node mrna_node = rel.getEndNode();
                    String mrna_id = (String) mrna_node.getProperty("protein_ID");
                    int genome_nr = (int) mrna_node.getProperty("genome");
                    line.append(mrna_id).append("#").append(genome_nr).append(",");
                }
                String line_str = line.toString().replaceFirst(".$",""); // remove last character 
                output_builder.append(line_str).append("\n");
            }
        }
        if (active && !present_versions.isEmpty()) {
            String grouping_info = (String) pangenome_node.getProperty("grouping_v" + present_versions.first());
            write_string_to_file_in_DB("#grouping_v" + present_versions.first() + "," + grouping_info + "\n" + output_builder.toString(), 
                "pantools_homology_groups.txt");
        } 
        if (present_versions.isEmpty()) {
            Pantools.logger.info("No {} homology groups are found.", active_or_inactive);
        }
    }
    
    /**
     * remove_homology_groups(), remove_grouping()
     * 
     * Delete 'is_similar' relations, 'component' property
     * 
     * Requires 
     * -dp 
     * 
     * Optional
     * --version
     * --mode fast
    */
    public void remove_grouping() {
        Pantools.logger.info("Removing homology groups from the pangenome.");
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            report_number_of_threads(); //do not print how many threads were given, only when more than 1 thread was selected
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
      
        if (!FAST) { // do not remove 'is_similar' relations. Leaving these does not influence the next grouping
            delete_is_similar_to_relations();
        } else if (!Mode.equals("0")) {
            Pantools.logger.error("--mode argument not recognized.");
            System.exit(1);
        }
          
        ArrayList<Node> hm_nodes_list;
        remove_component_from_mrna_nodes(pangenome_node); // removes 'component' property. These are a remainder from the previous group run
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            grouping_version = check_highest_grouping_version();
            if (grouping_version == 0) {
                grouping_version ++; 
            }
            
            boolean last_grouping_successful = was_the_last_grouping_successful();
            hm_nodes_list = retrieve_hmgroup_nodes_for_selected_grouping(); // sets 'grouping_version' based on --version value 
            rm_grouping_v_from_pangenome_node(last_grouping_successful);
            tx.success();
        }
        remove_hmgroups_and_relations(hm_nodes_list);
    }
    
    /**
     * Remove 'grouping_v' properties in the 'pangenome' node 
     * Changes the highest_grouping property in the 'pangenome' node
     * 
     * @param last_grouping_successful 
     */
    public void rm_grouping_v_from_pangenome_node(boolean last_grouping_successful) {
        Iterable<String> property_keys;
        switch (grouping_version) {
            case -1: // all inactive hmgroup nodes will be removed
                ResourceIterator<Node> hmgroup_nodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL);
                int active_grouping_version = 0; // stays 0 if there is no active grouping
                while (hmgroup_nodes.hasNext()) {
                    Node hm_node = hmgroup_nodes.next(); // nodes are stored in a list so the transasction can be commited frequently
                    active_grouping_version = (int) hm_node.getProperty("group_version");
                    break;
                }
               
                property_keys = pangenome_node.getPropertyKeys();
                for (String property_key : property_keys) {
                    if (property_key.startsWith("grouping_v") && !property_key.equals("grouping_v" + active_grouping_version)) {
                        pangenome_node.removeProperty(property_key); // remove versions that do not match the active one 
                    }
                }
                pangenome_node.removeProperty("highest_grouping"); // as only the active grouping remains, it should become the highest 
                pangenome_node.setProperty("highest_grouping", active_grouping_version); 
                break;
            case -2: // all homology group nodes will be removed
                property_keys = pangenome_node.getPropertyKeys();
                for (String property_key : property_keys) {
                    if (property_key.startsWith("grouping_v")) {
                        pangenome_node.removeProperty(property_key);
                    }
                }
                pangenome_node.removeProperty("highest_grouping"); // no more groupings present so remove this
                for (int i = 25; i < 96; i += 10) {
                    delete_file_in_DB("group/" + i);
                } 
                break;
            default:
                // hmgroup nodes of a specific grouping will be removed
                int original_grouping_version;
                ArrayList<Integer> available_versions = new ArrayList<>();
                for(int i=1; i<= 100; i++) {
                    if (pangenome_node.hasProperty("grouping_v" + i)) {
                        available_versions.add(i);
                    }
                }   
                if (available_versions.isEmpty() && !last_grouping_successful) { // the first run of a user failed or group was never run before 
                    available_versions.add(0);
                }
                original_grouping_version = grouping_version;
                if (grouping_version > available_versions.get(available_versions.size()-1)) {
                    grouping_version = available_versions.get(available_versions.size()-1);
                    Pantools.logger.info("Newest highest grouping is {}.", grouping_version);
                    put_grouping_version(false);
                } else if (grouping_version == available_versions.get(available_versions.size()-1)) { // This was the highest grouping, thus the most recent
                    if (available_versions.size() == 1) {
                        grouping_version = 0;
                        pangenome_node.removeProperty("last_grouping_successful" + original_grouping_version); // this property cannot be removed for other grouping versions!
                    } else {
                        grouping_version = available_versions.get(available_versions.size()-2);
                    }
                    Pantools.logger.info("Newest highest grouping is {}.", grouping_version);
                    put_grouping_version(false);
                }
                pangenome_node.removeProperty("grouping_v" + original_grouping_version + "_skipped_genomes");
                break;
        }
    }
    
    /**
     * Extracts the 'last_grouping_successful' property
     * @return 
     */
    public boolean was_the_last_grouping_successful() {
        boolean last_grouping_successful;
        try {
            last_grouping_successful = (boolean) pangenome_node.getProperty("last_grouping_successful");
        } catch (NotFoundException nfe) {
           last_grouping_successful = false; // property does not exists yet when 'group' never has been run 
        }
        return last_grouping_successful;
    }
    
    /**
     * --version must be a number, 'all' or 'all_inactive'
     */
    public void check_if_version_is_correct() {
        if (GROUPING_VERSION == null && target_genome != null) { // --reference is updated to --version
            GROUPING_VERSION = target_genome;
        }
        
        if (GROUPING_VERSION == null) {
            return;
        }
        try {
            int number = Integer.parseInt(GROUPING_VERSION);
        } catch (NumberFormatException nfe) {
            if (!GROUPING_VERSION.equals("ALL_INACTIVE") && !GROUPING_VERSION.equals("ALL")) {
                Pantools.logger.info(" {}", GROUPING_VERSION);
                Pantools.logger.error("--version must be a number, 'all' or 'all_inactive.");
                System.exit(1);
            }
        }
    }
    
    /**
     * hmgroup nodes with a version number that match the input number are put in a list
     * Sets 'grouping_version' 
     * 
     * @return arraylist with 'homology_group' nodes
     */
    public ArrayList<Node> retrieve_hmgroup_nodes_for_selected_grouping() { 
        ResourceIterator<Node> hm_nodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL, "group_version", grouping_version); // takes the groups from the active grouping
        ArrayList<Node> hm_nodes_list = new ArrayList<>();
        check_if_version_is_correct();
        if (GROUPING_VERSION == null) { // if no grouping has been given, use the version that is active
            check_current_grouping_version(); // automatically changes grouping_version
            if (grouping_version == -1) {
                Pantools.logger.error("No grouping was selected and no grouping is active.");
                System.exit(1);
            } else {
                Pantools.logger.info("No grouping was selected with --version. Taking the active one {}.", grouping_version);
                pangenome_node.removeProperty("last_grouping_successful");
            }  
            hm_nodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL, "group_version", grouping_version);
        } else if (GROUPING_VERSION.equals("ALL_INACTIVE")) { // remove ALL inactive groups
            hm_nodes = GRAPH_DB.findNodes(INACTIVE_HOMOLOGY_GROUP_LABEL);
            grouping_version = -1;
        } else if (GROUPING_VERSION.equals("ALL")) { // remove both active and inactive groups
            hm_nodes = GRAPH_DB.findNodes(INACTIVE_HOMOLOGY_GROUP_LABEL);
            while (hm_nodes.hasNext()) {
                Node hm_node = hm_nodes.next();
                hm_nodes_list.add(hm_node); // nodes are stored in a list so the transasction can be commited frequently
            }  
            hm_nodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL); // are put in the list at the end of this function
            grouping_version = -2;
        } else { // a specific grouping version was given by the user. It's either active or inactive
            Pantools.logger.info("Grouping {} was selected.", GROUPING_VERSION);
            grouping_version = Integer.parseInt(GROUPING_VERSION);
            ResourceIterator<Node> hm_nodes1 = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL, "group_version", grouping_version);
            ResourceIterator<Node> hm_nodes2 = GRAPH_DB.findNodes(INACTIVE_HOMOLOGY_GROUP_LABEL, "group_version", grouping_version);
            boolean present1 = false, present2 = false;
            if (hm_nodes1.hasNext()) { // check if any node is present 
                present1 = true;
                Node hm_node = hm_nodes1.next();
            }
            if (hm_nodes2.hasNext()) { // check if any node is present 
                present2 = true;
                Node hm_node = hm_nodes2.next();
            }
               
            if (present1 && !present2) { // the grouping is active 
                hm_nodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL, "group_version", grouping_version);
            } else if (!present1 && present2) { // the grouping is inactive 
                hm_nodes = GRAPH_DB.findNodes(INACTIVE_HOMOLOGY_GROUP_LABEL, "group_version", grouping_version);
               // active_grouping = false;
            } else if (!present1 && !present2) {
                Pantools.logger.error("Grouping version {} does not exist.", GROUPING_VERSION);
                System.exit(1);
            } else { // both true
                Pantools.logger.error("both true. this is not allowed.");
                System.exit(1);
            }
        }
         
        while (hm_nodes.hasNext()) {
            Node hm_node = hm_nodes.next();
            hm_nodes_list.add(hm_node); // nodes are stored in a list so the transasction can be commited frequently
        }
        return hm_nodes_list;
    }
    
    
    /**
     * Remove the nodes and (relations connected to it), that were placed in the arraylist
     * @param hm_nodes_list
     */
    public void remove_hmgroups_and_relations(ArrayList<Node> hm_nodes_list) {
        MAX_TRANSACTION_SIZE = 100000;
        if (hm_nodes_list.isEmpty()) {
            Pantools.logger.info("No homology groups are present.");
            return;
        } else {
            Pantools.logger.trace("Selected {} homology groups.", hm_nodes_list.size());
        }
        
        int trsc = 0, node_counter = 0;
        Transaction tx = GRAPH_DB.beginTx();
        try {
            for (Node hm_node : hm_nodes_list) {
                node_counter ++;
                if (node_counter % 10 == 0 || node_counter == hm_nodes_list.size()) {
                    System.out.print("\rDeleting homology groups: " + node_counter + "/" + hm_nodes_list.size()); 
                }
                Iterable<Relationship> has_homolog_rels = hm_node.getRelationships();
                for (Relationship rel : has_homolog_rels) {
                    rel.delete();
                    ++trsc; 
                }
                if (trsc >= MAX_TRANSACTION_SIZE) {
                    tx.success(); // transaction successful, commit changes
                    tx.close();
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                    trsc = 0;
                }  
                hm_node.delete();
            }
            tx.success(); // transaction successful, commit changes
        } finally {
            tx.close();
        }
        Pantools.logger.info("Successfully removed the homology groups.");
    }
    
    /**
     * Delete the 'is_similar' relations between mRNA nodes. 
     * Transactions must be committed frequently to prevent the program from running out of memory
     */
    public void delete_is_similar_to_relations() {
        boolean present = false;
        for (int i = 1; i <= total_genomes; ++i) {
            int trsc = 0;
            Transaction tx = GRAPH_DB.beginTx();
            try {
                ResourceIterator<Node> mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "genome", i);
                int rel_count = 0;
                System.out.print("\rDeleting 'is_similar_to' relationships between mRNA nodes. Genome " + i + "                          "); 
                while (mrna_nodes.hasNext()) {
                    Node mrna_node = mrna_nodes.next();
                    if (rel_count % 100 == 0 && rel_count > 0) {
                        System.out.print("\rDeleting 'is_similar_to' relationships between mRNA nodes. "
                                + "Genome " + i + ": " + rel_count + " relations           "); 
                    }
                    Iterable<Relationship> similar_rels = mrna_node.getRelationships(RelTypes.is_similar_to);
                    for (Relationship sim_rel : similar_rels) {
                        sim_rel.delete();
                        ++trsc; 
                        rel_count ++;
                    }
                    if (trsc >= 10000) { // Commit changes every 10K relations
                        tx.success();
                        tx.close();
                        tx = GRAPH_DB.beginTx(); // start a new database transaction
                        mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "genome", i);
                        trsc = 0;
                    }  
                }
                tx.success(); // transaction successful, commit changes
            } finally {
                tx.close();
            }
        }
    }
    
    /**
     * 
     * @param add_info 
     */
    public void put_grouping_version(boolean add_info) {
        pangenome_node.removeProperty("highest_grouping");
        pangenome_node.setProperty("highest_grouping", grouping_version);
        if (add_info) { // only happens during 'group' 
            String longest_transcript = "";
            if (longest_transcripts) {
                longest_transcript = ", Only longest gene transcripts";
            }
            pangenome_node.setProperty("grouping_v" + grouping_version, "similarity: " + MIN_NORMALIZED_SIMILARITY + ", inflation: " + MCL_INFLATION +
                    ", intersection rate " + INTERSECTION_RATE + ", contrast: " + CONTRAST + longest_transcript);
            if (!skip_list.isEmpty()) {
                pangenome_node.setProperty("grouping_v" + grouping_version + "_skipped_genomes", skip_list.toString().replace("[","").replace("]","").replace(" ",""));
            } 
        }
    }
    
    /**
     * Requires 
     * --database-path or -dp
     * --version
     */
    public void change_active_grouping() {
        Pantools.logger.info("Changing 'inactive_homology_groups' back to 'homology_groups'.");
        delete_file_in_DB("log/active_grouping_version");
        report_number_of_threads(); //print how many threads were given, only 1 thread allowed
        check_database(); // starts up the graph database if needed
        if (target_genome != null && GROUPING_VERSION == null) {
            GROUPING_VERSION = target_genome; // --reference argument is changed to --version in manual
        }
       
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves 'K_size' & 'total_genomes' variables
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
        if (GROUPING_VERSION == null) {
            Pantools.logger.info("No grouping version selected via --version.");
            try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
                show_available_grouping(1, true);
                tx.success(); // transaction successful, commit changes
            }
            return;
        }
       
        int selected_version;
        try {
            selected_version = Integer.parseInt(GROUPING_VERSION);
        } catch (NumberFormatException nfe) {
            Pantools.logger.info("--version must be a number.");
            return;
        }
      
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            show_available_grouping(0, false);
            check_current_grouping_version(); // check which version of homology grouping is active
            if (grouping_version == selected_version) {
                Pantools.logger.info(".Grouping version {} is already active.", GROUPING_VERSION);
                return;
            }
           
            int total_inactive_groups = (int) count_nodes(INACTIVE_HOMOLOGY_GROUP_LABEL);
            if (total_inactive_groups == 0) {
                Pantools.logger.info(".No other grouping is available.");
                return;
            }
            long selected_nodes = count_nodes(INACTIVE_HOMOLOGY_GROUP_LABEL, "group_version", selected_version);
            if (selected_nodes == 0) {
                Pantools.logger.info("No homology groups were found matching version '{}'.", selected_version);
                return;
            }
            Pantools.logger.info("Found {} homology groups matching version '{}'.", selected_nodes, selected_version);
            tx.success(); // transaction successful, commit changes
        }
        
        move_grouping(false); // relabel the current active grouping as unactive
        activate_inactive_grouping(selected_version);
        write_string_to_file_in_DB("Active grouping version is " + GROUPING_VERSION +"\n", "log/active_grouping_version");
        Pantools.logger.info("Updated file: {}pantools_homology_groups.txt", WORKING_DIRECTORY);
    }
    
    /*
     Removes 'inactive_homology_group' label and adds 'homology_group' label to the selected nodes 
    */
    public void activate_inactive_grouping(int selected_grouping_version) {
        MAX_TRANSACTION_SIZE = 10000;
        StringBuilder output_builder = new StringBuilder();
        Transaction tx = GRAPH_DB.beginTx(); // start database transaction
        int trsc = 0;
        try {
            ResourceIterator<Node> hm_nodes = GRAPH_DB.findNodes(INACTIVE_HOMOLOGY_GROUP_LABEL, "group_version", selected_grouping_version);
            String grouping_info = (String) pangenome_node.getProperty("grouping_v" + selected_grouping_version);
            output_builder.append("#grouping_v").append(selected_grouping_version).append(", ").append(grouping_info).append("\n");
            ArrayList<Node> hm_node_list = new ArrayList<>();
            while (hm_nodes.hasNext()) {
                Node hm_node = hm_nodes.next(); // nodes are stored in a list so the transasction can be commited frequently
                hm_node_list.add(hm_node);
            }
            int node_counter = 0;
            for (Node hm_node : hm_node_list) {
                node_counter ++;
                if (node_counter % 111 == 0 || node_counter < 100) {
                    System.out.print("\rActivating homology groups: " + node_counter + " nodes   ");
                }
                hm_node.removeLabel(INACTIVE_HOMOLOGY_GROUP_LABEL);
                hm_node.addLabel(HOMOLOGY_GROUP_LABEL);
                output_builder.append(hm_node.getId()).append(":");
                Iterable<Relationship> all_rels = hm_node.getRelationships(); 
                for (Relationship rel : all_rels) {
                    Node mrna_node = rel.getEndNode();
                    String protein_ID = (String) mrna_node.getProperty("protein_ID");
                    output_builder.append(" ").append(protein_ID);
                    hm_node.createRelationshipTo(mrna_node, RelTypes.has_homolog);
                    rel.delete();
                    ++trsc; 
                    if (trsc >= MAX_TRANSACTION_SIZE) {
                        tx.success(); // transaction successful, commit changes
                        tx.close();
                        tx = GRAPH_DB.beginTx(); // start a new database transaction
                        trsc = 0;
                    }
                }
                output_builder.append("\n");
            }
            System.out.print("\rActivating homology_group: " + node_counter + " nodes   ");
            pangenome_node.removeProperty("last_grouping_successful");
            pangenome_node.setProperty("last_grouping_successful", true);
            tx.success(); // transaction successful, commit changes
            tx.close();
        } finally {
           tx.close();
        }
        write_SB_to_file_in_DB(output_builder, "pantools_homology_groups.txt");
    }
    
    /**
     * 
     * @param hm_node 
     */
    public void check_if_grouping_v_exists(Node hm_node) {
        int group_version = (int) hm_node.getProperty("group_version");
        if (!pangenome_node.hasProperty("grouping_v" + group_version)) {
            Pantools.logger.error("This is a grouping that was not completely finished yet, please use 'remove_grouping'.");
            System.exit(1);
        }
    }
    
    /**
     * Change the label of 'homology_group' nodes into 'inactive_homology_group' 
     * 
     * When run from the command line, only requires --database-path/-dp 
     * @param standalone
     */
    public void move_grouping(boolean standalone) {
        MAX_TRANSACTION_SIZE = 10000;
        check_database(); // starts up the graph database if needed
        if (standalone) {
            Pantools.logger.info("Moving the homology nodes so 'group' can be run again.");
            report_number_of_threads(); //print how many threads were given, only 1 thread allowed
        }
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            total_genomes = (int) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("num_genomes");
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start database.");
            System.exit(1);
        }
        remove_component_from_mrna_nodes(pangenome_node); // removes 'component' property that is only used for grouping the proteins. 
      
        Transaction tx = GRAPH_DB.beginTx(); // start database transaction
        try {
            ResourceIterator<Node> hm_nodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL);
            long node_counter = 0;
            int trsc = 0;
            ArrayList<Node> hmgroup_node_list = new ArrayList<>();
            boolean first_node = true;
            while (hm_nodes.hasNext()) {
                Node hm_node = hm_nodes.next();
                hmgroup_node_list.add(hm_node);
                if (first_node) {
                    check_if_grouping_v_exists(hm_node);
                    first_node = false;
                }
            }
            for (Node hm_node : hmgroup_node_list) {
                node_counter ++;
                if (node_counter % 10 == 0) {
                    System.out.print("\rMoving homology group nodes: " + node_counter + " nodes");
                }
                Iterable<Relationship> all_rels = hm_node.getRelationships(RelTypes.has_homolog);
                hm_node.removeLabel(HOMOLOGY_GROUP_LABEL);
                hm_node.addLabel(INACTIVE_HOMOLOGY_GROUP_LABEL);
                for (Relationship rel : all_rels) {
                    Node mRNA_node = rel.getEndNode();
                    hm_node.createRelationshipTo(mRNA_node, RelTypes.has_inactive_homolog);
                    ++trsc; 
                    rel.delete(); // delete the 'has_homolog' relationship
                }
                ++trsc; 
                if (trsc >= MAX_TRANSACTION_SIZE) {
                    tx.success(); // transaction successful, commit changes
                    tx.close();
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                    trsc = 0;
                }  
            }
            tx.success(); // transaction successful, commit changes
        } finally {
           tx.close();
        }
    }
    
    /**
     * Stop when there are active homology groups.
     */
    public void stop_if_hmgroups_exist() { 
        ResourceIterator<Node> hm_node = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL);
        if (hm_node.hasNext()) {
            Pantools.logger.error("Homology groups are already present. Use either 'move_grouping' or 'remove_grouping'.");
            System.exit(1);
        }
    }
    
    /**
     * Removes 'component' property that is only used for grouping the proteins. These are a remainder from previous runs
     * @param pangenome_node
     */
    public void remove_component_from_mrna_nodes(Node pangenome_node) {
        MAX_TRANSACTION_SIZE = 10000;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            if (!pangenome_node.hasProperty("components_presents")) {
                return;
            }
            tx.success();
        }
        for (int i = 1; i <= total_genomes; ++i) {
            System.out.print("\rRemoving 'component' from mRNA nodes. Genome " + i + "                                            "); 
            int trsc = 0;
            Transaction tx = GRAPH_DB.beginTx(); // start database transaction
            try {
                ResourceIterator<Node> mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "genome", i);
                ArrayList<Node> mrna_node_list = new ArrayList<>();
                while (mrna_nodes.hasNext()) {
                    Node mrna_node = mrna_nodes.next();
                    mrna_node_list.add(mrna_node);
                }
                int node_count = 0;
                for (Node mrna_node : mrna_node_list) {
                    node_count ++;
                    if (node_count % 51 == 0) {
                        System.out.print("\rRemoving 'component' from mRNA nodes. Genome " + i + ": " + node_count + " nodes           "); 
                    }
                    if (mrna_node.hasProperty("component")) {
                        mrna_node.removeProperty("component");
                        ++trsc; 
                    }
                   
                    if (trsc >= MAX_TRANSACTION_SIZE) {
                        tx.success(); // transaction successful, commit changes
                        tx.close();
                        tx = GRAPH_DB.beginTx(); // start a new database transaction
                        trsc = 0;
                    }  
                }
                tx.success(); // transaction successful, commit changes
            } finally {
                tx.close();
            }
        }
        System.out.print("\r                                                              ");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            pangenome_node.removeProperty("components_presents"); 
            tx.success();
        }
    }
    
    /**
     * Write the results from 'optimal_grouping' to a csv formatted table
     * @param csv_output_array
     * @param busco_groups 
     */
    public void create_optimal_grouping_csv_overview(String[][] csv_output_array, int busco_groups) {
        String[] similarity_array = new String[] {"95","85","75","65","55","45","35","25","OF"}; 
        StringBuilder output_builder = new StringBuilder("Relaxation mode,Minimum sequence similarity,Grouping version,Homology groups,Core groups,"
                + "Single copy orthologous groups,Unique groups,Correct groups (" + busco_groups + "),True Positives,False Positives,False Negatives,"
                + "Recall,Precision,F-score\n");
        for (int i = 0; i < csv_output_array.length; i++) {
            if (csv_output_array[i][1] == null) {
                continue;
            }
            if (i == 8) {
                output_builder.append("Orthofinder,?,");
            } else {
               output_builder.append("D").append((i+1)).append(",").append(similarity_array[i]).append(",");
            }
            
            for (int j = 0; j < csv_output_array[i].length; j++) {
                if (j == 0 && csv_output_array[i][j] == null) {
                    output_builder.append("-,");
                } else {
                    output_builder.append(csv_output_array[i][j]).append(",");
                }
            }
            output_builder.append("\n");
        }
        write_SB_to_file_in_DB(output_builder, "optimal_grouping/grouping_overview.csv");
    }
    
    /**
     * //two input files are possible 'protein or protein_longest_transcript', in case only the outer directory is provided, first check for longest_transcript
     * @return 
     */
    public String find_busco_input() {
        String busco_path = INPUT_FILE + "/protein/";
        boolean found_input = false, all_protein_exists = true, all_protein_longest_exists = true;
        if (INPUT_FILE.endsWith("protein") || INPUT_FILE.endsWith("protein/")) {
            found_input = true;
            busco_path = INPUT_FILE + "/";
        } else if (INPUT_FILE.endsWith("protein_longest_transcript") || INPUT_FILE.endsWith("protein_longest_transcript/")) {
            found_input = true;
            busco_path = INPUT_FILE + "/";
        }
       
        if (!found_input) {
            TreeSet<Integer> missing_genomes = new TreeSet<>();
            for (int i = 1; i <= total_genomes; i++) {
                if (skip_array[i-1]) {
                    continue;
                }
                boolean exists1 = check_if_file_exists(INPUT_FILE + "/protein/results/" + i);
                boolean exists2 = check_if_file_exists(INPUT_FILE + "/protein_longest_transcript/results/" + i);
                if (!exists1) {
                    all_protein_exists = false;
                }    
                if (!exists2) {
                    all_protein_longest_exists = false;
                } 
                if (!exists1 && !exists2) { // file missing for both 
                    missing_genomes.add(i);
                }     
            }
            if (all_protein_exists && all_protein_longest_exists) { // both exists, taking longest_transcript
                busco_path = INPUT_FILE + "/protein_longest_transcript/";
                Pantools.logger.info("There are two possible BUSCO directories in {}. Taking protein_longest_transcript.", INPUT_FILE);
            } else if (!all_protein_exists && all_protein_longest_exists) {
                busco_path = INPUT_FILE + "/protein_longest_transcript/";
            } else if (all_protein_exists && !all_protein_longest_exists) {
                // original busco_path is correct
            } else {
                Pantools.logger.error("Not all selected genomes have BUSCO results in: {}/protein/ or {}/protein_longest_transcript/results/", INPUT_FILE, INPUT_FILE);
                Pantools.logger.error("{} missing genomes: {}", missing_genomes.size(), missing_genomes.toString().replace("[", "").replace("]", ""));
                System.exit(1);
            }
        }
        busco_path = busco_path.replace("//","/"); // the path is later split on /
        return busco_path;
    }
   
    /**
     * Output for BUSCO v3 and v4 is named differently and stored in different location
     * @param busco_path
     * @param genome_nr
     * @return 
     */
    public String retrieve_correct_busco_file(String busco_path, int genome_nr) {
        String[] busco_path_array = busco_path.split("/");
        String db_name = busco_path_array[busco_path_array.length-2];
        String busco4_output = busco_path + "/results/" + genome_nr + "/run_" +  db_name + "/full_table.tsv";
        String busco3_output = busco_path + "/results/" + genome_nr + "/full_table_genome_" + genome_nr + ".tsv";
        String busco_output = busco4_output;
        boolean exists1 = check_if_file_exists(busco4_output);
        boolean exists2 = check_if_file_exists(busco_path + "/results/" + genome_nr + "/full_table_genome_" + genome_nr + ".tsv");
        if (!exists1 && exists2) {
            busco_output = busco3_output;
        }
        if (!exists1 && !exists2) {
            Pantools.logger.error("Neither of these files exist. Please run 'busco_protein' first: {} {}", busco4_output, busco3_output);
            System.exit(1);
        }
        return busco_output;
    }
    
    /**
     * Parse BUSCO v3 and v4 output to retrieve the 'complete' BUSCOs
     * @param busco_path
     * @param busco_log_builder
     * @return 
     */
    public HashMap<String, ArrayList<String>> read_all_busco_output_files(String busco_path, StringBuilder busco_log_builder) {
        ArrayList<Integer> all_busco_presence = new ArrayList<>(); //number of complete BUSCO's per genome
        HashMap<String, ArrayList<String>> busco_mrna_map = new HashMap<>();  // key is busco id, value is list with mrna identifiers
        for (int i = 1; i <= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            String busco_output = retrieve_correct_busco_file(busco_path, i);
            int count = 0; // number of single copy and complete BUSCOs
            try (BufferedReader in = new BufferedReader(new FileReader(busco_output))) {
                for (int c = 0; in.ready();) {
                    String line = in.readLine().trim();
                    if (line.startsWith("#")) {
                        continue;
                    }
                    String[] line_array = line.split("\t"); //[0] is busco id, [2] is the mrna identifier  
                    if (!line_array[1].equals("Complete")) { // only take single copy and complete BUSCOs
                        continue;
                    }
                    count ++;
                    try_incr_AL_hashmap(busco_mrna_map, line_array[0], line_array[2] + "#" + i);
                }
            } catch (IOException ioe) {
                Pantools.logger.error("Failed to read {}", busco_output);
                System.exit(1);
            }
            all_busco_presence.add(count);
        }
        String busco_freq = determine_frequency_list_int(all_busco_presence);
        busco_log_builder.append("Total number of 'Complete' and single copy BUSCO's (occurs in this many genomes): ").append(busco_freq).append("\n"); 
        return busco_mrna_map;
    }
    
    /**
     * Remove BUSCOs not 'complete' in all genomes and print information and add to log 
     * @param busco_mrna_map
     * @param busco_log_builder 
     * @param busco_path
     */
    public void find_buscos_shared_by_all_genomes(HashMap<String, ArrayList<String>> busco_mrna_map, StringBuilder busco_log_builder, String busco_path) {
        int total_buscos = busco_mrna_map.size();
        ArrayList<Integer> genome_presence = new ArrayList<>();
        StringBuilder busco_presence_builder = new StringBuilder();
        ArrayList<String> remove_buscos = new ArrayList<>();
        for (String busco_id : busco_mrna_map.keySet()) {
            ArrayList<String> mrna_identifiers = busco_mrna_map.get(busco_id);
            busco_presence_builder.append(busco_id).append(", ").append(mrna_identifiers.size()).append("\n");
            genome_presence.add(mrna_identifiers.size());
            if (mrna_identifiers.size() != adj_total_genomes) { // only selecting busco's present in all 
                remove_buscos.add(busco_id);
            }
        }
        for (String busco_id : remove_buscos) { // delete BUSCOs not 'complete' in all genomes
            busco_mrna_map.remove(busco_id);
        }
        
        String busco_count_in_genomes = determine_frequency_list_int(genome_presence);
        busco_log_builder.append("Number of genomes in which a complete and single copy BUSCO gene is found (number of BUSCO's): ")
                .append(busco_count_in_genomes.replace("x","")).append("\n\n");
        if (busco_mrna_map.isEmpty()) {
            Pantools.logger.error("No BUSCO gene can be found that is 'Complete' and single copy in in all {} selected genomes, please take a look at: {}busco_overview.csv", adj_total_genomes, busco_path);
            System.exit(1);
        }
        
        String percentage = get_percentage_str(busco_mrna_map.size(), total_buscos, 1);
        String info = "Using " + busco_mrna_map.size() + "/" + total_buscos + " (" + percentage + "%) "
                + "BUSCOs which are 'Complete' and single copy in all " + adj_total_genomes + " genomes"
                + "\nThe number of True Postives (TP) and False Negatives (FN) is: " + (busco_mrna_map.size() * adj_total_genomes);
        if (print_info) {
            System.out.print("\r                                                                \n" + info + "\n");
        }
        
        busco_log_builder.append(info).append("\n\nBUSCO identifier, number of genomes found\n")
                .append(busco_presence_builder.toString()).append("\n")
                .append("##Part 2. Counting True positives (TP), false positives (FP) and false negatives(FN) for all groupings. "
                        + "The highest number of correctly clustered BUSCOs present in one group are TP. "
                        + "Any other gene clustered inside this group is considered a FP."
                        + "The remaining BUSCO genes outside this best group are counted as FN.\n");
    }
    
    /**
     * Finds BUSCO genes that are complete in all genomes. Puts these in mrna_busco_map hashmap where the key is busco id and value is list with mrna identifiers
     * @param busco_log_builder
     * @return 
     */
    public HashMap<String, ArrayList<String>> prepare_busco_hashmap(StringBuilder busco_log_builder) {
        String busco_path = find_busco_input();
        HashMap<String, ArrayList<String>> busco_mrna_map = read_all_busco_output_files(busco_path, busco_log_builder);
        find_buscos_shared_by_all_genomes(busco_mrna_map, busco_log_builder, busco_path);
        return busco_mrna_map;
    }
     
    /**
     * The breaks on the Y-axis will scale to 10 million FP or FN 
     * @param highest_number_in_plot 
     */
    public void create_best_grouping_rscript(int highest_number_in_plot) {
        StringBuilder rscript = new StringBuilder();
        String path = WD_full_path + "optimal_grouping/";
        rscript.append("#install.packages(\"ggplot2\", \"").append("~/local/R_libs/").append("\", \"https://cran.us.r-project.org\")\n")
                .append("library(ggplot2)\n\n")
                .append("df = read.csv(\"").append(path).append("fn_fp.csv\", sep=\",\", header = TRUE)\n")
                .append("plot1 = ggplot(data=df, aes(x=Mode, y=Score, group=Type))\n")
                .append("if (packageVersion(\"ggplot2\") < \"3.4.0\") {\n")
                .append("    plot1 = plot1 + geom_line(aes(color=Type), size = 2) #increase linewidth for ggplot2 version < 3.4.0\n")
                .append("} else {\n")
                .append("    plot1 = plot1 + geom_line(aes(color=Type), linewidth = 2) #increase linewidth for ggplot2 version >= 3.4.0\n")
                .append("}\n")
                .append("plot1 = plot1 +\n");

        String add_str = retrieve_of_output_if_available();
        rscript.append(add_str);
        rscript.append("    scale_color_manual(values=c(\"FP\" = \"#000000\", \"FN\" = \"#990000\")) +\n")
                .append("    theme_bw(base_size = 30) +\n")
                .append("    theme(legend.key.size = unit(1.5, \"cm\")) +\n")
                .append("    scale_y_continuous(\"Proteins\", trans = 'log10', breaks=c(1,10,100,1000,10000,100000,100000,1000000)) +\n")
                .append("    scale_x_continuous(\"Relaxation mode\", breaks = 1:8, limits=c(1, 8))\n\n")
                .append("pdf(NULL)\n")
                .append("ggsave(\"").append(path).append("optimal_grouping.png\", plot= plot1, height = 10, width = 10)\n") 
                .append("cat(\"\\n\\nThe warning messages can safely be ignored.\\n\")\n")
                .append("cat(\"Plot written to: ").append(path).append("optimal_grouping.png\\n\")");
        write_SB_to_file_in_DB(rscript, "optimal_grouping/optimal_grouping.R");
    }
    
    /*
      Add orthofinder FP FN results to plot if these are available
    */
    public String retrieve_of_output_if_available() {
        String orthofinder_results = WD_full_path + "optimal_grouping/orthofinder_benchmark";
        String add_str = "";
        if (check_if_file_exists(orthofinder_results)) {
            try (BufferedReader in = new BufferedReader(new FileReader(orthofinder_results))) { // first check if all files exist
                for (int c = 0; in.ready();) {
                    String line = in.readLine().trim();
                    add_str += "    " + line + "\n";
                }
            } catch (IOException ioe) {
                Pantools.logger.error("Failed to read: {}", orthofinder_results);
                System.exit(1);
            }
            delete_file_full_path(orthofinder_results);
        }
        return add_str;
    }
   
    /**
     * Group will get stuck if .graph or .clusters files from a previous run are still present 
     */
    public void delete_files_from_previous_group_run() {
        File f = new File(WORKING_DIRECTORY);
        String[] filenames = f.list();
        System.out.print("\rRemoving files from previous run");
        for (String pathname : filenames) {
            if (pathname.endsWith(".graph")) { // delete .graph files from previous (crashed) run
                delete_file_in_DB(pathname);
            }
            if (pathname.endsWith(".clusters")) { // delete .clusters files from previous (crashed) run
                delete_file_in_DB(pathname);
            }
        }
        System.out.print("\r                                            "); // spaces are intentiona
    }
    
    /** 
     @return total number of proteins in current genome selection
    */
    public int get_num_proteins_from_individual_genomes() {
        ResourceIterator<Node> annotation_nodes = GRAPH_DB.findNodes(ANNOTATION_LABEL);
        int updated_num_proteins = 0;
        while (annotation_nodes.hasNext()) { 
            Node annotation_node = annotation_nodes.next();
            String identifier = (String) annotation_node.getProperty("identifier");
            String[] id_array = identifier.split("_");
            System.out.print("\rCounting protein sequences: " + id_array[0]);
            if (annotation_identifiers.contains(identifier)) { // the number of proteins for genomes that are skipped are not present
               int prot_count = (int) annotation_node.getProperty("num_proteins");
               updated_num_proteins += prot_count;
               if (prot_count == 0) { // even though the genome is annotated, there are no protein coding genes
                    int genome_nr = Integer.parseInt(id_array[0]);
                    skip_array[genome_nr -1] = true;
                    skip_list.add(genome_nr);
                    adj_total_genomes --;
                }
            }
        }
        System.out.print("\r                                                      "); // spaces are intentional 
        return updated_num_proteins;
    }
    
    public void get_number_of_proteins() {
        if (longest_transcripts) {
            num_proteins = find_longest_transcript_per_gene(true, num_proteins); 
            System.out.println("\rProteins (longest transcripts only) = " + num_proteins);
        } else {
            try (Transaction tx = GRAPH_DB.beginTx()) {
                if (PROTEOME) { 
                    num_proteins = (int) pangenome_node.getProperty("num_proteins"); 
                } else { // pangenome 
                    num_proteins = get_num_proteins_from_individual_genomes();
                }
                System.out.println("\rProteins = " + num_proteins);
                tx.success();
            }
        }
        if (num_proteins == 0) {
            System.out.println("\rNo proteins sequences available. Please run 'add_annotations'\n");
            System.exit(1);
        }
    }
    
    /**
     * Sets 'longest_transcript' property to mRNA, exon, intron and CDS nodes.
     * 
     * @param count_nodes count the total number of longest transcripts
     * @param original_number_of_proteins number of protein
     * @return number of proteins, only counting the longest transcripts
     */
    public int find_longest_transcript_per_gene(boolean count_nodes, int original_number_of_proteins) {
        StringBuilder duplicate_id_builder = new StringBuilder("#mRNA name 1, mRNA name 2, mRNA node id 1, mRNA node id 2\n");
        int protein_counter = 0, duplicated_name_counter = 0;
        if (Mode.contains("LONGEST-TRANSCRIPT")) { 
            // do not remove yet, in an older version the longest transcripts option was enabled via --mode longest-transcripts
            longest_transcripts = true;
        }
        if (!longest_transcripts || PROTEOME) {
            return original_number_of_proteins;
        }
        create_directory_in_DB("proteins");
        create_directory_in_DB("proteins/longest_transcripts");
        for (String anno_id : annotation_identifiers) {
            if (anno_id.endsWith("_0")) { // identifiers with _0 are skipped genomes
                continue;
            }
            boolean file_exists = check_if_file_exists(WORKING_DIRECTORY + "proteins/longest_transcripts/proteins_" + anno_id + ".fasta");
            if (file_exists && !count_nodes) {
                continue; // longest transcripts are already set and counting is not required
            }  
            try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction, commit changes after every genome
                String[] anno_id_array = anno_id.split("_");
                duplicate_id_builder.append("#Genome ").append(anno_id_array[0]).append("\n");
                ResourceIterator<Node> gene_nodes = GRAPH_DB.findNodes(GENE_LABEL, "annotation_id", anno_id);
                int gene_counter = 0; // prot_per_genome = 0, prot_per_genome2 = 0;
                StringBuilder longest_proteins = new StringBuilder();
                while (gene_nodes.hasNext()) {
                    gene_counter ++;
                    if (gene_counter % 1000 == 0 || gene_counter == 1) {
                        System.out.print("\rFinding longest transcript per gene: Genome " + anno_id_array[0] + ", " + gene_counter + "         ");
                    }
                    Node gene_node = gene_nodes.next();
                    Iterable<Relationship> mrna_relations = gene_node.getRelationships(RelTypes.codes_for);
                    Node longest_transcript_node = GRAPH_DB.getNodeById(0); // mRNA node
                    int longest_transcript_length = 0;
                    boolean first = true;
                    for (Relationship mrna_rel : mrna_relations) {
                        Node mrna_node = mrna_rel.getEndNode();
                        if (!mrna_node.hasProperty("protein_ID")) {
                            continue;
                        }
                        if (first) {
                            first = false;
                            //prot_per_genome ++;
                        }
                        String protein = (String) mrna_node.getProperty("protein_sequence");
                        if (protein.length() > longest_transcript_length) { 
                            longest_transcript_length = protein.length();
                            longest_transcript_node = mrna_node;
                        } else if (protein.length() == longest_transcript_length) { // check if the protein ids have id.1, id.2, ... id.10 etc
                            String new_id = (String) mrna_node.getProperty("protein_ID");
                            String prev_id = (String) longest_transcript_node.getProperty("protein_ID");
                            if (new_id.compareTo(prev_id) < 0) { 
                                longest_transcript_node = mrna_node;
                            } else if ((new_id.compareTo(prev_id) == 0)) { // compareTo() returns 0 when equal
                                duplicate_id_builder.append(new_id).append(",").append(prev_id).append(",").append(mrna_node.getId())
                                        .append(",").append(longest_transcript_node.getId()).append("\n");
                                duplicated_name_counter ++;
                                if (mrna_node.getId() < longest_transcript_node.getId()) { 
                                    // select the longest transcript based on the lower database identifier
                                    longest_transcript_node = mrna_node;
                                }
                            }
                        }
                    }
                    
                    if (longest_transcript_length > 0) {
                        if (!longest_transcript_node.hasProperty("longest_transcript")) {
                            longest_transcript_node.setProperty("longest_transcript", "yes");
                        }
                        Iterable<Relationship> relations = longest_transcript_node.getRelationships(RelTypes.is_parent_of);
                        for (Relationship rel : relations) {
                            Node cds_intron_exon_node = rel.getEndNode(); // CDS, intron or exon
                            if (cds_intron_exon_node.hasLabel(EXON_LABEL) || cds_intron_exon_node.hasLabel(CDS_LABEL) || cds_intron_exon_node.hasLabel(INTRON_LABEL)) {
                                if (!cds_intron_exon_node.hasProperty("longest_transcript")) {
                                    cds_intron_exon_node.setProperty("longest_transcript", "yes");
                                }
                            }
                        }   
                        
                        relations = longest_transcript_node.getRelationships(RelTypes.contributes_to);
                        for (Relationship rel : relations) {
                            Node cds_node = rel.getStartNode(); // CDS
                            if (cds_node.hasLabel(CDS_LABEL)) {
                                if (!cds_node.hasProperty("longest_transcript")) {
                                    cds_node.setProperty("longest_transcript", "yes");
                                }
                            }
                        }  
                        
                        protein_counter ++;
                        //prot_per_genome2 ++;
                        String protein_id = (String) longest_transcript_node.getProperty("protein_ID");
                        String protein_sequence = get_protein_sequence(longest_transcript_node);
                        protein_sequence = split_seq_in_parts_of_80bp(protein_sequence);
                        longest_proteins.append(">").append(protein_id).append("\n").append(protein_sequence).append("\n");
                    }
                }
                tx.success(); // transaction successful, commit changes
                write_SB_to_file_in_DB(longest_proteins, "proteins/longest_transcripts/proteins_" + anno_id + ".fasta");
            }
        }
        System.out.print("\r                                                                          ");
        if (duplicated_name_counter > 0) {
            write_SB_to_file_in_DB(duplicate_id_builder, "duplicate_identifiers.log");
            System.out.println("\r" + duplicated_name_counter + " cases were a gene has equally long transcripts with the same identifier"
                    + "\nPlease check " + WORKING_DIRECTORY + "duplicate_identifiers.log\n");
        }
        return protein_counter;
    }
    
    /* 
      When a genome does not have an annotation (or is skipped) the id will end with '_0'. 
    */  
    public void update_skip_array_based_on_anno_ids() {
        if (annotation_identifiers == null) {
            annotation_identifiers = get_annotation_identifiers(true, true, PATH_TO_THE_ANNOTATIONS_FILE);
        }
        for (String anno_id : annotation_identifiers) {
            String[] annno_id_array = anno_id.split("_"); // example 1_1
            int genome_nr = Integer.parseInt(annno_id_array[0]);
            int anno_nr = Integer.parseInt(annno_id_array[1]);
            if (anno_nr == 0 && !skip_array[genome_nr -1]) {
                skip_array[genome_nr -1] = true;
                skip_list.add(genome_nr);
                adj_total_genomes --;
            }
        }
    }

    /**
     * @return the highest grouping version number
     */
    public static int check_highest_grouping_version() {
        Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
        int highest_grouping = 0;
        if (pangenome_node.hasProperty("highest_grouping")) {
            highest_grouping = (int) pangenome_node.getProperty("highest_grouping");
        }
        return highest_grouping;
    }

    /**
     * Groups similar proteins into homology groups.
     */
    public void group(String scoringMatrix) {
        MAX_TRANSACTION_SIZE = 1000;
        progress = "";
        Pantools.logger.info("Clustering protein sequences.");
        check_if_program_exists_stdout("mcl -h", 100, "mcl"); // check if program is set to $PATH
        check_database(); // starts up the graph database if needed
        report_number_of_threads(); // prints how many threads were selected by user
        delete_files_from_previous_group_run();
        
        create_directory_in_DB("group");
        proteins = new LinkedBlockingQueue<>();
        intersections = new LinkedBlockingQueue<>((int)(heapSize/2/100));
        similarities = new LinkedBlockingQueue<>((int)(heapSize/2/100));
        num_intersections = new AtomicInteger(0);
        num_similarities = new AtomicInteger(0);
        num_components = new AtomicInteger(0);
        similarity_bars = new AtomicInteger(0);
       
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes 
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
           Pantools.logger.error("Unable to start the database.");
           System.exit(1);
        } 
        if ((Mode.contains("LONGEST-TRANSCRIPT") || longest_transcripts) && PROTEOME) {
            Mode = Mode.replace("LONGEST-TRANSCRIPT","");
            longest_transcripts = false;
            Pantools.logger.info("--mode longest-transcript is NOT usable on a panproteome. All sequences of a proteome are used.");
        }
        
        remove_component_from_mrna_nodes(pangenome_node); // removes 'component' property that is only used for grouping the proteins. These are a remainder from previous runs 
        try (Transaction tx = GRAPH_DB.beginTx()) {
            stop_if_hmgroups_exist(); // stop when there are active homology groups
            grouping_version = check_highest_grouping_version();
            grouping_version ++;
            total_genomes = (int) pangenome_node.getProperty("num_genomes");
            pangenome_node.removeProperty("last_grouping_successful");
            pangenome_node.setProperty("last_grouping_successful", false);
            pangenome_node.setProperty("components_presents", true);
            create_skip_arrays(false, true);
            num_genomes = total_genomes;  
            update_skip_array_based_on_anno_ids();  
            tx.success();
        }
        get_number_of_proteins();
        Pantools.logger.info("Genomes = {}.", adj_total_genomes);
        if (adj_total_genomes == 0) {
            Pantools.logger.error("No genomes are selected or none of the genomes have an annotation.");
            System.exit(1);
        }
        
        if (THREADS < 3) {
            THREADS = 3;
        }
        MAX_KMER_FREQ = num_proteins / 1000 + 50 * adj_total_genomes; // because of probability p and copy number of 50
        num_hexamers = 0;
       
        try {
            ExecutorService es = Executors.newFixedThreadPool(2);
            es.execute(new Generate_proteins());
            es.shutdown();
            es.awaitTermination(10, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            
        }
       
        first_generate_proteins = false;
        try{
            ExecutorService es = Executors.newFixedThreadPool(2);
            es.execute(new Generate_proteins());
            es.execute(new count_kmers(num_proteins));
            es.shutdown();
            es.awaitTermination(10, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            
        }
        
        Pantools.logger.info("Kmerizing proteins :");
        System.out.println("0 ......................................... 100");
        try{
            ExecutorService es = Executors.newFixedThreadPool(2);
            es.execute(new Generate_proteins());
            es.execute(new Kmerize_proteins(num_proteins));
            es.shutdown();
            es.awaitTermination(10, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            
        }
        Pantools.logger.info("Kmers = {}", num_hexamers);
        Pantools.logger.info("Finding intersections:");
        System.out.println("0 ......................................... 100");
        try{
            ExecutorService es = Executors.newFixedThreadPool(THREADS);
            es.execute(new Generate_proteins());
            es.execute(new Find_intersections(num_proteins));
            for(int i = 1; i <= THREADS - 2; i++) {
                es.execute(new Find_similarities(scoringMatrix));
            }
            es.execute(new Write_similarities());
            es.shutdown();
            es.awaitTermination(10, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            
        }
        while (similarity_bars.intValue() < 41) {
            System.out.print("|"); //TODO: add progress bar
            similarity_bars.getAndIncrement();
        }
        System.out.println();
        Pantools.logger.info("Similarities = {}", num_similarities.intValue());
        
    //Free a considerable space
        for (int i = 0; i < MAX_KMERS_NUM; ++i)
            kmers_proteins_list[i] = null;
        kmers_proteins_list = null;
        kmer_frequencies = null;
        intersections = null;
        similarities = null;
        System.gc();
        components = new LinkedBlockingQueue<>((int)(heapSize/200));
        homology_groups_list = new LinkedBlockingQueue<>((int)(heapSize/200));
        Pantools.logger.trace("num genomes {}.", num_genomes);
        try{
            ExecutorService es = Executors.newFixedThreadPool(2);
            es.execute(new Generate_proteins());
            es.execute(new build_similarity_components(WORKING_DIRECTORY, num_proteins));
            for(int i = 1; i <= THREADS - 2; i++)
                es.execute(new build_homology_groups(WORKING_DIRECTORY, num_proteins));
            es.execute(new write_homology_groups(WORKING_DIRECTORY, num_proteins));
            es.shutdown();
            es.awaitTermination(10, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            
        }
      
        try (Transaction tx = GRAPH_DB.beginTx()) {
            put_grouping_version(true);
            pangenome_node.removeProperty("last_grouping_successful");
            pangenome_node.setProperty("last_grouping_successful", true);
            tx.success();
        }
        copy_file(WORKING_DIRECTORY + "pantools_homology_groups.txt", WORKING_DIRECTORY + "/group/" + MIN_NORMALIZED_SIMILARITY);
        System.out.println();
        Pantools.logger.info("Components = {}", num_components);
        Pantools.logger.info("Groups = {}", num_groups);
        Pantools.logger.info("Database size = {} MB", FileUtils.getFolderSize(new File(WORKING_DIRECTORY + GRAPH_DATABASE_PATH)));
    }
}
package nl.wur.bif.pantools.pangenome.export;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Streams;
import nl.wur.bif.pantools.pangenome.export.records.NodeProperty;
import nl.wur.bif.pantools.pangenome.export.records.Record;
import nl.wur.bif.pantools.pangenome.export.records.RelationshipProperty;
import nl.wur.bif.pantools.pangenome.export.records.SequenceAnchor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.registerShutdownHook;


/**
 * Main class and entry point for exporting a paFsngenome. Will export pangenome
 * contents as three CSV files for comparison purposes.
 *
 */
public class PangenomeExporter {
    // TODO: we're using names because RelTypes.x contains ordinal properties, and relationship types don't
    private static final Set<String> VALID_RELATIONSHIP_TYPE_NAMES = new HashSet<>(Arrays.asList(
        RelTypes.has.name(),
        RelTypes.FF.name(),
        RelTypes.FR.name(),
        RelTypes.RF.name(),
        RelTypes.RR.name()
    ));

    private static final Map<Label, Set<String>> NODE_PROPERTIES_TO_SKIP = ImmutableMap.<Label, Set<String>>builder()
        .put(PANGENOME_LABEL, ImmutableSet.of("date"))
        .put(GENOME_LABEL, ImmutableSet.of("date", "path"))
        .put(SEQUENCE_LABEL, ImmutableSet.of("anchor_sides", "anchor_nodes", "anchor_positions"))
        .put(DEGENERATE_LABEL, ImmutableSet.of("sequence"))
        .put(NUCLEOTIDE_LABEL, ImmutableSet.of("sequence"))
        .build();


    /**
     * Create a new pangenome exporter, exporting a database to files of three types:
     * <p>
     * 1. Node properties;
     * 2. Relationship properties;
     * 3. Sequence anchors.
     * </p>
     * @param nodePropertiesOutputPath path to node properties output file.
     * @param relationshipPropertiesOutputPath path to relationship properties output file.
     * @param sequenceAnchorsOutputPath path to sequence anchors output file.
     * @throws IOException in case of Neo4j or output file IO error.
     */
    public PangenomeExporter(
        Path nodePropertiesOutputPath,
        Path relationshipPropertiesOutputPath,
        Path sequenceAnchorsOutputPath) throws IOException {

        exportNodeProperties(nodePropertiesOutputPath);
        exportRelationshipProperties(relationshipPropertiesOutputPath);
        exportSequenceAnchors(sequenceAnchorsOutputPath);
    }

    private void exportRelationshipProperties(Path outputFile) throws IOException {
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Relationship> relationships = GRAPH_DB.getAllRelationships().iterator();
             CSVPrinter printer = getCSVPrinter(outputFile)) {

            final Stream<RelationshipProperty> anchors = relationships
                .stream()
                .flatMap(relationship -> getRelationshipPropertyRecords(GRAPH_DB, relationship));

            printRecords(printer, anchors);
        }
    }

    private void exportNodeProperties(Path outputFile) throws IOException {
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> nodes = GRAPH_DB.getAllNodes().iterator();
             CSVPrinter printer = getCSVPrinter(outputFile)) {

            final Stream<NodeProperty> properties = nodes
                .stream()
                .flatMap(this::getNodePropertyRecords);

            printRecords(printer, properties);
        }
    }

    /**
     * Return stream of relationship property records. Might be empty if the relationship should not be included in the
     * property dump, for example when relationship is not of the proper type, or has start and end nodes that are not
     * part of the pangenome as such (i.e. not a pangenome, genome, sequence or nucleotide/degenerate node).
     * @param r relationship to create property records for.
     * @return stream of relationship property records. Might be emtpy.
     */
    private Stream<RelationshipProperty> getRelationshipPropertyRecords(GraphDatabaseService GRAPH_DB, Relationship r) {
        // TODO: we're retrieving the start and end node labels twice, as well as the relationship's type
        // TODO: refactor to method returning optional<relationship type, start node label, end node label>?

        // Verify we should include the relationship by checking it's type, and start and end node labels
        if (!isValidRelationshipType(r.getType()))
            return Stream.empty();

        final Node startNode = GRAPH_DB.getNodeById(r.getStartNodeId());
        final Optional<NodeUUIDAndLabel> startNodeUUIDAndLabel = getNodeUuidAndLabel(startNode);
        if (!startNodeUUIDAndLabel.isPresent())
            return Stream.empty();

        final Node endNode = GRAPH_DB.getNodeById(r.getEndNodeId());
        final Optional<NodeUUIDAndLabel> endNodeUUIDAndLabel = getNodeUuidAndLabel(endNode);
        if (!endNodeUUIDAndLabel.isPresent())
            return Stream.empty();

        // Generate record for each property

        final Map<String, Object> properties = r.getAllProperties();
        if (properties.isEmpty()) {
            // Some relationships do not have any properties, such as those going from pangenome to genome nodes,
            // or genome to sequence nodes. In these cases, the property key and value are null.

            // TODO: combine start and end node optionals instead of using get()

            return Stream.of(
                new RelationshipProperty(
                    r.getType().name(),
                    startNodeUUIDAndLabel.get().getLabel().name(),
                    startNodeUUIDAndLabel.get().getUuid(),
                    endNodeUUIDAndLabel.get().getLabel().name(),
                    endNodeUUIDAndLabel.get().getUuid(),
                    "",
                    ""
                )
            );
        }

        // TODO: refactor duplicate code
        return properties
            .entrySet()
            .stream()
            .map(entity -> new RelationshipProperty(
                r.getType().name(),
                startNodeUUIDAndLabel.get().getLabel().name(),
                startNodeUUIDAndLabel.get().getUuid(),
                endNodeUUIDAndLabel.get().getLabel().name(),
                endNodeUUIDAndLabel.get().getUuid(),
                entity.getKey(),
                formatPropertyValue(entity.getValue())
            ));
    }

    /**
     * Return a stream of node property records for writing. For each node, we first retrieve
     * the node's UUID and label, as an optional. If the optional is empty we return an empty list. This will happen
     * when the node should not be included in the node properties dump, as determined by its label set. If the optional
     * is non-empty, we iterate over the properties of the node, excluding those that are not relevant, and generate
     * a record for each.
     * @param node node to generate property records for.
     * @return stream with property records, or empty stream if node should not be included in the dump.
     */
    private Stream<NodeProperty> getNodePropertyRecords(Node node) {
        return getNodeUuidAndLabel(node)
            .map(uuidAndLabel -> skipNodeProperties(node.getAllProperties(), uuidAndLabel.getLabel())
                .entrySet()
                .stream()
                .map(entry -> new NodeProperty(
                    uuidAndLabel.getUuid(),
                    uuidAndLabel.getLabel().name(),
                    entry.getKey(),
                    formatPropertyValue(entry.getValue())
                ))
            )
            .orElse(Stream.empty());
    }

    /**
     * Return a property map, skipping node properties that are not relevant or are used for generating UUIDs or labels.
     *
     * @param properties property map from node.
     * @param label most significant node label.
     * @return map excluding node properties.
     */
    private Map<String, Object> skipNodeProperties(Map<String, Object> properties, Label label) {
        final Set<String> propertiesToSkip = NODE_PROPERTIES_TO_SKIP.get(label);
        if (propertiesToSkip == null)
            throw new IllegalArgumentException("cannot find node properties to skip for label " + label);

        return properties
            .entrySet()
            .stream()
            .filter(entry -> !propertiesToSkip.contains(entry.getKey()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }



    /**
     * Indicates whether the relationship's type is valid, meaning whether it is eligible for inclusion in the
     * relationship property dump. Other checks are those on the node labels in {@link #getRelationshipPropertyRecords}.
     * @param type the type of relationship.
     * @return true if the relationship might be included in the relationship property dump, false if not.
     * // TODO: different name would be clearer, but this is not the only criterion, see getRelationshipPropertyRecords
     */
    private boolean isValidRelationshipType(RelationshipType type) {
        return VALID_RELATIONSHIP_TYPE_NAMES.contains(type.name());
    }

    /**
     * Return the node's UUID and label. The node's label is the most signicant label from the node's label set. Only
     * pangenome, genome, sequence, nucleotide and degenerate nodes are processed, other types of nodes are returned
     * as an empty optional. The node's UUID is dependent on the label.
     * This method expects to be run inside a transaction to retrieve node labels and properties.
     * {@link IllegalArgumentException} if the label set is empty.
     * @param node node to return UUID and label for.
     * @return empty if node does not have relevant labels, optional if it does.
     */
    private Optional<NodeUUIDAndLabel> getNodeUuidAndLabel(Node node) {
        final Set<Label> labels = getNodeLabelsAsSet(node);
        if (labels.isEmpty())
            throw new IllegalArgumentException("no labels for node " + node);

        if (labels.contains(PANGENOME_LABEL))
            return Optional.of(new NodeUUIDAndLabel(PANGENOME_LABEL.name(), PANGENOME_LABEL));

        if (labels.contains(GENOME_LABEL)) {
            // Strip directories from path to genome files - only filenames need to match
            final String filename = Paths.get(node.getProperty("path").toString()).getFileName().toString();
            return Optional.of(new NodeUUIDAndLabel(filename, GENOME_LABEL));
        }

        if (labels.contains(SEQUENCE_LABEL))
            return Optional.of(new NodeUUIDAndLabel(node.getProperty("identifier").toString(), SEQUENCE_LABEL));

        // TODO: checking for degenerate label should come before nucleotide label, but order should not be significant
        if (labels.contains(DEGENERATE_LABEL))
            return Optional.of(new NodeUUIDAndLabel(node.getProperty("sequence").toString(), DEGENERATE_LABEL));

        if (labels.contains(NUCLEOTIDE_LABEL))
            return Optional.of(new NodeUUIDAndLabel(node.getProperty("sequence").toString(), NUCLEOTIDE_LABEL));

        return Optional.empty();
    }

    /**
     * Convenience function to turn Neo4j's Label iterable into a set, which makes downstream functionality easier
     * to read.
     * This method expects to be run inside a transaction to retrieve node labels and properties.
     * @param node node to collect labels for.
     * @return set of labels.
     */
    private Set<Label> getNodeLabelsAsSet(Node node) {
        return Streams.stream(node.getLabels()).collect(Collectors.toSet());
    }

    /**
     * Format value of property of a string. Returns null if object is null, will convert integer and long arrays to
     * strings, with elements of long arrays suffixed by an 'L' character (e.g. '1L,2L'), otherwise calls toString().
     * @param object value.
     * @return string representation of property value.
     */
    private String formatPropertyValue(Object object) {
        if (object == null)
            return null;

        if (object instanceof int[])
            return Arrays
                .stream(((int[]) object))
                .mapToObj(Integer::toString)
                .collect(Collectors.joining(","));
        else if (object instanceof long[])
            return Arrays
                .stream(((long[]) object))
                .mapToObj(l -> l + "L")
                .collect(Collectors.joining(","));

        return object.toString();
    }

    /**
     * Export sequence nodes' anchors to a file.
     */
    private void exportSequenceAnchors(Path outputFile) throws IOException {
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> nodes = GRAPH_DB.findNodes(SEQUENCE_LABEL);
             CSVPrinter printer = getCSVPrinter(outputFile)) {

           final Stream<SequenceAnchor> anchors = nodes
                .stream()
                .flatMap(node -> getSequenceAnchorRecords(GRAPH_DB, node));

           printRecords(printer, anchors);
        }
    }

    /**
     * Print all records in a stream to a CSVPrinter.
     * @param printer CSVPrinter to print to.
     * @param records stream of records.
     * @throws IOException in case of write error.
     */
    private void printRecords(CSVPrinter printer, Stream<? extends Record> records) throws IOException {
        final Iterator<? extends Record> iterator = records.iterator();
        while (iterator.hasNext()) {
            final Record record = iterator.next();
            printer.printRecord(record.asList());
        }
    }

    /**
     * Return a CSV printer to the provided path.
     * @param path file to write to.
     */
    private CSVPrinter getCSVPrinter(Path path) throws IOException {
        return new CSVPrinter(Files.newBufferedWriter(path), CSVFormat.DEFAULT);
    }

    /**
     * Get sequence anchor records for a sequence node, an anchor being a combination of:
     * <p>
     * 1. A nucleotide node ID;
     * 2. The base pair position;
     * 3. The nucleotide node's side.
     * </p>
     * @param node sequence node to generate records for.
     * @return stream of sequence anchor records.
     */
    private Stream<SequenceAnchor> getSequenceAnchorRecords(GraphDatabaseService GRAPH_DB, Node node) {
        final Map<String, Object> properties = node.getAllProperties();

        // TODO: a stream implementation would be nice, but zipping three streams into one is messy and unreadable
        // TODO: use NodeUUIDAndLabel for node's UUID, instead of sequence directly

        final int[] positions = (int[]) properties.get("anchor_positions");
        final long[] nodeIds = (long[]) properties.get("anchor_nodes");
        final String sides = (String) properties.get("anchor_sides");

        if ((positions.length != nodeIds.length) || (nodeIds.length != sides.length()))
            throw new IllegalArgumentException("positions, nodes and sides not of equal length for node " + node);

        final String identifier = (String) properties.get("identifier");

        return IntStream
            .range(0, positions.length)
            .mapToObj(i -> new SequenceAnchor(
                identifier,
                i,
                positions[i],
                getNucleotideNodeSequence(GRAPH_DB, nodeIds[i]),
                sides.substring(i, i + 1)
            ));
    }

    /**
     * Return a nucleotide node's sequence. Does not check whether the
     * referenced node is actually a nucleotide node.
     * @param GRAPH_DB graph database service.
     * @param nodeId ID of the nucleotide node.
     * @return the node's sequence.
     */
    private String getNucleotideNodeSequence(GraphDatabaseService GRAPH_DB, long nodeId) {
        return (String) GRAPH_DB.getNodeById(nodeId).getProperty("sequence");
    }


    /**
     * Return a new graph database service for the Neo4j database at the
     * provided path. A shutdown hook is configured to make sure the database
     * is always shut down correctly.
     * @param neo4jDatabasePath path to Neo4j database.
     * @return graph database service.
     */
    private GraphDatabaseService createGraphDatabaseService(Path neo4jDatabasePath) {
        final GraphDatabaseService GRAPH_DB = new GraphDatabaseFactory()
            .newEmbeddedDatabaseBuilder(neo4jDatabasePath.toFile())
            .setConfig(GraphDatabaseSettings.keep_logical_logs, "4 files")
            .newGraphDatabase();

        registerShutdownHook(GRAPH_DB);
        return GRAPH_DB;
    }
}

package nl.wur.bif.pantools.pangenome.parallel;

import com.esotericsoftware.kryo.kryo5.Kryo;
import com.esotericsoftware.kryo.kryo5.io.Output;
import com.github.benmanes.caffeine.cache.Cache;
import nl.wur.bif.pantools.sequence.SequenceScanner;
import org.apache.logging.log4j.Logger;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.pangenome.parallel.KryoUtils.getKryo;
import static nl.wur.bif.pantools.utils.Globals.*;

public class NodeLocalizationTask implements Callable<Path> {
    private final Node sequenceNode;
    private final Path outputDirectory;
    private final Cache<Long, NodeProperties> cache;
    private final Logger log;
    private final SequenceScanner genomeSc;

    /**
     * Localize a sequence node. This method will follow the sequence node to its sequence and degenerate nodes, and
     * write localization information to the specified output file. In addition, this method will write anchor
     * information (node IDs, node sides and anchor sides) to the sequence node.
     *
     * @param sequenceNode    sequence node to localize.
     * @param outputDirectory to write to.
     * @param cache           cache with node properties to speed up localization.
     * @param log             logger.
     */
    public NodeLocalizationTask(Node sequenceNode, Path outputDirectory, Cache<Long, NodeProperties> cache, Logger log) {
        this.sequenceNode = sequenceNode;
        this.outputDirectory = outputDirectory;
        this.cache = cache;
        this.log = log;
        this.genomeSc = new SequenceScanner(GENOME_DB, 1, 1, K_SIZE, INDEX_SC.get_pre_len());
    }

    /**
     * Localize a sequence node. This method will follow the sequence node to its sequence and degenerate nodes, and
     * write localization information to an output file whose path will be returned by this function. In addition, this
     * method will write anchor information (node IDs, node sides and anchor sides) to the sequence node at the end of
     * the localization process.
     *
     * @return path to the output file with @see nl.wur.bif.pantools.pangenome.parallel.Localization objects.
     */
    @Override
    public Path call() {
        // Adapted from GenomeLayer::localize_nodes()

        final Kryo kryo = getKryo();
        int neighbor_length = 0;
        char node_side, neighbor_side;
        long length, distance;
        Node node, neighbor;
        String rel_name, origin;
        int[] address = new int[3], addr = null;
        boolean found = true;
        final Anchors anchors = new Anchors();

        try (Transaction ignored = GRAPH_DB.beginTx()) {
            origin = "G" + ((String) sequenceNode.getProperty("identifier")).replace('_', 'S');
        }

        final Path path = outputDirectory.resolve("localizations-" + origin + ".kryo.xz");

        try (Transaction tx = GRAPH_DB.beginTx(); Output output = KryoUtils.createOutput(path)) {
            address[0] = (int) sequenceNode.getProperty("genome");
            address[1] = (int) sequenceNode.getProperty("number");
            length = (long) sequenceNode.getProperty("length");

            log.debug(String.format("Localizing node %s of length %,d (ID: %d)", origin, length, sequenceNode.getId()));
            log.debug("Writing to output file " + path);

            if (length >= K_SIZE) {
                node = sequenceNode;
                node_side = 'F';
                distance = 0;

                long previousTimestamp = System.currentTimeMillis();
                for (address[2] = 0; address[2] + K_SIZE - 1 < length && found; ) { // K-1 bases of the last node not added
                    found = false;

                    for (Relationship r : node.getRelationships(Direction.OUTGOING)) {
                        rel_name = r.getType().name();

                        if (rel_name.charAt(0) != node_side)
                            continue;

                        neighbor = r.getEndNode();

                        neighbor_side = rel_name.charAt(1);
                        final boolean is_nucleotide = neighbor.hasLabel(NUCLEOTIDE_LABEL);
                        final boolean is_degenerate = neighbor.hasLabel(DEGENERATE_LABEL);
                        final boolean is_node = is_nucleotide && !is_degenerate;

                        if (is_node || is_degenerate) {
                            final Node finalNeighbor = neighbor;
                            NodeProperties nodeProperties = cache.get(
                                neighbor.getId(),
                                k -> new NodeProperties(
                                    (int[]) finalNeighbor.getProperty("address"),
                                    (int) finalNeighbor.getProperty("length")
                                )
                            );

                            assert nodeProperties != null;
                            addr = nodeProperties.getAddress();
                            neighbor_length = nodeProperties.getLength();
                        }

                        if ((is_node && genomeSc.compare(address, addr, K_SIZE - 1,
                            neighbor_side == 'F' ? K_SIZE - 1 : neighbor_length - K_SIZE, 1, neighbor_side == 'F'))
                            || (is_degenerate && Arrays.equals(addr, address))) {
                            found = true;

                            // Write localization

                            kryo.writeObject(output, new Localization(
                                r.getId(),
                                address[0],
                                address[1],
                                address[2],
                                r.getEndNodeId()
                            ));

                            // Update node anchors

                            if (address[2] >= distance) {
                                anchors.add(neighbor.getId(), address[2], neighbor_side);
                                distance += ANCHORS_DISTANCE;
                            }

                            // Display progress every once in a while

                            final long currentTimestamp = System.currentTimeMillis();
                            if ((currentTimestamp - previousTimestamp) > 10000) {
                                final String progress = String.format("%.1f", (100 * (float) address[2]) / ((float) length));
                                log.info(String.format("At position %,d/%,d (%s%%) for node %s", address[2], length, progress, origin));

                                previousTimestamp = currentTimestamp;
                            }

                            address[2] = address[2] + neighbor_length - K_SIZE + 1;
                            node = neighbor;
                            node_side = neighbor_side;
                            break;
                        }
                    }
                }
                if (!found)
                    throw new RuntimeException("cannot locate position " + address[2] + " at node with ID " + node.getId());

                // Store anchors as node property

                log.info(String.format("Updating node %s with %,d anchors", origin, anchors.size()));
                sequenceNode.setProperty("anchor_nodes", anchors.getNodeIds());
                sequenceNode.setProperty("anchor_positions", anchors.getPositions());
                sequenceNode.setProperty("anchor_sides", anchors.getSides());
            }
            tx.success();
        } catch (IOException e) {
            // Thrown by KryoUtils.createOutput
            throw new RuntimeException("cannot create output file at path " + path + ": " + e);
        }

        return path;
    }
}

package nl.wur.bif.pantools.pangenome.export;

import org.neo4j.graphdb.Label;

/**
 * Convenience class for storing a node's UUID and label. The reason we're packing both fields into one class is
 * for reasons of optimization: in order to generate both fields we need to look at both a node's type and its
 * properties.
 */
public class NodeUUIDAndLabel {
    private final String uuid;
    private final Label label;

    public NodeUUIDAndLabel(String uuid, Label label) {
        this.uuid = uuid;
        this.label = label;
    }

    public String getUuid() {
        return uuid;
    }

    public Label getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return "NodeUUIDAndLabel{" +
            "uuid='" + uuid + '\'' +
            ", label=" + label +
            '}';
    }
}

package nl.wur.bif.pantools.pangenome;

import nl.wur.bif.pantools.pantools.Pantools;
import org.neo4j.graphdb.*;

import java.util.*;

import static nl.wur.bif.pantools.utils.Globals.*;

/**
 * This class is used to store and add co-features correctly to the pangenome database. This is
 * part of the pangenome annotation process (see {@link AnnotationLayer}).
 * @author Dirk-Jan van Workum, Bioinformatics group, Wageningen University, the Netherlands.
 */
public class CoFeatures {
    private final int genomeNr;
    private final GraphDatabaseService graphDb;
    private HashSet<Node> coFeatures;

    public CoFeatures(int genomeNr, GraphDatabaseService graphDb) {
        this.genomeNr = genomeNr;
        this.graphDb = graphDb;
        this.coFeatures = new HashSet<>();
    }

    /**
     * Add a co-feature to the set of co-features.
     * @param coFeatureNode The node of the co-feature to add.
     */
    public void add(Node coFeatureNode) {
        coFeatures.add(coFeatureNode);
    }

    /**
     * Remove a co-feature from the set of co-features.
     * @param coFeatureNode The node of the co-feature to remove.
     */
    public void remove(Node coFeatureNode) {
        coFeatures.remove(coFeatureNode);
    }

    /**
     * Get the set of co-features.
     */
    public HashSet<Node> getCoFeatures() {
        return coFeatures;
    }

    /**
     * Remove all co-features from the set of co-features.
     */
    public void clear() {
        coFeatures.clear();
    }

    /**
     * Return the size of the set of co-features.
     */
    public int size() {
        return coFeatures.size();
    }

    /**
     * Handle co-features that are gene nodes. This is done by creating a new gene node with the same ID as the other
     * co-features and connecting the children of the other co-features to the new gene node in a non-redundant way.
     * The other co-features are then deleted and the address of the new gene node is updated to the address of the
     * original co-features. Also, the address of the mRNA node is updated to the address of the original co-features.
     * NB: If there is more than one mRNA node connected to one of the original co-features, this function will not work.
     * NB: This function will add a property "parts" to the new gene node with all the addresses of the original
     * co-features.
     */
    public void addCoFeaturesToPangenome() {
        Pantools.logger.debug("Handling {} co-feature ({}).", coFeatures.size(), coFeatures);

        //convert hashset to iterator and empty the hashset
        Iterator<Node> coFeaturesIterator = coFeatures.iterator();
        coFeatures = new HashSet<>();

        //find names of all unique co-features (based on their ID)
        final HashMap<String, HashSet<Node>> uniqueCoFeatures = new HashMap<>();
        try { // start database transaction
            Transaction tx = graphDb.beginTx();

            while (coFeaturesIterator.hasNext()) {
                Node coFeature = coFeaturesIterator.next();
                String id = (String) coFeature.getProperty("id");

                Pantools.logger.debug("Co-feature {} ({}) at {}.",
                        id, coFeature.getProperty("type"), Arrays.toString((int[]) coFeature.getProperty("address")));

                // add co-feature to unique co-features
                uniqueCoFeatures.putIfAbsent(id, new HashSet<>());
                uniqueCoFeatures.get(id).add(coFeature);

                // remove co-feature from coFeatures
                coFeatures.remove(coFeature);
            }

            tx.success();
            tx.close();
        } catch (NotFoundException nfe) {
            Pantools.logger.info("Error in database transaction.");
            nfe.printStackTrace();
            System.exit(1);
        }

        //loop over all unique co-features
        for (Map.Entry<String, HashSet<Node>> entry : uniqueCoFeatures.entrySet()) {
            handleOneCoFeature(entry.getKey(), genomeNr, entry.getValue());
        }
    }

    /**
     * Handle one co-feature that is a gene node. This function is called by handleCoFeatures.
     * @param coFeatureId ID of the co-feature
     * @param genomeNr genome number of feature
     * @param coFeatures list of co-feature nodes belonging to the given ID
     */
    private void handleOneCoFeature(String coFeatureId, int genomeNr, HashSet<Node> coFeatures) {
        Pantools.logger.debug("Handling co-feature {}.", coFeatureId);

        //continue if there are less than two co-features with the same ID
        if (coFeatures.size() < 2) {
            Pantools.logger.debug("Less than two co-features with ID {}, skipping.", coFeatureId);
            return;
        }

        ArrayList<int[]> addresses = new ArrayList<>();

        try { // start database transaction
            Transaction tx = graphDb.beginTx();

            //find all addresses of the co-features
            for (Node coFeature : coFeatures) {
                int[] address = (int[]) coFeature.getProperty("address");
                addresses.add(address);
            }

            tx.success();
            tx.close();
        } catch (NotFoundException nfe) {
            Pantools.logger.info("Error in database transaction.");
            nfe.printStackTrace();
            System.exit(1);
        }

        int[] newAddress = findNewAddress(addresses);

        //loop over all co-features
        boolean first = true; //first co-feature is the one that will be kept, since we assume all co-feature genes are the same
        for (Node coFeature : coFeatures) {
            if (first) {

                try { // start database transaction
                    Transaction tx = graphDb.beginTx();

                    //update address of first co-feature
                    coFeature.setProperty("address", newAddress);
                    Pantools.logger.debug("Updated address of {} to {}.", coFeature.getProperty("id"), Arrays.toString(newAddress));

                    //add property to co-feature node for easy access to all co-features
                    coFeature.addLabel(COFEATURE_LABEL);

                    //add property to co-feature node for easy access to all co-features
                    coFeature.addLabel(COFEATURE_LABEL);

                    //loop over all children of first co-feature
                    Node firstMrnaNode = null;
                    for (Relationship rel : coFeature.getRelationships(RelTypes.is_parent_of)) {
                        Node child = rel.getEndNode();
                        Pantools.logger.debug("Child of {}: {} ({}).", coFeature.getProperty("id"), child.getProperty("id"), child.getId());

                        // skip if the child appears not to be an mRNA node
                        if (!child.hasLabel(MRNA_LABEL)) continue;

                        if (firstMrnaNode == null) {
                            //update address of mRNA node
                            int[] newMrnaAddress = new int[]{genomeNr, newAddress[1], newAddress[2], newAddress[3]};
                            child.setProperty("address", newMrnaAddress);
                            Pantools.logger.debug("Updated address of {} to {}.", child.getProperty("id"), Arrays.toString(newMrnaAddress));

                            firstMrnaNode = child;
                        } else {
                            for (Relationship childRel : child.getRelationships(RelTypes.is_parent_of)) {
                                //connect children of mRNA node to first mRNA node
                                Pantools.logger.debug("Connecting {} to {}.", childRel.getEndNode().getProperty("id"), firstMrnaNode.getProperty("id"));

                                Node grandChild = childRel.getEndNode();

                                //both CDS and exon nodes are connected to the mRNA node via the is_parent_of relationship
                                firstMrnaNode.createRelationshipTo(grandChild, RelTypes.is_parent_of);

                                //only CDS is connected to the mRNA node via the contributes_to relationship
                                if (grandChild.hasLabel(CDS_LABEL)) {
                                    grandChild.createRelationshipTo(firstMrnaNode, RelTypes.contributes_to);
                                }
                            }

                            //delete mRNA node if it is not the first mRNA node (first delete all relationships)
                            Pantools.logger.debug("Deleting {} ({}).", child.getProperty("id"), child.getId());
                            for (Relationship childRel : child.getRelationships()) {
                                childRel.delete();
                            }
                            child.delete();
                        }

                    }

                    //set first to false
                    first = false;

                    //commit transaction
                    tx.success();
                    tx.close();

                } catch (NotFoundException nfe) {
                    Pantools.logger.info("Error in database transaction.");
                    nfe.printStackTrace();
                    System.exit(1);
                }
            } else {
                deleteCoFeatureNode(coFeature);
            }
        }
    }

    /**
     * Delete a co-feature node including all its relationships and children.
     * @param coFeature co-feature node to be deleted
     */
    private void deleteCoFeatureNode(Node coFeature) {
        try { // start database transaction
            Transaction tx = graphDb.beginTx();

            Pantools.logger.debug("Deleting {} ({}) with all its children.", coFeature.getProperty("id"), coFeature.getId());

            //co-features are connected to mRNA nodes with is_parent_of and codes_for relationships, mRNA nodes are connected to CDS nodes with is_parent_of and contributes_to relationships, and mRNA nodes are connected to exon nodes with is_parent_of relationships
            if (coFeature.hasRelationship(RelTypes.is_parent_of, Direction.OUTGOING)) {
                for (Relationship rel : coFeature.getRelationships(RelTypes.is_parent_of, Direction.OUTGOING)) {
                    Node mrnaNode = rel.getEndNode();
                    Pantools.logger.debug("Deleting mRNA {} ({}) with all its children.", mrnaNode.getProperty("id"), mrnaNode.getId());

                    // delete all relationships of mRNA node and gather all child nodes
                    HashSet<Node> childNodes = new HashSet<>();
                    for (Relationship mrnaRel : mrnaNode.getRelationships(RelTypes.is_parent_of, Direction.OUTGOING)) {
                        Pantools.logger.debug("Marking end node of relationship {} for deletion.", mrnaRel.toString());
                        childNodes.add(mrnaRel.getEndNode());
                    }

                    // delete all child nodes
                    for (Node childNode : childNodes) {
                        Pantools.logger.debug("Deleting child {} ({}).", childNode.getProperty("id"), childNode.getId());
                        for (Relationship childRel : childNode.getRelationships()) {
                            childRel.delete();
                        }
                        for (Relationship childRel : childNode.getRelationships()) {
                            childRel.delete();
                        }
                        childNode.delete();
                    }

                    // delete mRNA node
                    for (Relationship mrnaRel : mrnaNode.getRelationships()) {
                        mrnaRel.delete();
                    }
                    mrnaNode.delete();
                }
            } else {
                Pantools.logger.debug("Co-feature {} ({}) has no children.", coFeature.getProperty("id"), coFeature.getId());
            }

            // delete the co-feature node
            for (Relationship rel : coFeature.getRelationships()) {
                rel.delete();
            }
            coFeature.delete();

            tx.success();
            tx.close();
        } catch (NotFoundException nfe) {
            Pantools.logger.info("Error in database transaction.");
            nfe.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Find the address of the new gene node (the lowest start and the highest stop of all co-features in addresses)
     * @param addresses the addresses of the co-features
     * @return the address of the new gene node
     */
    private int[] findNewAddress(ArrayList<int[]> addresses) {
        int[] newAddress = new int[4];
        newAddress[0] = addresses.get(0)[0];
        newAddress[1] = addresses.get(0)[1];
        newAddress[2] = Integer.MAX_VALUE;
        newAddress[3] = Integer.MIN_VALUE;
        for (int[] address : addresses) {
            if (address[2] < newAddress[2]) {
                newAddress[2] = address[2];
            }
            if (address[3] > newAddress[3]) {
                newAddress[3] = address[3];
            }
        }
        return newAddress;
    }

    /**
     * toString method
     */
    @Override
    public String toString() {
        return "CoFeatures{" +
                "graphDb=" + graphDb +
                ", genomeNr=" + genomeNr +
                ", co-features=" + coFeatures +
                "}";
    }
}

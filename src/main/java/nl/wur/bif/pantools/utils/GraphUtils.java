package nl.wur.bif.pantools.utils;

import nl.wur.bif.pantools.index.IndexDatabase;
import nl.wur.bif.pantools.pantools.Pantools;
import nl.wur.bif.pantools.sequence.SequenceDatabase;
import org.codehaus.plexus.util.StringUtils;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.unsafe.impl.batchimport.input.DataException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.NoSuchElementException;

import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.connect_pangenome;

/**
 * Create, load and validate neo4j graph databases
 *
 * @author Robin van Esch
 */
public final class GraphUtils {

    private GraphUtils() {}

    /**
     * Create a new graph database service for the Neo4j database.
     * A shutdown hook is configured to make sure the database is always shut down correctly.
     *
     * @param databaseDirectory path to the pangenome database directory
     */
    public static void createGraphDatabaseService(Path databaseDirectory) {
        final Path graphDatabasePath = databaseDirectory.resolve("databases").resolve("graph.db");
        GRAPH_DB = new GraphDatabaseFactory()
                .newEmbeddedDatabaseBuilder(graphDatabasePath.toFile())
                .setConfig(GraphDatabaseSettings.keep_logical_logs, "4 files")
                .newGraphDatabase();

        Runtime.getRuntime().addShutdownHook(new Thread(GRAPH_DB::shutdown));
    }

    /**
     * Verify if the database contains a pangenome or a panproteome and set type-specific parameters.
     */
    public static void setDatabaseParameters() {
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> pangenomeNodes = GRAPH_DB.findNodes(PANGENOME_LABEL)) {
            final Node pangenomeNode = pangenomeNodes.next();
            total_genomes = (int) pangenomeNode.getProperty("num_genomes");
            if (pangenomeNode.hasProperty("k_mer_size")) {
                K_SIZE = (int) pangenomeNode.getProperty("k_mer_size");
                connect_pangenome();
            } else {
                PROTEOME = true;
            }
            if (pangenomeNodes.hasNext()) {
                throw new DataException("Database contains more than one pangenome and can only contain one");
            }
        } catch (NotFoundException nfe) {
            throw new NotFoundException("Unable to find the pangenome node in the graph database");
        }
    }

    /**
     * Creates and connects to genome, index and graph databases of the pangenome.
     *
     * @param databaseDirectory path to the pangenome database directory
     * @throws IOException when directories cannot be created in the database directory
     */
    public static void createPangenomeDatabase(Path databaseDirectory) throws IOException {
        createGraphDatabaseService(databaseDirectory);
        GENOME_DB = new SequenceDatabase(WORKING_DIRECTORY + GENOME_DATABASE_PATH, PATH_TO_THE_GENOMES_FILE);
        INDEX_DB = new IndexDatabase(WORKING_DIRECTORY + INDEX_DATABASE_PATH, PATH_TO_THE_GENOMES_FILE, GENOME_DB, K_SIZE);
        Files.createDirectories(databaseDirectory.resolve("databases").resolve("genome.db").resolve("genomes"));
        Files.createDirectory(databaseDirectory.resolve("log")); //TODO: remove
    }

    /**
     * Creates and connects to graph databases of the panproteome.
     *
     * @param databaseDirectory path to the pangenome database directory
     * @throws IOException when directories cannot be created in the database directory
     */
    public static void createPanproteomeDatabase(Path databaseDirectory) throws IOException {
        createGraphDatabaseService(databaseDirectory);
        Files.createDirectory(databaseDirectory.resolve("proteins"));
        Files.createDirectory(databaseDirectory.resolve("log")); //TODO: remove
    }

    /**
     * Validate that the database is a pangenome or a panproteome if one is required.
     *
     * @param type database type; panproteome or pangenome
     */
    public static void validateGraphType(String type) {
        final String databaseType;
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> pangenomeNodes = GRAPH_DB.findNodes(PANGENOME_LABEL)) {
            final Node pangenomeNode = pangenomeNodes.next();
            if (pangenomeNode.hasProperty("k_mer_size")) {
                databaseType = "pangenome";
            } else {
                databaseType = "panproteome";
            }
        }
        if (!databaseType.equals(type)) {
            throw new DataException(
                    String.format("%s database detected; this function is for %ss only",
                            StringUtils.capitalise(databaseType), type));
        }
    }

    /**
     * Check if the pangenome has genome nodes
     */
    public static void validateGenomes() {
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> genomeNodes = GRAPH_DB.findNodes(GENOME_LABEL)) {
            if (!genomeNodes.hasNext()) {
                Pantools.logger.error("The pangenome does not contain genomes.");
                throw new RuntimeException("Invalid graph structure");
            }
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Could not find the genome nodes in the graph database.");
            throw new DataException("Invalid graph structure");
        }
    }

    /**
     * check if pangenome contains annotation and if not throw an exception
     */
    public static void validateAnnotations() {
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> annotationNodes = GRAPH_DB.findNodes(ANNOTATION_LABEL)) {
            if (!annotationNodes.hasNext()) {
                Pantools.logger.error("The pangenome does not contain annotations. Please run add_annotations first.");
                throw new RuntimeException("Invalid graph structure");
            }
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Could not find the annotation nodes in the graph database.");
            throw new DataException("Invalid graph structure");
        }
    }

    /**
     * check if pangenome contains accession information and if not throw an exception
     */
    public static void validateAccessions() {
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> accessionNodes = GRAPH_DB.findNodes(ACCESSION_LABEL)) {
            if (!accessionNodes.hasNext()) {
                throw new RuntimeException("Invalid graph structure");
            }
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Could not find the accession nodes in the graph database.");
            throw new DataException("Invalid graph structure");
        }
    }

    /**
     * check if pangenome contains PAV/VCF accession information and if not throw an exception
     *
     * @param type type of accession information (PAV/VCF)
     */
    public static void validateAccessions(String type) {
        assert (type.matches("PAV|VCF"));
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> accessionNodes = GRAPH_DB.findNodes(ACCESSION_LABEL, type, true)) {
            if (!accessionNodes.hasNext()) {
                Pantools.logger.error("The pangenome does not contain {} information.", type);
                throw new RuntimeException("Invalid graph structure");
            }
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Could not find {} nodes in the graph database.", type);
            throw new DataException("Invalid graph structure");
        }
    }

    /**
     * Remove a node and ints relationships from the graph database.
     *
     * @param node node to remove
     */
    public static void removeNode(Node node) { // TODO: make general neo4j functions
        for (Relationship relationship : node.getRelationships()) {
            relationship.delete();
        }
        node.delete();
    }

    /**
     * Checks if this pangenome contains accessions (variants in VCF format) that were added with add_variants.
     *
     * @return true if the pangenome contains accessions, false otherwise
     */
    public static boolean containsVariantInformation() {
        final boolean containsVcf;
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> accessionNodes = GRAPH_DB.findNodes(ACCESSION_LABEL, "VCF", true)) {
            containsVcf = accessionNodes.hasNext();
        }
        return containsVcf;
    }
}

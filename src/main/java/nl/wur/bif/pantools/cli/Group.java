package nl.wur.bif.pantools.cli;

import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.cli.validation.Constraints.ScoringMatrix;
import nl.wur.bif.pantools.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Generate homology groups based on similarity of protein sequences.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "group", sortOptions = false, abbreviateSynopsis = true)
public class Group implements Callable<Integer> {

    @Spec static CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-A", "--annotations-file"})
    @InputFile(message = "{file.annotations}")
    Path annotationsFile;

    @Option(names = "--longest")
    boolean longestTranscript;

    @DecimalMin(value = "0.001", message = "{range.ir}")
    @DecimalMax(value = "0.1", message = "{range.ir")
    static double intersectionRate;

    @Min(value = 1, message = "{min.st}")
    @Max(value = 99, message = "{max.st}")
    static int similarityThreshold;

    @Min(value = 1, message = "{min.mcl-i}")
    @Max(value = 99, message = "{max.mcl-i}")
    static double mclInflation;

    @DecimalMin(value = "0", message = "{min.contrast}")
    @DecimalMax(value = "10", message = "{max.contrast}")
    static double contrast;

    @Option(names = "--scoring-matrix")
    @ScoringMatrix(message = "{scoring_matrix}")
    String scoringMatrix;

    @ArgGroup(multiplicity = "1") RelaxationSettings relaxationSettings;
    private static class RelaxationSettings {
        @Option(names = {"--relaxation"})
        void setParams(int value) {
            try {
                intersectionRate = new double[]{0.08, 0.07, 0.06, 0.05, 0.04, 0.03, 0.02, 0.01}[value - 1];
                similarityThreshold = new int[]{95, 85, 75, 65, 55, 45, 35, 25}[value - 1];
                mclInflation = new double[]{10.8, 9.6, 8.4, 7.2, 6.0, 4.8, 3.6, 2.4}[value - 1];
                contrast = new double[]{8, 7, 6, 5, 4, 3, 2, 1}[value - 1];
            } catch (Exception e) {
                throw new ParameterException(spec.commandLine(), "--relaxation should be in range [1..8]");
            }
        }

        @ArgGroup(exclusive = false) RelaxationParameters params;
    }

    private static class RelaxationParameters {
        @Option(names = "--intersection-rate", required = true)
        void setIntersectionRate(double value) {intersectionRate = value;}

        @Option(names = "--similarity-threshold", required = true)
        void setSimilarityThreshold(int value) {similarityThreshold = value;}

        @Option(names = "--mcl-inflation", required = true)
        void setMclInflation(double value) {mclInflation = value;}

        @Option(names = "--contrast", required = true)
        void setContrast(double value) {contrast = value;}
    }

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectGenomes);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        proLayer.group(scoringMatrix);
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        if (annotationsFile != null) PATH_TO_THE_ANNOTATIONS_FILE = String.valueOf(annotationsFile);
        THREADS = threadNumber.getnThreads();
        INTERSECTION_RATE = intersectionRate;
        MIN_NORMALIZED_SIMILARITY = similarityThreshold;
        MCL_INFLATION = mclInflation;
        CONTRAST = contrast;
        longest_transcripts = longestTranscript;
    }
}

package nl.wur.bif.pantools.cli;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Classify functional annotations as core, accessory or unique.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "functional_classification", sortOptions = false, abbreviateSynopsis = true)
public class FunctionalClassification implements Callable<Integer> {

    @Spec CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-A", "--annotations-file"})
    @InputFile(message = "{file.annotations}")
    Path annotationsFile;

    @Option(names = {"-p", "--phenotype"})
    String phenotype;

    @Option(names = "--core-threshold")
    @Min(value = 0, message = "{min.ct}")
    @Max(value = 100, message = "{max.ct}")
    int coreThreshold;

    @Option(names = "--unique-threshold")
    @Min(value = 0, message = "{min.ut}")
    @Max(value = 100, message = "{max.ut}")
    int uniqueThreshold;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        functionalAnnotations.functional_classification();
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        if (annotationsFile != null) PATH_TO_THE_ANNOTATIONS_FILE = annotationsFile.toString();
        core_threshold = coreThreshold;
        unique_threshold = uniqueThreshold;
        PHENOTYPE = phenotype;
    }

}

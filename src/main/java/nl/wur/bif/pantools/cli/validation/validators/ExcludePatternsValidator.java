package nl.wur.bif.pantools.cli.validation.validators;

import nl.wur.bif.pantools.cli.validation.Constraints.ExcludePatterns;
import nl.wur.bif.pantools.cli.validation.Constraints.ExcludePatterns.Flag;
import nl.wur.bif.pantools.pantools.Pantools;
import nl.wur.bif.pantools.utils.ConsoleUtils;
import org.apache.commons.beanutils.PropertyUtils;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static java.util.regex.Pattern.compile;
/**
 * Custom ConstraintValidator that verifies whether an input string matches 'forbidden' words. Allows queries to enable
 * yes|no user input for continuing after encountering a forbidden word.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public class ExcludePatternsValidator implements ConstraintValidator<ExcludePatterns, Object> {

    private List<Pattern> patterns;
    private String query;
    private String field;

    @Override
    public void initialize(ExcludePatterns constraintAnnotation) {
        final Flag[] flags = constraintAnnotation.flags();
        int intFlag = 0;
        for (Flag flag : flags) intFlag = intFlag | flag.getValue();
        final String[] regexps = constraintAnnotation.regexp().split(",");
        patterns = new ArrayList<>();

        try {
            for (String regex : regexps) {
                patterns.add(compile(regex, intFlag));
            }
        } catch (PatternSyntaxException e) {
            throw new IllegalArgumentException("Invalid regular expression.", e);
        }

        field = constraintAnnotation.field();
        query = constraintAnnotation.query() + ", are you sure you want to continue?";
    }

    /**
     * Verifies whether a string does NOT match any of the given patterns. If there is a match and a query is given, the
     * user can decide to continue.
     *
     * @param classToValidate subcommand class containing the field to validate and Pantools parent command
     * @param context ConstraintValidatorContext containing contextual data for a given constraint validator
     * @return boolean for validity of the constraint
     */
    @Override
    public boolean isValid(Object classToValidate, ConstraintValidatorContext context) {
        try {
            final String value = (String) PropertyUtils.getProperty(classToValidate, field);
            if (value == null) return true;
            boolean input = false;

            try {
                final Pantools pantools = (Pantools) PropertyUtils.getProperty(classToValidate, "pantools");
                if (pantools.isForce()) return true;
                input = pantools.isInput();
            } catch (Exception ignored) {
                // If the class does not contain a Pantools parent command, it can still validate without user input.
            }

            for (Pattern pattern : patterns) {
                final Matcher matcher = pattern.matcher(value);
                if (!matcher.matches()) continue;
                if (query.isEmpty() || !input) return false;
                query = String.format(query, value);
                return ConsoleUtils.askYesOrNo(query);
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
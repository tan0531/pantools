package nl.wur.bif.pantools.cli;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pantools.Pantools;

import java.io.IOException;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Calculate the number of core, accessory, unique, k-mer sequences.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "k_mer_classification", aliases = "kmer_classification", sortOptions = false, abbreviateSynopsis = true)
public class KmerClassification implements Callable<Integer> {

    @Spec CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-p", "--phenotype"})
    private String phenotype;

    @Option(names = "--compressed")
    boolean compressed;

    @Option(names = "--core-threshold")
    @Min(value = 0, message = "{min.ct}")
    @Max(value = 100, message = "{max.ct}")
    int coreThreshold;

    @Option(names = "--unique-threshold")
    @Min(value = 0, message = "{min.ut}")
    @Max(value = 100, message = "{max.ut}")
    int uniqueThreshold;

    @Option(names = "--phenotype-threshold")
    @Min(value = 0, message = "{min.pt}")
    @Max(value = 100, message = "{max.pt}")
    int phenotypeThreshold;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes);

        pantools.setPangenomeGraph("pangenome");
        setGlobalParameters(); //TODO: use local parameters instead

        classification.kmer_classification2();
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        core_threshold = coreThreshold;
        unique_threshold = uniqueThreshold;
        phenotype_threshold = phenotypeThreshold;
        PHENOTYPE = phenotype;
        if (compressed) Mode = "COMPRESSED";
    }

}

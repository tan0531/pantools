package nl.wur.bif.pantools.cli;

import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.cli.mixins.SelectHmGroups;
import nl.wur.bif.pantools.pantools.Pantools;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.*;
import static nl.wur.bif.pantools.cli.validation.Constraints.Patterns.Flag.CASE_INSENSITIVE;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Report all available information of one or multiple homology groups.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "group_info", sortOptions = false)
public class GroupInfo implements Callable<Integer> {

    @Spec Model.CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;
    @ArgGroup private SelectHmGroups selectHmGroups = new SelectHmGroups();

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = "--functions")
    void setFunctions(String value) {
        functions = Arrays.asList(value.split(","));
    }
    @Patterns(regexp = "(GO:|PF|IPR|TIGR)[0-9]+|secreted|receptor|transmembrane", flags = CASE_INSENSITIVE,
            message = "{patterns.functions}")
    List<String> functions;

    @Option(names = {"-g", "--genes"})
    void setGenes(String value) {
        genes = Arrays.asList(value.split(","));
    }
    List<String> genes;

    @Option(names = {"--node"})
    boolean obtainNodes;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes, selectHmGroups);
        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        classification.homology_group_info(selectHmGroups.getHomologyGroups());
        return 0;
    }

    private void setGlobalParameters() throws IOException {
        setGenomeSelectionOptions(selectGenomes);
        if (genes != null) SELECTED_NAME = genes.toString().replaceAll("[\\[\\]]", "");
        if (functions != null) SELECTED_LABEL = functions.toString().replaceAll("[\\[\\]]", "");

        if (obtainNodes) {
            Mode = "K-MER";
        }
    }

}

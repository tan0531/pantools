package nl.wur.bif.pantools.cli;

import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Delete phenotype nodes or remove specific phenotype information from the nodes.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "remove_phenotypes", sortOptions = false)
public class RemovePhenotypes implements Callable<Integer> {

    @Spec CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-p", "--phenotype"})
    private String phenotype;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        classification.removePhenotype();
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        PHENOTYPE = phenotype;
    }
}

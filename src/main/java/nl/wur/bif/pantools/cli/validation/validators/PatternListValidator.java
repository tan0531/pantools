package nl.wur.bif.pantools.cli.validation.validators;

import nl.wur.bif.pantools.cli.validation.Constraints.Patterns.Flag;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static java.util.regex.Pattern.compile;
import static nl.wur.bif.pantools.cli.validation.Constraints.*;

/**
 * Custom version of @Pattern constraint that matches a list of strings with a regular expression.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public class PatternListValidator implements ConstraintValidator<Patterns, List<String>> {

    private Pattern pattern;

    @Override
    public void initialize(Patterns constraintAnnotation) {
        Flag[] flags = constraintAnnotation.flags();
        int intFlag = 0;
        for (Flag flag : flags) intFlag = intFlag | flag.getValue();

        try {
            pattern = compile(constraintAnnotation.regexp(), intFlag);
        } catch (PatternSyntaxException e) {
            throw new IllegalArgumentException("Invalid regular expression.", e);
        }
    }

    /**
     * Verifies whether the strings in the provided list match the regular expression provided in the constraint
     * @param list list of string values to be validated
     * @param context ConstraintValidatorContext containing contextual data for a given constraint validator
     * @return true if ALL strings in the list match the given regular expression, false otherwise
     */
    @Override
    public boolean isValid(List<String> list, ConstraintValidatorContext context) {
        if (list == null) return true;
        for (String string : list) {
            final Matcher matcher = pattern.matcher(string);
            if (!matcher.matches()) return false;
        }
        return true;
    }
}

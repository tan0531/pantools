package nl.wur.bif.pantools.cli;

import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import nl.wur.bif.pantools.cli.validation.Constraints.InputFile;
import nl.wur.bif.pantools.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Adds genomic features to genomes and generates their proteomes.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "add_annotations", sortOptions = false)
public class AddAnnotations implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "annotations-file", index = "0+")
    @InputFile(message = "{file.annotations}")
    Path annotationsFile;

    @Option(names = "--ignore-invalid-features")
    boolean ignoreInvalidFeatures;

    @Option(names = "--connect")
    boolean connectAnnotations;

    @Option(names = "--assume-one-mrna-per-cds")
    boolean assumeOneMrnaPerCds;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        pantools.setPangenomeGraph("pangenome");

        final Path databaseDirectory = pantools.getDatabaseDirectory();
        annLayer.addAnnotations(databaseDirectory, annotationsFile,
                ignoreInvalidFeatures, connectAnnotations, assumeOneMrnaPerCds);
        return 0;
    }
}

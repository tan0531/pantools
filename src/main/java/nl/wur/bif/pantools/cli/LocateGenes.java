package nl.wur.bif.pantools.cli;

import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.cli.mixins.SelectHmGroups;
import nl.wur.bif.pantools.pantools.Pantools;
import picocli.CommandLine.Command;
import picocli.CommandLine.Model.CommandSpec;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import nl.wur.bif.pantools.utils.BeanUtils;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Identify and compare gene clusters of from a set of homology groups.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "locate_genes", sortOptions = false, abbreviateSynopsis = true)
public class LocateGenes implements Callable<Integer> {

    @Spec CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;
    @ArgGroup(multiplicity = "1") private SelectHmGroups selectHmGroups;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-p", "--phenotype"})
    String phenotype;

    @Min(value = 0, message = "{min.nucleotides}")
    @Option(names = "--nucleotides")
    int maxNucleotides;

    @Min(value = 0, message = "{min.gap-open}")
    @Option(names = "--gap-open")
    int gapOpen;

    @Option(names = "--core-threshold")
    @Min(value = 0, message = "{min.ct}")
    @Max(value = 100, message = "{max.ct}")
    int coreThreshold;

    @Option(names = "--ignore-duplications")
    boolean ignoreDuplications;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes, selectHmGroups);
        pantools.setPangenomeGraph("pangenome");
        setGlobalParameters(); //TODO: use local parameters instead

        classification.locate_genes();
        return 0;
    }

    private void setGlobalParameters() throws IOException {
        setGenomeSelectionOptions(selectGenomes);
        SELECTED_HMGROUPS = selectHmGroups.getHomologyGroups().stream().map(String::valueOf).collect(Collectors.joining(","));
        core_threshold = coreThreshold;
        PHENOTYPE = phenotype;
        if (ignoreDuplications) Mode = "IGNORE-COPIES";
        NODE_VALUE = Integer.toString(maxNucleotides);
        GAP_OPEN = gapOpen;
    }

}

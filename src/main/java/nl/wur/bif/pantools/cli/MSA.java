package nl.wur.bif.pantools.cli;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.cli.mixins.SelectHmGroups;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pangenome.MultipleSequenceAlignment;
import nl.wur.bif.pantools.pantools.Pantools;
import nl.wur.bif.pantools.utils.GraphUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import static jakarta.validation.constraints.Pattern.Flag.CASE_INSENSITIVE;
import static nl.wur.bif.pantools.cli.validation.Constraints.*;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Create multiple sequence alignments.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "msa", sortOptions = false, abbreviateSynopsis = true)
public class MSA implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;
    @ArgGroup private SelectHmGroups selectHmGroups = new SelectHmGroups();

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-R", "--regions-file"})
    @InputFile(message = "{file.regions}")
    private Path regionsFile;

    @Option(names = "--method")
    @Pattern(regexp = "per-group|multiple-groups|regions|functions", flags = CASE_INSENSITIVE,
            message = "{pattern.method-msa}")
    private String method;

    private static boolean alignNucleotide;
    private static boolean alignProtein;
    private static boolean alignVariants;
    private static boolean pavs;

    @ArgGroup AlignmentMode alignmentMode;
    private static class AlignmentMode {

        @Option(names = "--align-nucleotide")
        void setAlignNucleotide(boolean value) {
            alignNucleotide = value;
        }

        @Option(names = "--align-protein")
        void setAlignProtein(boolean value) {
            alignProtein = value;
        }

        @ArgGroup(exclusive = false)
        VariationOptions variationOptions;
        static class VariationOptions {
            @Option(names = {"-v", "--align-variants"}, required = true)
            void setVariants(boolean value) {
                alignVariants = value;
            }

            @Option(names = "--pavs")
            void setPavs(boolean value) {
                pavs = value;
            }
        }
    }

    @Option(names = "--functions")
    void setFunctions(String value) {
        functions = Arrays.asList(value.toUpperCase().split(","));
    }
    @Patterns(regexp = "(GO:|PF|IPR|TIGR)[0-9]+", message = "{patterns.functions}")
    List<String> functions;

    @Option(names = {"-p", "--phenotype"})
    boolean phenotype;

    @Option(names = "--blosum")
    @MatchInteger(value = {45, 50, 62, 80, 90}, message = "match.blosum")
    int blosum;

    @Option(names = "--phenotype-threshold")
    @Min(value = 0, message = "{min.pt}")
    @Max(value = 100, message = "{max.pt}")
    int phenotypeThreshold;

    @Option(names = "--no-trimming", negatable = true)
    boolean trimming;

    @Option(names = "--no-fasttree", negatable = true)
    boolean fastTree;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectGenomes, selectHmGroups);
        pantools.setPangenomeGraph();
        crossValidate();
        method = method.replace("-", "_");
        setGlobalParameters(); //TODO: use local parameters instead

        MultipleSequenceAlignment msa = new MultipleSequenceAlignment(
                method,
                alignNucleotide,
                alignProtein,
                alignVariants,
                pavs,
                selectHmGroups.getHomologyGroups()
        );
        msa.alignSequences(trimming, true);
        if (phenotype) msa.createPhenotypeMsaOutput(trimming);
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        if (regionsFile != null) PATH_TO_THE_REGIONS_FILE = regionsFile.toString();
        if (functions != null) SELECTED_NAME = functions.toString().replaceAll("[\\[\\]]", "");
        THREADS = threadNumber.getnThreads();
        BLOSUM = blosum;
        phenotype_threshold = phenotypeThreshold;
        FAST = !fastTree;
    }

    /**
     * Validate whether method specific/exclusive parameters are properly set. Method "regions" is unavailable for
     * protein alignment and requires a regions file; method "functions" requires a list of functions and the regions
     * file and function list are exclusive to their specific method.
     */
    private void crossValidate() {
        // Assert that the method is not "regions" for protein alignment
        if (alignProtein && method.equals("regions")) {
            throw new ParameterException(spec.commandLine(), "Method 'regions' is unavailable for protein alignment");
        }
        // Assert that the regions file is set if the method is "regions"
        if (regionsFile == null && method.equals("regions")) {
            throw new ParameterException(spec.commandLine(), "--regions-file required for method 'regions'");
        }
        // Assert that the method is "regions" if the regions file is set
        if (regionsFile != null && !method.equals("regions")) {
            throw new ParameterException(spec.commandLine(), "--regions-file is for method 'regions' only");
        }
        // Assert that the functions are set if the method is "functions"
        if (functions == null && method.equals("functions")) {
            throw new ParameterException(spec.commandLine(), "--functions is required for method 'functions'");
        }
        // Assert that the method is "functions" if the functions are set
        if (functions != null && !method.equals("functions")) {
            throw new ParameterException(spec.commandLine(), "--functions is for method 'functions' only");
        }
        // Set default alignment mode if none were provided
        if (!alignNucleotide && !alignProtein && !alignVariants) {
            if (PROTEOME) {
                Pantools.logger.info("No alignment mode was provided, defaulting to protein alignment.");
                alignProtein = true;
            } else if (GraphUtils.containsVariantInformation()) {
                Pantools.logger.info("No alignment mode was provided, defaulting to variant alignment.");
                alignVariants = true;
            } else {
                Pantools.logger.info("No alignment mode was provided, defaulting to nucleotide alignment.");
                alignNucleotide = true;
            }
        }
        // Assert that nucleotide alignment is not performed on a panproteome
        if (alignNucleotide && PROTEOME) {
            throw new ParameterException(spec.commandLine(), "Nucleotide alignment is unavailable for panproteomes");
        }
        // Assert that nucleotide alignment is not performed on a panproteome
        if (alignVariants && PROTEOME) {
            throw new ParameterException(spec.commandLine(), "Variant alignment is unavailable for panproteomes");
        }
        // Assert that the database contains variants if the method is "variants"
        if (alignVariants) {
            GraphUtils.validateAccessions("VCF");
        }
        // Assert that alignVariants is not used for "regions" method
        if (alignVariants && method.equals("regions")) {
            throw new ParameterException(spec.commandLine(), "Variants cannot be aligned with method 'regions'");
        }
    }
}

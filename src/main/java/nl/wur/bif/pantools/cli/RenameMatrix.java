package nl.wur.bif.pantools.cli;

import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Rename the headers of CSV formatted matrix files.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "rename_matrix", sortOptions = false)
public class RenameMatrix implements Callable<Integer> {

    @Spec CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "matrix-file", index = "0+")
    @InputFile(message = "{file.matrix}")
    Path matrixFile;

    @ArgGroup(exclusive = false) PhenotypeOptions phenotypeOptions;
    static class PhenotypeOptions {
        @Option(names = {"-p", "--phenotype"}, required = true)
        String phenotype;

        @Option(names = "--no-numbers", negatable = true)
        boolean numbers;
    }

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        classification.rename_matrix();
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        INPUT_FILE = matrixFile.toString();
        if (phenotypeOptions != null) {
            PHENOTYPE = phenotypeOptions.phenotype;
            if (!phenotypeOptions.numbers) Mode = "NO-NUMBERS";
        }
    }

}

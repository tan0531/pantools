package nl.wur.bif.pantools.cli;

import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import nl.wur.bif.pantools.pantools.Pantools;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Step 1/3 of MLSA. Search and filter suitable genes for the MLSA.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "mlsa_find_genes", sortOptions = false)
public class MLSAFindGenes implements Callable<Integer> {

    @Spec CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-g", "--genes"}, required = true)
    void setGenes(String value) {
        genes = Arrays.asList(value.toUpperCase().split(","));
    }
    List<String> genes;

    @Option(names = "--extensive")
    boolean extensive;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes);

        pantools.setPangenomeGraph("pangenome");
        setGlobalParameters(); //TODO: use local parameters instead

        phylogeny.mlsa_find_genes(true);
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        SELECTED_NAME = genes.toString().replaceAll("[\\[\\]]", "").replace(" ","");
        if (extensive) Mode = "EXTENSIVE";
    }

}

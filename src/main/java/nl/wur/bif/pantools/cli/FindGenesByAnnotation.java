package nl.wur.bif.pantools.cli;

import jakarta.validation.constraints.Size;
import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.cli.validation.Constraints.Patterns;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.StringUtils.stringToIntegerList;
import static picocli.CommandLine.*;

/**
 * Find genes of interest in the pangenome that share a functional annotation node and extract the nucleotide and
 * protein sequence.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "find_genes_by_annotation", sortOptions = false)
public class FindGenesByAnnotation implements Callable<Integer> {

    @Spec static CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @ArgGroup(multiplicity = "1")
    Identifiers identifiers;
    private static class Identifiers {
        @Option(names = "--functions", required = true)
        void setFunctions(String value) {
            functions = Arrays.asList(value.toUpperCase().split(","));
        }
        @Patterns(regexp = "(GO:|PF|IPR|TIGR)[0-9]+", message = "{patterns.functions}")
        List<String> functions;

        @Option(names = {"-n", "--nodes"}, required = true)
        void setNodes(String value) {
            nodes = stringToIntegerList(value);
        }
        @Size(min = 1, message = "{size.empty.node}")
        List<Integer> nodes;
    }

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes, identifiers);

        pantools.setPangenomeGraph("pangenome");
        setGlobalParameters(); //TODO: use local parameters instead

        classification.find_genes_by_annotation();
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        if (identifiers.nodes != null) NODE_ID = identifiers.nodes.toString().replaceAll("[\\[\\]]", "");
        if (NODE_ID.matches("^[0-9]*$") && NODE_ID.length() > 0) NODE_ID_long = Long.parseLong(NODE_ID);
        if (identifiers.functions != null) SELECTED_NAME = identifiers.functions.toString().replaceAll("[\\[\\]]", "");
    }
}

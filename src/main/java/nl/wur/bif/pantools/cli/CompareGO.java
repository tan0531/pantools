package nl.wur.bif.pantools.cli;

import jakarta.validation.constraints.Size;
import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.cli.validation.Constraints.Patterns;
import static nl.wur.bif.pantools.cli.validation.Constraints.Patterns.Flag.CASE_INSENSITIVE;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.StringUtils.stringToIntegerList;
import static picocli.CommandLine.*;

/**
 * For two given GO terms, move up in the GO hierarchy to see if they are related.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "compare_go", sortOptions = false)
public class CompareGO implements Callable<Integer> {

    @Spec static CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @ArgGroup(multiplicity = "1") Identifiers identifiers;
    private static class Identifiers {
        @Option(names = "--functions", required = true)
        void setFunctions(String value) {
            functions = Arrays.asList(value.split(","));
        }
        @Patterns(regexp = "GO:[0-9]+", flags = CASE_INSENSITIVE, message = "{patterns.go}")
        @Size(min = 2, max = 2, message = "{size.go}")
        List<String> functions;

        @Option(names = {"-n", "--nodes"}, required = true)
        void setNodes(String value) {
            nodes = stringToIntegerList(value);
        }
        @Size(min = 2, max = 2, message = "{size.node}")
        @Size(min = 1, message = "{size.empty.node}")
        List<Integer> nodes;
    }

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes, identifiers);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        functionalAnnotations.compare_go();
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        if (identifiers.nodes != null) NODE_ID = identifiers.nodes.toString().replaceAll("[\\[\\]]", "");
        if (NODE_ID.matches("^[0-9]*$") && NODE_ID.length() > 0) NODE_ID_long = Long.parseLong(NODE_ID);
        if (identifiers.functions != null) SELECTED_NAME = identifiers.functions.toString().replaceAll("[\\[\\]]", "");
    }

}

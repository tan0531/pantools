package nl.wur.bif.pantools.variation.add_variants;

import nl.wur.bif.pantools.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.utils.GraphUtils;
import nl.wur.bif.pantools.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.cli.validation.Constraints.InputFile;
import static picocli.CommandLine.*;

/**
 * Adds variants from a VCF file to the pantools database.
 *
 * @author Dirk-Jan van Workum, Wageningen University, the Netherlands.
 */
@Command(name = "add_variants", aliases = "add_variant", sortOptions = false)
public class AddVariantsCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "vcfs-file", index = "0+")
    @InputFile(message = "{file.vcfs}")
    Path vcfLocationsFile;

    @Option(names = "--scratch-directory")
    Path scratchDirectory;

    @Option(names = "--keep-intermediate-files")
    boolean keepIntermediateFiles;

    @Override
    public Integer call() throws IOException {
        // initialize logging
        pantools.createLogger(spec);

        // validate command line arguments
        BeanUtils.argValidation(spec, this, threadNumber);

        // initialize the neo4j graph database
        final Path databaseDirectory = pantools.getDatabaseDirectory();
        GraphUtils.createGraphDatabaseService(databaseDirectory);
        GraphUtils.setDatabaseParameters();

        // validate the neo4j graph database
        GraphUtils.validateGraphType("pangenome");
        GraphUtils.validateGenomes();
        GraphUtils.validateAnnotations();

        // run the main code
        new AddVariants(vcfLocationsFile, threadNumber.getnThreads(), scratchDirectory, keepIntermediateFiles);

        // return exit code 0 if successful
        return 0;
    }
}

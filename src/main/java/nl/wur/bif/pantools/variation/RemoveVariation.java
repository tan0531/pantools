package nl.wur.bif.pantools.variation;

import nl.wur.bif.pantools.pantools.Pantools;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

import static nl.wur.bif.pantools.utils.Globals.*;

public abstract class RemoveVariation {

    /**
     * Remove VCF or PAV information from accession nodes. If they contain no other information, remove them.
     *
     * @param type information type, VCF or PAV
     */
    protected void removeAccessionInformation(String type) {
        assert type.matches("VCF|PAV");
        Pantools.logger.info("Removing {} information from accession nodes.", type);
        try (Transaction tx = GRAPH_DB.beginTx();
             ResourceIterator<Node> accessionNodes = GRAPH_DB.findNodes(ACCESSION_LABEL, type, true)) {
            while (accessionNodes.hasNext()) {
                removeAccessionNode(accessionNodes.next());
            }
            tx.success();
        }
    }

    /**
     * Remove VCF or PAV information from variation nodes. If they contain no other information, remove them.
     *
     * @param type information type, VCF or PAV
     */
    protected void removeVariantInformation(String type) {
        assert type.matches("VCF|PAV");
        Pantools.logger.info("Removing {} information from variant nodes.", type);
        try (Transaction tx = GRAPH_DB.beginTx();
             ResourceIterator<Node> variantNodes = GRAPH_DB.findNodes(VARIANT_LABEL)) {
            while (variantNodes.hasNext()) {
                removeVariantNode(variantNodes.next());
            }
            tx.success();
        }
    }

    protected abstract void removeAccessionNode(Node accessionNode);

    protected abstract void removeVariantNode(Node variantNode);

}

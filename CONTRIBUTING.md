# Contributing to _PanTools_

## Get in touch

There are many ways to get in touch with the _PanTools_ team!

- GitLab [issues][pantools-issues] and [merge requests][pantools-mergerequests]
  - Join a discussion, collaborate on an ongoing task and exchange your thoughts with others. You will have to request a guest role to start contributing.
  - Can't find your idea being discussed anywhere?
    [Open a new issue](https://git.wur.nl/bioinformatics/pantools/-/issues/new)! (See our [Where to start: issues](#where-to-start-issues) section below.)
- Contact the Project Lead of the _PanTools_ project - Sandra Smit - by email at [sandra.smit@wur.nl](mailto:sandra.smit@wur.nl).

## Contributing through GitLab

[Git][git] is a really useful tool for version control. 
[GitLab][gitlab] sits on top of Git and supports collaborative and distributed working.

We know that it can be daunting to start using Git and GitLab if you haven't worked with them in the past, but the _PanTools_ maintainers are here to help you figure out any of the jargon or confusing instructions you encounter.

GitLab has a helpful page on [getting started with GitLab][gitlab-gettingstarted].

In order to contribute via GitLab, you'll need to [set up an account][gitlab-signup] and [sign in][gitlab-signin].
Remember that you can ask us any questions you need to along the way.

## Writing in Markdown

Most of the writing that you'll do will be in [Markdown][markdown].
You can think of Markdown as a few little symbols around your text that will allow GitLab to render the text with a little bit of formatting.
For example, you could write words as **bold** (`**bold**`), or in _italics_ (`_italics_`), or as a [link][rick-roll] (`[link](https://youtu.be/dQw4w9WgXcQ)`) to another webpage.

Also when writing in Markdown, please start each new sentence on a new line.
Having each sentence on a new line will make no difference to how the text is displayed, there will still be paragraphs, but it makes the diffs produced during the merge request review easier to read!


## Where to start: issues

Before you open a new issue, please check if any of our [open issues](https://git.wur.nl/bioinformatics/pantools/-/issues?scope=all&state=opened) cover your idea already.

## Making a change with a merge request

We appreciate all contributions to _PanTools_.
**THANK YOU** for helping us build this useful resource.

All project management, conversations and questions related to the _PanTools_ project happens here in the _PanTools_ repository.

The following steps are a guide to help you contribute in a way that will be easy for everyone to review and accept with ease.

### 1. Comment on an [existing issue][pantools-issues] or open a new issue referencing your addition

This allows other members of the _PanTools_ team to confirm that you aren't overlapping with work that's currently underway and that everyone is on the same page with the goal of the work you're going to carry out.

[This blog](https://www.igvita.com/2011/12/19/dont-push-your-pull-requests/) is a nice explanation of why putting this work in upfront is so useful to everyone involved.

You will need a guest role to create new issues using the _PanTools_ repository website. You can request a guest role by [getting in touch](#get-in-touch).

Alternatively, you can send an [email][pantools-email] to create a new issue. The title of the email will be used as the issue title and the email body will be put in the issue description.
Note that we are in the process of setting up this email functionality; when this is done, we will update the email link here.

### 2. [Fork][gitlab-fork] the [_PanTools_ repository][pantools-repo]

This is now your own unique copy of _PanTools_.
Changes here won't affect anyone else's work, so it's a safe space to explore edits to the code!

Make sure to [keep your fork up to date][gitlab-syncfork] with the main repository, otherwise, you can end up with lots of dreaded [merge conflicts][gitlab-mergeconflicts].

The repository website only provides functionality for forking inside the same server (git.wur.nl). You can fork the _PanTools_ project onto another GitLab server. Forking onto a GitHub server is currently not possible, but this is being worked on.
First create a blank repository on GitLab, let's assume it's called https://gitlab.com/johndoe/my-pantools
Use the following commands in a directory where you wish to fork _PanTools_.

~~~bash
mkdir my-pantools
cd my-pantools
git init
git remote add origin https://gitlab.com/johndoe/my-pantools
git remote add upstream https://git.wur.nl/bioinformatics/pantools
#There are now two remotes: origin (your remote fork) and upstream (the pantools repository).

git pull upstream master				#Get the pantools content.
git push --set-upstream origin master	#Push this content to the origin (fork).
~~~

### 3. Make the changes you've discussed

Try to keep the changes focused.
If you submit a large amount of work all in one go it will be much more work for whoever is reviewing your merge request.

While making your changes, commit often and write good, detailed commit messages.
[This blog](https://chris.beams.io/posts/git-commit/) explains how to write a good Git commit message and why it matters.
It is also perfectly fine to have a lot of commits - including ones that break code.
A good rule of thumb is to push up to GitLab when you _do_ have passing tests then the continuous integration (CI) has a good chance of passing everything.

If you feel tempted to "branch out" then please make a [new branch][gitlab-branches] and a [new issue](https://git.wur.nl/bioinformatics/pantools/-/issues/new) to go with it. [This blog](https://nvie.com/posts/a-successful-git-branching-model/) details the different Git branching models.

Please do not re-write history!
That is, please do not use the [rebase](https://docs.gitlab.com/ee/topics/git/git_rebase.html) command to edit previous commit messages, combine multiple commits into one, or delete or revert commits that are no longer necessary.

Are you new to Git and GitLab or just want a detailed guide on getting started with version control? Check out the [Version Control chapter](https://the-turing-way.netlify.com/version_control/version_control.html) in _The Turing Way_ Book!

### 4. Submit a [merge request][gitlab-mergerequest]

We encourage you to open a merge request as early in your contributing process as possible.
This allows everyone to see what is currently being worked on.
It also provides you, the contributor, feedback in real-time from both the community and the continuous integration as you make commits (which will help prevent stuff from breaking).

When you are ready to submit a merge request, please describe in the merge request body:

- The problem you're trying to fix in the merge request, reference any related issue and use fixes/close to automatically close them, if pertinent.
- A list of changes proposed in the merge request.
- What the reviewer should concentrate their feedback on.

By providing as much detail as possible, you will make it really easy for someone to review your contribution!

If you have opened the merge request early and know that its contents are not ready for review or to be merged, add "[WIP]" at the start of the merge request title, which stands for "Work in Progress".
When you are happy with it and are happy for it to be merged into the main repository, change the "[WIP]" in the title of the merge request to "[Ready for review]".

A member of the _PanTools_ team will then review your changes to confirm that they can be merged into the main repository.
A [review][gitlab-review] will probably consist of a few questions to help clarify the work you've done.
Keep an eye on your GitLab notifications and be prepared to join in that conversation.

You can update your [fork][gitlab-fork] of the _PanTools_ [repository][pantools-repo] and the merge request will automatically update with those changes.
You don't need to submit a new merge request when you make a change in response to a review.

You can also submit merge requests to other contributors' branches!
Do you see an [open merge request](https://git.wur.nl/bioinformatics/pantools/-/merge_requests?scope=all&state=opened ) that you find interesting and want to contribute to?
Simply make your edits on their files and open a merge request to their branch!

GitLab has a [nice introduction][gitlab-flow] to the merge request workflow, but please [get in touch](#get-in-touch) if you have any questions.

## Local development

You can build and run _PanTools_ locally. Please refer to the [manual][pantools-manual] for instructions on how to build, and run _PanTools_.

## Recognizing Contributions

We welcome and recognise all kinds of contributions, from fixing small errors, to developing documentation, maintaining the project infrastructure, writing code or reviewing existing resources.

### Current Contributors

The _PanTools_ team wants to graciously thank the following people for their contributions to the _PanTools_ project.

- Astrid van der Brandt
- Dirk-Jan van Workum
- Eef Jonkheer
- Matthijs Moed
- Sandra Smit
- Siavash Sheikhizadeh
- Thijs van Lankveld

---

_These Contributing Guidelines have been adapted from the [Contributing Guidelines](https://github.com/bids-standard/bids-starter-kit/blob/master/CONTRIBUTING.md) of the [BIDS Starter Kit](https://github.com/bids-standard/bids-starter-kit)! (License: CC-BY)_

[pantools-repo]: https://git.wur.nl/bioinformatics/pantools
[pantools-issues]: https://git.wur.nl/bioinformatics/pantools/-/issues
[pantools-mergerequests]: https://git.wur.nl/bioinformatics/pantools/-/merge_request
[pantools-manual]: https://www.bioinformatics.nl/pangenomics/manual/
[pantools-email]: mailto:broken_link
[git]: https://git-scm.com
[gitlab]: https://gitlab.com
[gitlab-signup]: https://gitlab.com/users/sign_up
[gitlab-signin]:https://gitlab.com/users/sign_in
[gitlab-gettingstarted]:https://about.gitlab.com/get-started/
[gitlab-fork]: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html
[gitlab-syncfork]: https://about.gitlab.com/blog/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/
[gitlab-mergeconflicts ]:https://docs.gitlab.com/ee/topics/git/merge_conflicts.html
[gitlab-branches]: https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-new-branch
[gitlab-rebase]:https://docs.gitlab.com/ee/topics/git/git_rebase.html
[gitlab-mergerequest]: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html
[gitlab-review]: https://docs.gitlab.com/ee/development/code_review.html
[gitlab-flow]: https://docs.gitlab.com/ee/topics/gitlab_flow.html
[markdown]: https://daringfireball.net/projects/markdown
[rick-roll]: https://www.youtube.com/watch?v=dQw4w9WgXcQ
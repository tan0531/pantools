# Configuration file for the Sphinx documentation builder.
import os
import sys

sys.path.append(os.path.abspath("./_ext"))

# -- Project information

project = 'PanTools'
copyright = '2016, the PanTools team'
author = 'Sandra Smit'

release = '4.2.2'
version = '4.2.2'

# -- General configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosectionlabel',
    'sphinx.ext.autosummary',
    'sphinx.ext.duration',
    'sphinx.ext.doctest',
    'sphinx.ext.graphviz',
    'sphinx.ext.intersphinx',
    'sphinx.ext.mathjax',
    'substitutioncodeblock',
]

intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}

intersphinx_disabled_domains = ['std']

templates_path = ['_templates']

# -- Options for HTML output

html_theme = 'sphinx_rtd_theme'

# -- Options for EPUB output

epub_show_urls = 'footnote'

# Make sure the target is unique
autosectionlabel_prefix_document = True

# Enable numref
numfig = True

# Math alignment configuration
mathjax3_config = {'chtml': {'displayAlign': 'left',
                             'displayIndent': '2em'}}

# CSS files
html_css_files = ['css/custom.css', 'css/s5defs-roles.css']

# Code substitutions
substitutions = [('|ProjectVersion|', version)]

# Prolog
rst_prolog = """
.. include:: <s5defs.txt>
.. default-role::

.. |br| raw:: html

     <br>
"""

# Epilog
rst_epilog = """
.. |ProjectVersion| replace:: {version}
.. |PantoolsGit| replace:: https://git.wur.nl/bioinformatics/pantools/-/tree/v{version}
""".format(version = version,)


Installing PanTools
===================

There are multiple options for installing PanTools:

1. :ref:`user_guide/install:install from bioconda` (recommended)
2. :ref:`user_guide/install:install from download`
3. :doc:`/developer_guide/install` (for developers)

--------------

Install from bioconda
---------------------

The easiest way to install PanTools is through `bioconda
<https://bioconda.github.io/>`_. Bioconda is a channel for the conda package
manager specializing in bioinformatics software.

For users that don't have conda installed, we recommend to install conda first:

.. substitution-code-block:: bash

   $ wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
   $ bash Miniconda3-latest-Linux-x86_64.sh

   # Follow the instructions on the screen
   # Restart your terminal

   $ conda config --set auto_activate_base false
   $ conda config --add channels bioconda
   $ conda config --add channels conda-forge
   $ conda config --remove channels defaults
   $ conda config --set channel_priority strict

   # Restart your terminal

Next, we recommend to install mamba into the conda base environment to enable
much faster dependency solving:

.. substitution-code-block:: bash

   $ conda install -n base mamba

Finally, we can install PanTools. We recommend to install PanTools into a
separate environment. Please make sure you install the most recent versions of
the tools.
NB: not all dependencies are available for macOS, therefore please use the
correct line from this code block depending on your operating system:

.. substitution-code-block:: bash

   $ mamba create -n pantools pantools bcftools busco  # Linux
   $ mamba create -n pantools pantools  # macOS

Please make sure to activate the environment before using PanTools:

.. substitution-code-block:: bash

   $ conda activate pantools

Test if PanTools is executable:

.. substitution-code-block:: bash

   $ pantools --help

--------------

Install from download
---------------------

The second option is to download PanTools from `our git page
<https://git.wur.nl/bioinformatics/pantools/-/releases>`_. For this, you will
need to install all dependencies separately. We provide YAML files for a conda
environment that has all dependencies.

Install dependencies using conda
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Please choose the conda_linux.yml or conda_macos.yml file depending on your
operating system. These files be found on the
`release page <https://git.wur.nl/bioinformatics/pantools/-/releases>`_. The
difference between the two files comes from some dependencies conflicting on
macOS but not on Linux. Only on Linux a full installation of PanTools is
possible. On macOS, some functionalities will not be available.

For users that don't have conda installed, we recommend to install conda first:

.. substitution-code-block:: bash

   $ wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
   $ bash Miniconda3-latest-Linux-x86_64.sh

   # Follow the instructions on the screen
   # Restart your terminal

   $ conda config --set auto_activate_base false
   $ conda config --add channels bioconda
   $ conda config --add channels conda-forge
   $ conda config --remove channels defaults
   $ conda config --set channel_priority strict

   # Restart your terminal

Next, we recommend to install mamba into the conda base environment to enable
much faster dependency solving:

.. substitution-code-block:: bash

   $ conda install mamba -n base -c conda-forge

Finally, we can install all dependencies into a separate environment:

.. substitution-code-block:: bash

   $ mamba env create -n pantools -f conda_linux.yml #for Linux
   $ mamba env create -n pantools -f conda_macos.yml #for macOS

Please make sure to activate the environment before using PanTools:

.. substitution-code-block:: bash

   $ conda activate pantools

Download PanTools
~~~~~~~~~~~~~~~~~

Download the latest release from
https://git.wur.nl/bioinformatics/pantools/-/releases. The file to download is
named pantools-|ProjectVersion|.jar.

Set PanTools alias
~~~~~~~~~~~~~~~~~~

To avoid typing long command line arguments every time, we suggest setting
an alias to your profile. Set an alias in your ~/.bashrc using the following
command. Always include the **full** path to PanTools .jar file.

Run the following to add the alias to your ~/.bashrc:

.. substitution-code-block:: bash

   $ echo "alias pantools='java -Xms20g -Xmx50g -jar /path/to/pantools-|ProjectVersion|.jar'" >> ~/.bashrc

Source your ~/.bashrc and test if the alias works:

.. code:: bash

   $ source ~/.bashrc
   pantools --help

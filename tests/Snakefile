"""
This Snakefile requires Java, Maven, curl and samtools to be installed.

It will check:

1. The identity of the pangenome between the local version and a reference
   version, as defined in the `shared.yaml` config file;
2. Idem for the identity of the SAM files generated by both versions.

To execute, specify a configuration file with the data set, e.g.:

snakemake --cores 1 --configfile yeast.yaml

The pipeline will package the local version into a .jar, as well as a reference
version (a git revision) specified in shared.yaml. It will use the dataset
referenced by the configuration file passed in by the user with --configfile
(e.g. yeast.yaml, etc.).

A pangenome will be built with `build_pangenome` for the local and reference
versions each. Pangenomes will be exported with `export_pangenome`, the
resulting .csv files sorted, and compared with the cmp command line tool for
differences. In case a difference is found, the pipeline will fail.

Reads from the data set will be aligned to the local and reference pangenomes,
resulting in a .sam file per genome per database (local or reference). Each
.sam file is and will have its @PG header line stripped to remove samtools
metadata that would otherwise fail the comparison immediately. Then, .sam files
for each genome are compared between the local and reference versions with the
cmp command line tool.
"""
configfile: "shared.yaml"

maven = f"{config['tools']['maven']['command']} {' '.join(config['tools']['maven']['arguments'])}"
dataset_name = config["dataset"]["name"]
java = f"java {' '.join(config['tools']['pantools']['java_arguments'])}"

rule all:
    input:
        # Pangenome export comparison manifest checks. A .cmp file will be generated for each
        # type of exported CSV file generated by 'pantools export_pangenome'.
        expand(
            "output/validation/exports/{dataset_name}/local-vs-{version}/{comparison_type}.cmp",
            dataset_name=dataset_name,
            version=config["tools"]["pantools"]["reference_version"],
            comparison_type=["node-properties", "relationship-properties", "sequence-node-anchors"]
        ),
        # SAM files comparison .TSV files. A .cmp file will be generated for each pair of SAM
        # files, a pair being the alignments against genome x for the local version and the
        # reference version.
        expand(
            "output/validation/alignments/{dataset_name}/local-vs-{version}/pantools_{genome}.cmp",
            dataset_name=dataset_name,
            version=config["tools"]["pantools"]["reference_version"],
            genome=range(1, len(config["dataset"]["genomes"]) + 1)
        )

rule package_local_pantools:
    """Package the local (git) version of PanTools into a .jar."""
    output:
        "jars/pantools/target/pantools-local.jar"
    shell:
        """
        {maven} -f ../pom.xml clean package
        mv ../target/pantools-*.jar {output}
        """

rule package_remote_pantools_version:
    """
    Package any PanTools version into a .jar by cloning it from git.
    NOTE: to reduce clone time and disk space the repo is cloned only to a limited
    depth. The reference version should be updated periodically.
    """
    output:
        "jars/pantools/target/pantools-{version}.jar"
    params:
        repository_url=config["tools"]["pantools"]["repository_url"],
        clone_depth=150
    shell:
        """
        # TODO: clean up
        TEMPORARY_DIRECTORY=$(mktemp -d)
        git clone --depth {params.clone_depth} --branch develop {params.repository_url} ${{TEMPORARY_DIRECTORY}}
        cd ${{TEMPORARY_DIRECTORY}}
        git checkout {wildcards.version}
        cd -
        {maven} -f ${{TEMPORARY_DIRECTORY}}/pom.xml clean package
        mv ${{TEMPORARY_DIRECTORY}}/target/pantools-*.jar {output}
        rm -rf ${{TEMPORARY_DIRECTORY}}
        """

rule build_pangenome:
    """Run build_pangenome with a versioned .jar on a genomes.txt."""
    input:
        jar="jars/pantools/target/pantools-{version}.jar",
        genomes="input/data/{dataset_name}/genomes/genomes.txt"
    output:
        output_directory=directory("output/databases/{dataset_name}/{version}/"),
        done_marker=touch("output/databases/{dataset_name}/{version}.done")
    shell:
        """
        {java} \
            -jar {input.jar} \
            build_pangenome \
            {output.output_directory} \
            {input.genomes}
        """

rule export_pangenome:
    """Export pangenome to multiple, each containing different properties."""
    input:
        database_done_marker="output/databases/{dataset_name}/{version}.done",
        jar="jars/pantools/target/pantools-local.jar"
    output:
        node_properties="output/exports/{dataset_name}/{version}/node-properties.csv",
        relationship_properties="output/exports/{dataset_name}/{version}/relationship-properties.csv",
        sequence_node_anchors="output/exports/{dataset_name}/{version}/sequence-node-anchors.csv"
    shell:
        """
        {java} \
            -jar {input.jar} \
            export_pangenome \
            $(dirname {input.database_done_marker})/{wildcards.version} \
           --node-properties-file {output.node_properties} \
           --relationship-properties-file {output.relationship_properties} \
           --sequence-node-anchors-file {output.sequence_node_anchors}
        """

rule list_genomes:
    """Lists the input genomes."""
    input:
        genomes=config["dataset"]["genomes"],
    output:
        genome_list="input/data/{dataset_name}/genomes/genomes.txt",
    shell:
        """
        ls {input.genomes} | sort > {output.genome_list}
        """

rule map_reads:
    """Map reads against a a database, outputs a SAM file per genome."""
    input:
        fastq1=config["dataset"]["reads"]["fastq1"],
        fastq2=config["dataset"]["reads"]["fastq2"],
        database="output/databases/{dataset_name}/{version}/",
        jar="jars/pantools/target/pantools-{version}.jar"
    output:
        sam_files=expand(
            "output/alignments/{{dataset_name}}/{{version}}/sams/pantools_{genome}.sam",
            genome=range(1, len(config["dataset"]["genomes"]) + 1)
        )
    shell:
        """
        {java} \
            -jar {input.jar} \
            map \
            --threads 1 \
            {input.database} \
            {input.fastq1} \
            {input.fastq2} \
            --gap-open -20 \
            --gap-extension -3 \
            --out-format SAM \
            --output $(dirname {output.sam_files[0]})
        """

rule strip_sam_pg_header_line:
    """
    Strip the @PG header line of a SAM file. Necessary to remove samtools metadata,
    which would otherwise always fail the comparison.
    """
    input: "output/alignments/{dataset_name}/{version}/sams/{filename}.sam"
    output: "output/alignments/{dataset_name}/{version}/sams/{filename}.no-pg-header-line.sam"
    shell: "grep -v '^@PG' {input} > {output}"

rule compare_sams:
    """
    Compare SAM files with cmp. Will exit with a non-zero exit code if files are different.
    It would be nice to use Picard here, but there's a bug that makes it unusable. See:
    https://github.com/broadinstitute/picard/issues/284
    https://github.com/broadinstitute/picard/issues/160
    """
    input:
        sam1="output/alignments/{dataset_name}/{version1}/sams/{filename}.no-pg-header-line.sam",
        sam2="output/alignments/{dataset_name}/{version2}/sams/{filename}.no-pg-header-line.sam"
    output:
        "output/validation/alignments/{dataset_name}/{version1}-vs-{version2}/{filename}.cmp"
    shell:
        """
        mkdir -p $(dirname {output})
        cmp {input.sam1} {input.sam2} | tee {output}
        """

rule compare_exported_csvs:
    """Compare sorted CSV files from export_pangenome with cmp."""
    input:
        csv1="output/exports/{dataset_name}/{version1}/{comparison_type}.sorted.csv",
        csv2="output/exports/{dataset_name}/{version2}/{comparison_type}.sorted.csv"
    output:
        "output/validation/exports/{dataset_name}/{version1}-vs-{version2}/{comparison_type}.cmp"
    shell:
        """
        mkdir -p $(dirname {output})
        cmp {input.csv1} {input.csv2} | tee {output}
        """

rule sort_csvs:
    """Sort CSVs exported by export_pangenome."""
    input:
        "output/exports/{dataset_name}/{version}/{filename}.csv"
    output:
        "output/exports/{dataset_name}/{version}/{filename}.sorted.csv"
    shell:
        """sort {input} > {output}"""
